<?
session_start();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Winn: премиальный корпоративный сайт");
$APPLICATION->SetPageProperty("wide_page", "Y");
$APPLICATION->SetPageProperty("hide_title", "Y");
$APPLICATION->SetPageProperty("hide_section", "Y");
$APPLICATION->SetPageProperty("dark_page", "Y");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"section",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_ID" => "winn_home_banner",
		"BACKGROUND_COLOR" => "#000000",
		"BACKGROUND_IMAGE" => "",
		"COMPONENT_TEMPLATE" => "section",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/index/banners.php",
		"RS_LIST_CONTAINER_ALIGN" => "-",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "N",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "Y",
		"RS_LIST_SECTION_VH100" => "Y",
		"USE_CONTAINER" => "N"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"section",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_ID" => "",
		"BACKGROUND_COLOR" => "",
		"BACKGROUND_IMAGE" => "",
		"COMPANY_ACHIVEMENTS" => "",
		"CONTAINER_ALIGN" => "right",
		"EDIT_TEMPLATE" => "winn_home_catalog",
		"PATH" => "/include/index/catalog.new.php",
		"RS_LIST_CONTAINER_ALIGN" => "right",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "Y",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y",
		"USE_CONTAINER" => "Y"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"section",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_ID" => "winn_home_services",
		"BACKGROUND_COLOR" => "#000000",
		"BACKGROUND_IMAGE" => "",
		"COMPONENT_TEMPLATE" => "section",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/index/banners2.php",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "N",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y",
		"USE_CONTAINER" => "N"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"section",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_ID" => "winn_home_articles",
		"BACKGROUND_COLOR" => "#0B1226",
		"BACKGROUND_IMAGE" => "",
		"COMPONENT_TEMPLATE" => "section",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/index/new-news.php",
		"RS_LIST_CONTAINER_ALIGN" => "-",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "Y",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y",
		"USE_CONTAINER" => "Y"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"section",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_ID" => "",
		"BACKGROUND_COLOR" => "#000000",
		"BACKGROUND_IMAGE" => "/assets/images/about_us1.jpg",
		"COMPONENT_TEMPLATE" => "section",
		"CONTAINER_ALIGN" => "-",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/index/contacts.php",
		"RS_LIST_CONTAINER_ALIGN" => "-",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "Y",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y",
		"USE_CONTAINER" => "Y"
	)
);?>
    <script>

        $(document).ready(function () {
            $(".owl-dot #bx_3218110189_496_dot").prev().text("Безграничность");
            $(".owl-dot #bx_3218110189_486_dot").prev().text("Уверенность");
            $(".owl-dot #bx_3218110189_487_dot").prev().text("Защищенность");
            $(".owl-dot #bx_3218110189_490_dot").prev().text("Скорость");
            $(".owl-dot #bx_3218110189_615_dot").prev().text("Совершенство");

            $(".bx-newslist-more").remove();

            
            $("#winn_home_banner").addClass("bg_custom");
            $("#winn_home_services").addClass("bg_custom");
            $("#miZbvuOf").addClass("bg_custom");


        });
    </script><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>