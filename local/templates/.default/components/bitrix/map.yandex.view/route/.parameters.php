<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
        "ENABLED_ROUTING" => Array(
                "NAME" => GetMessage("ENABLED_ROUTING"),
                "TYPE" => "CHECKBOX",
                "DEFAULT" => "Y",
        ),
);
