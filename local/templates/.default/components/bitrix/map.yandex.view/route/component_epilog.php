<?
if($arParams["ENABLED_ROUTING"] == "Y") {
?><script>
function createRoute(currentCoords) {
                new ymaps.route([currentCoords, firstCoords], {mapStateAutoApply: true}).then(function (route) {
                        if (null != currentRoute) {
                                map.geoObjects.remove(currentRoute);
                        }
                        let points = route.getWayPoints();
                        points.get(0).options.set({preset: 'twirl#redIcon'})
                        currentRoute = route;
                        map.geoObjects.add(route);
                });
        }

window.onload = function () {
	setTimeout(function() {
		// init
		firstCoords = null;
		map = window.GLOBAL_arMapObjects.<?=$arParams["MAP_ID"]?>;
		map.geoObjects.each(function (obj) {
			if(undefined != obj.properties.get('hintContent')) {
				firstCoords = obj.geometry.getCoordinates();
			}
		});
	        currentPlacemark = null;
        	currentRoute = null;
	        // search adress
        	searchControl = new ymaps.control.SearchControl({
                	options: {noPlacemark: true}
	        });
        	searchControl.events.add('resultselect', function (e) {
                	let fieldResultSelect = e.get('target').state;
               		result = fieldResultSelect.get('results')[fieldResultSelect.get('currentIndex')];
                	let currentAddres = result.properties.get('text');
               		createRoute(currentAddres);
        	});
        	map.controls.add(searchControl);
        	// click maps
        	map.events.add('click', function (e) {
                	createRoute(e.get('coords'));
        	});
	}, 1000);
};
</script><?
}
