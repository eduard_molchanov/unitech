<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ваша корзина");
?><?$APPLICATION->IncludeComponent(
	"redsign:lightbasket.basket", 
	"cart", 
	array(
		"COMPONENT_TEMPLATE" => "cart",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "44",
		"PROPS" => array(
			0 => "ARTNUMBER",
			1 => "COLOR",
			2 => "BRAND",
			3 => "WEIGHT",
			4 => "FEATURES",
			5 => "COUNTRY",
			6 => "BIKE_SIZE",
			7 => "COLOR_REF",
			8 => "BRAND_REF",
			9 => "CML2_ARTICLE",
			10 => "",
		),
		"PATH_TO_ORDER" => "/site_nj/cart/order/",
		"PATH_TO_CATALOG" => "/site_nj/catalog/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
