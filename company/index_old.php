<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добро пожаловать");
?><div class="row">
	<div class="col-lg-10">
		<p class="accent">
			Наша команда работает в отрасли ИКТ с 2000 года. Компания специализируется на комплексных решения в области автоматизации, проектированию и созданию Центров обработки данных, а также на поставке специализированного оборудования.
		</p>
	</div>
</div>
<div class="mt-1 mb-5 row">
	<div class="col-lg-12">
 <img src="/site_nj/assets/images/company.jpg" class="about-image" alt="Компания" title="Компания">
	</div>
</div>
<div class="row">
	<div class="col-lg-10">
		<p class="lead">
			 На данный момент мы представляем собой крупную компанию, владеющую интернет-магазином и имеющую в своей сети единый call-центр, который регулирует всю деятельность магазина, отдела продаж, службу доставки, широкий штат квалифицированных сборщиков, собственный склад с постоянным наличием необходимого запаса товаров
		</p>
	</div>
</div>
<div class="mb-6 snippet-quote">
	<div class="row">
		<div class="col-12 col-xl-9">
			<p class="snippet-quote__descr">
				 Мы всегда рады общению с нашими клиентами. Если у вас есть какие-либо пожелания, предложения, замечания, касающиеся работы нашей организации - пишите нам, и мы с благодарностью примем ваше мнение во внимание
			</p>
 <span class="d-block position-relative snippet-quote__author">Лев Бровко, коммерческий директор</span>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-10">
		<p class="lead">
			За это время у нас сложились партнерские отношения с ведущими производителями, позволяющие предлагать высококачественную продукцию по конкурентноспособным ценам.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-lg-10">
		<p class="lead">
			Мы можем гордиться тем, что у нас одни из самых широких ассортиментов продукции в городе и области
		</p>
	</div>
</div>
<h2>Наши возможности</h2>
<div class="row">
	<div class="col-lg-4">
		<ul class="ml-2 ml-sm-6 pl-2 pl-sm-3 mb-6 snippet-list-checkbox">
			<li class="d-flex mb-4 snippet-list-checkbox__list-element"> <span class="position-relative mr-3 snippet-list-checkbox__box"> <span class="d-block position-absolute snippet-list-checkbox__check"></span> </span> <span class="text-break align-self-center snippet-list-checkbox__text">Быстрая доставка</span> </li>
			<li class="d-flex mb-4 snippet-list-checkbox__list-element"> <span class="position-relative mr-3 snippet-list-checkbox__box"> <span class="d-block position-absolute snippet-list-checkbox__check"></span> </span> <span class="text-break align-self-center snippet-list-checkbox__text">Низкие цены</span> </li>
			<li class="d-flex mb-4 snippet-list-checkbox__list-element"> <span class="position-relative mr-3 snippet-list-checkbox__box"> <span class="d-block position-absolute snippet-list-checkbox__check"></span> </span> <span class="text-break align-self-center snippet-list-checkbox__text">Широкий ассортимент</span> </li>
			<li class="d-flex mb-4 snippet-list-checkbox__list-element"> <span class="position-relative mr-3 snippet-list-checkbox__box"> <span class="d-block position-absolute snippet-list-checkbox__check"></span> </span> <span class="text-break align-self-center snippet-list-checkbox__text">Надежные поставщики</span> </li>
			<li class="d-flex mb-4 snippet-list-checkbox__list-element"> <span class="position-relative mr-3 snippet-list-checkbox__box"> <span class="d-block position-absolute snippet-list-checkbox__check"></span> </span> <span class="text-break align-self-center snippet-list-checkbox__text">Бонусы и подарки</span> </li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-lg-10">
		<p class="lead">
			Мы всегда рады общению с нашими клиентами. Если у вас есть какие-либо пожелания, предложения, замечания, касающиеся работы нашего интернет-магазина - пишите нам, и мы с благодарностью примем ваше мнение во внимание
		</p>
	</div>
</div>
<div class="row">
	<div class="col-lg-10">
		<p class="lead">
			Электронная почта <a href="mailto:sale@www.utechs.ru">sale@www.utechs.ru</a>
		</p>
	</div>
</div>
<h2>Компания в цифрах</h2>
 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"achievements",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPANY_ACHIVEMENTS" => "[[\"600000\",\"Посетителей сайта<br>\\nежемесячно\"],[\"12\",\"Регионов<br>\\nприсутствия\"],[\"48000\",\"Покупок каждый<br>\\nмесяц\"],[\"15000\",\"Товаров<br>\\nна складах\"]]",
		"COMPONENT_TEMPLATE" => "achievements",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/site_nj/include/empty.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"redsign:hcard", 
	"organization", 
	array(
		"COMPONENT_TEMPLATE" => "organization",
		"DIRECTOR_FIRST_NAME" => "Александр Генрихович",
		"DIRECTOR_LAST_NAME" => "Витоженц",
		"EMAIL" => "sale@www.utechs.ru",
		"ORGANIZATION" => "Руководство компании",
		"PHONE" => "+7 (495) 662-9291",
		"PHOTO" => "/site_nj/assets/images/about_compressed.jpg",
		"POSITION" => "Генеральный директор",
		"ADR_COUNTRY_NAME" => "",
		"ADR_REGION" => "",
		"ADR_LOCALITY" => "",
		"ADR_STREET_ADDRESS" => "",
		"ADR_EXT_ADDRESS" => "",
		"ADR_POSTAL_CODE" => "",
		"WORKHOURS" => ""
	),
	false
);?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>