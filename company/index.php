<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>


<?php
$APPLICATION->SetTitle("О компании");
?>

    <div class="row">
        <div class="col-lg-12">
            <p class="accent">
                Компания «Унитех» - российский поставщик решений в сфере информационных технологий,
                предоставляющий своим клиентам полный спектр профессиональных услуг в области системной интеграции.
            </p>
        </div>
    </div>
    <div class="col-lg-12">
        <img src="/site_nj/assets/images/company.jpg" class="about-image" alt="Компания" title="Компания">
    </div>
    <h2>
        Основные направления деятельности:
    </h2>
    <!-- ****************************************   -->
    <div class="row">
        <div class="col-lg-12">
            <ul class="ml-2 ml-sm-6 pl-2 pl-sm-3 mb-6 snippet-list-checkbox">
                <? //php foreach ($data as $i): ?>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
                          ИТ-консалтинг
 </span>
                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Автоматизация (Внедрение корпоративных информационных систем и систем управления ИТ-инфраструктурой)
 </span>
                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Информационная безопасность (в том числе, оптимизация корпоративных приложений и контроль доступа в Internet)
                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Центры обработки данных (в том числе, проектирование, строительство и монтаж инфраструктуры
                        центров обработки данных, внедрение автоматизированных систем управления ЦОДами)
 </span>
                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Телекоммуникации (передача данных, создание каналов связи и т.д.)
 </span>
                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Поставка оборудования и программного обеспечения
 </span>
                </li>
                <? //php endforeach; ?>
            </ul>
        </div>
    </div>
    <!-- ****************************************   -->


    <div class="row">
        <div class="col-lg-12">
            <p class="accent">
                Наша компания имеет партнерские отношения с мировыми производителями оборудования и программного
                обеспечения, что позволяет создавать комплексные решения, сочетающие инфраструктурную часть с
                автоматизацией бизнес-процессов.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <p class="accent">
                Компания обладает штатом высококвалифицированных специалистов с опытом реализации
                проектов разного уровня сложности и знаниями бизнес-процессов различных отраслей.
            </p>
        </div>
    </div>

<? //require($_SERVER["DOCUMENT_ROOT"] . "/local/inc.data.company.php"); ?>
    <h2>Наши возможности</h2>
    <div class="row">
        <div class="col-lg-12">
            <ul class="ml-2 ml-sm-6 pl-2 pl-sm-3 mb-6 snippet-list-checkbox">
                <? //php foreach ($data as $i): ?>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
                            Проектирование, внедрение и сопровождение инфраструктурных решений

                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Проектные поставки оборудования и программного обеспечения мировых и российских производителей ИТ- и телеком-оборудования

                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Консалтинг и автоматизация управления ИТ в соответствии и на основе мировых стандартов и рекомендаций ITIL/ITSM, CobiT, ISO 20000 и других

                </li>
                <li class="d-flex  snippet-list-checkbox__list-element">
                    <span class="position-relative mr-3 snippet-list-checkbox__box">
                        <span class="d-block position-absolute snippet-list-checkbox__check"></span>
                    </span> <span
                            class="text-break align-self-center snippet-list-checkbox__text">
Разработка и сопровождение решений в области информационной безопасности

                </li>
                <? //php endforeach; ?>
            </ul>

        </div>

    </div>
    <h2>Наши лицензии и сертификаты</h2>
    <style>
        /*.shadow {*/
        /*    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), -23px 0 20px -23px rgba(0, 0, 0, 0.8), 23px 0 20px -23px rgba(0, 0, 0, 0.8), 0 0 40px rgba(0, 0, 0, 0.1) inset;*/
        /*}*/
    </style>
    <style>

        .css-modal-details {
            height: 60px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        /* Кнопка для открытия */
        .css-modal-details summary {
            display: inline-flex;
            margin: 10px;
            text-decoration: none;
            position: relative;
            font-size: 20px;
            line-height: 20px;
            padding: 12px 30px;
            color: #FFF;
            font-weight: bold;
            text-transform: uppercase;
            font-family: 'Roboto Condensed', Тahoma, sans-serif;
            /*background: #337AB7;*/
            cursor: pointer;
            /*border: 2px solid #BFE2FF;*/
            overflow: hidden;
            z-index: 1;
        }

        /*.css-modal-details summary:hover,*/
        /*.css-modal-details summary:active,*/
        /*.css-modal-details summary:focus {*/
        /*    color: #FFF;*/
        /*}*/

        /*.css-modal-details summary:before {*/
        /*    content: '';*/
        /*    !*position: relative;*!*/
        /*    top: 0;*/
        /*    right: -50px;*/
        /*    bottom: 0;*/
        /*    left: 0;*/
        /*    !*border-right: 50px solid transparent;*!*/
        /*    !*border-top: 50px solid #2D6B9F;*!*/
        /*    transition: transform 0.5s;*/
        /*    transform: translateX(-100%);*/
        /*    !*z-index: -1;*!*/
        /*}*/

        /*.css-modal-details summary:hover:before,*/
        /*.css-modal-details summary:active:before,*/
        /*.css-modal-details summary:focus:before {*/
        /*    transform: translateX(0);*/
        /*}*/

        /* Кнопка при открытом окне переходит на весь экран */
        .css-modal-details details[open] summary {
            cursor: default;
            opacity: 0;
            position: fixed;
            left: 0;
            top: 0;
            z-index: 3;
            width: 100%;
            height: 100%;
        }

        /* Контейнер, который затемняет страницу */
        .css-modal-details details .cmc {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .css-modal-details details[open] .cmc {
            pointer-events: none;
            z-index: 4;
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            animation: bg 0.5s ease;
            background: rgba(0, 0, 0, 0.7);
        }

        /* Модальное окно */
        .css-modal-details details .cmt {
            font-family: Verdana, sans-serif;
            font-size: 16px;
            padding: 20px;
            /*width: 80%;*/
            /*max-width: 600px;*/
            /*max-height: 100%;*/
            height: 100%;
            transition: 0.5s;
            /*border: 6px solid #BFE2FF;*/
            /*border-radius: 12px;*/
            background: #FFF;
            box-shadow: 0 4px 12px rgba(0, 0, 0, 0.2), 0 16px 20px rgba(0, 0, 0, 0.2);
            text-align: center;
            overflow: auto;
        }

        .css-modal-details details[open] .cmt {
            animation: scale 0.5s ease;
            z-index: 4;
            pointer-events: auto;
        }

        /* Декоративная кнопка с крестиком */
        /*.css-modal-details details[open] .cmc:after {*/
        /*    content: "";*/
        /*    width: 50px;*/
        /*    height: 50px;*/
        /*    !*border: 6px solid #BFE2FF;*!*/
        /*    !*border-radius: 12px;*!*/
        /*    position: absolute;*/
        /*    z-index: 10;*/
        /*    top: 20px;*/
        /*    right: 20px;*/
        /*    box-shadow: 0 4px 12px rgba(0, 0, 0, 0.2), 0 16px 20px rgba(0, 0, 0, 0.2);*/
        /*    background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='none' stroke='%23337AB7' stroke-width='3' stroke-linecap='round' stroke-linejoin='round'%3e%3cline x1='18' y1='6' x2='6' y2='18'%3e%3c/line%3e%3cline x1='6' y1='6' x2='18' y2='18'%3e%3c/line%3e%3c/svg%3e");*/
        /*    background-color: #48a586;*/
        /*    background-size: cover;*/
        /*    animation: move 0.5s ease;*/
        /*}*/

        /* Анимации */
        @keyframes scale {
            0% {
                transform: scale(0);
            }
            100% {
                transform: scale(1);
            }
        }

        @keyframes move {
            0% {
                right: -80px;
            }
            100% {
                right: 20px;
            }
        }

        /*@keyframes bg {*/
        /*    0% {*/
        /*        background: rgba(51, 122, 183, 0);*/
        /*    }*/
        /*    100% {*/
        /*        background: rgba(51, 122, 183, 0.7);*/
        /*    }*/
        /*}*/

        .css-modal-details summary {
            /*display: inline-flex;*/
            margin: 5px;
            text-decoration: none;
            position: relative;
            font-size: 16px;
            line-height: 20px;
            padding: 12px 30px;
            color: #48a586;
            /*font-weight: bold;*/
            text-transform: uppercase;
            font-family: 'Roboto Condensed', Тahoma, sans-serif;
            background: #FFF;
            cursor: pointer;
            border: 0px solid #BFE2FF;
            overflow: hidden;
            z-index: 1;
        }

        .css-modal-details {
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>


    <div class="row ">
        <div class="css-modal-details col-6 col-md-offset-6">
            <details>
                <summary>
                    <img src="/company/img/certificate_1.jpg" alt="" width="250" title="Лицензия ФСБ" style=""/>
                </summary>

                <div class="cmc">
                    <div class="cmt">
                        <img src="/company/img/certificate_1.jpg" alt=""/>
                        <br>
                        Лицензия ФСБ
                    </div>
                </div>
            </details>
        </div>
        <div class="css-modal-details col-4">
            <details>
                <summary>
                    <img src="/company/img/certificate_2.jpg" alt="" width="250"
                         title="Сертификат соответствия ГОСТ Р ИСО 9001-2015"/>
                </summary>
                <div class="cmc">
                    <div class="cmt">
                        <img src="/company/img/certificate_2.jpg" alt="" height="100%"/>
                        <br>
                        Сертификат соответствия ГОСТ Р ИСО 9001-2015
                    </div>
                </div>
            </details>
        </div>

    </div>

    <div class="row ">
        <div class="css-modal-details col-6 col-md-offset-6">
            <details style="">
                <summary>
                    Лицензия ФСБ
                </summary>

                <div class="cmc">
                    <div class="cmt">
                        <img src="/company/img/certificate_1.jpg" alt=""/>
                        <br>
                        Лицензия ФСБ
                    </div>
                </div>
            </details>
        </div>
        <div class="css-modal-details col-4">
            <details>
                <summary>
                    Сертификат соответствия ГОСТ Р ИСО 9001-2015
                </summary>
                <div class="cmc">
                    <div class="cmt">
                        <img src="/company/img/certificate_2.jpg" alt="" height="100%"/>
                        <br>
                        Сертификат соответствия ГОСТ Р ИСО 9001-2015
                    </div>
                </div>
            </details>
        </div>

    </div>



    <div class="row">
        <div class="col-lg-10">
            <p class="lead">
                Электронная почта <a href="mailto:info@unitechnologies.ru">info@unitechnologies.ru</a>
            </p>
        </div>
    </div>


<? $APPLICATION->IncludeComponent(
    "redsign:hcard",
    "organization",
    array(
        "COMPONENT_TEMPLATE" => "organization",
        "DIRECTOR_FIRST_NAME" => "Александр Генрихович",
        "DIRECTOR_LAST_NAME" => "Витоженц",
        "EMAIL" => "info@unitechnologies.ru",
        "ORGANIZATION" => "Руководство компании",
        "PHONE" => "+7 (495) 662-9291",
        "PHOTO" => "./team/img/fce14698e2c49bc75a107ebf8e6f46b1.png",
        "POSITION" => "Генеральный директор",
        "ADR_COUNTRY_NAME" => "",
        "ADR_REGION" => "",
        "ADR_LOCALITY" => "",
        "ADR_STREET_ADDRESS" => "",
        "ADR_EXT_ADDRESS" => "",
        "ADR_POSTAL_CODE" => "",
        "WORKHOURS" => ""
    ),
    false
); ?>

    <h2>Компания в цифрах</h2>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "achievements",
    array(
        "AREA_FILE_SHOW" => "file",
        "COMPANY_ACHIVEMENTS" => "[[\"600000\",\"Посетителей сайта<br>\\nежемесячно\"],[\"12\",\"Регионов<br>\\nприсутствия\"],[\"48000\",\"Покупок каждый<br>\\nмесяц\"],[\"15000\",\"Товаров<br>\\nна складах\"]]",
        "COMPONENT_TEMPLATE" => "achievements",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/site_nj/include/empty.php"
    )
); ?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>