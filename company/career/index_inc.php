<p>
	 Команда компании «Унитех» &nbsp;состоит&nbsp; из квалифицированных и опытных специалистов, всегда нацеленных на результат.
</p>
<p>
	 Мы дорожим качеством предоставляемых услуг, поэтому обеспечиваем нашим сотрудникам возможность постоянного повышения квалификации. Каждый специалист может пройти обучение и сертифицироваться по решениям наших партнеров.
</p>
<p>
	 Каждому сотруднику гарантируется&nbsp;стабильная заработная плата&nbsp;и комфортные условия работы.
</p>
<p>
	 Отправить свое резюме Вы можете на адрес: <a href="mailto:job@unitechnologies.ru">job@unitechnologies.ru</a>
</p>
<p>
</p>
<h2>Открытые вакансии</h2>
<h3>
<ul>
	<li><a href="http://utechs.beget.tech/company/career/sistemnyy-inzhener-v-oblasti-servernoy-infrastruktury-i-virtualizatsii-vychisleniy.php?clear_cache=Y" target="_blank">Системный инженер в области серверной инфраструктуры и виртуализации вычислений</a></li>
</ul>
<ul>
	<li><a href="http://utechs.beget.tech/company/career/sistemnyy-inzhener-v-oblasti-setevykh-tekhnologiy-i-telekommunikatsiy.php?clear_cache=Y" target="_blank">Системный инженер в области сетевых технологий и телекоммуникаций</a></li>
</ul>
<ul>
	<li><a href="http://utechs.beget.tech/servisnyy-inzhener-vtoroy-linii-podderzhki.php?clear_cache=Y" target="_blank">Сервисный инженер второй линии поддержки</a></li>
</ul>
 </h3>