<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Наша команда");
?>
<? $APPLICATION->AddChainItem("Наша команда"); ?>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    array(
        "AREA_FILE_SHOW" => "page",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => ""
    )
); ?>
    <style>
        .shadow {
            /*position: relative;*/
            /*box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 30px rgba(0, 0, 0, 0.1) inset;*/
        }

        .shadow:before,
        .shadow:after {
            content: "";
            position: absolute;
            z-index: -1;
            bottom: 15px;
            left: 10px;
            width: 50%;
            height: 20%;
            max-width: 300px;
            box-shadow: 0 15px 10px rgba(0, 0, 0, 0.7);
            transform: rotate(-3deg);
        }

        .shadow:after {
            right: 10px;
            left: auto;
            transform: rotate(3deg);
        }

        .a-foto {
           /* position: relative;*/
           /* height: 430px;*/
           /*margin-top: 10px;*/


        }

    </style>

    <br>
    <div class="vcard mb-5 ">

        <div class="row mb-6 pb-6 flex-row align-items-stretch justify-content-center ">

            <div class="col-12 col-sm-8 ">
                <div class="vcard__descr ">
                    <div class="vcard__inner d-flex flex-column">

                        <h4 class="mt-1 vcard__director">Витоженц<br>Александр Генрихович</h4>
                        <p class="mt-2 pb-5 vcard__position">Генеральный директор</p>
                        <a class="ml-6 pb-5 position-relative vcard__phone" href="tel: +74956629291">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-phone-call"></use>
                            </svg>
                            +7 (495) 662-9291 </a>
                        <a class="ml-6 position-relative vcard__email" href="mailto:info@unitechnologies.ru">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-email"></use>
                            </svg>
                            info@unitechnologies.ru </a>
                    </div>
                </div>
            </div>
            <div class="justify-content-center align-items-stretch position-relative overflow-hidden d-flex col-12 col-sm-4">
                <div class="position-relative w-100 ">
                    <img class="vcard__photo a-foto" src="./img/fce14698e2c49bc75a107ebf8e6f46b1.png"  alt="Фото директора"
                         height="50">
                </div>

            </div>
        </div>
    </div>

    <div class="vcard mb-5">

        <div class="row mb-6 pb-6 flex-row align-items-stretch justify-content-center">
            <div class="justify-content-center align-items-stretch position-relative overflow-hidden d-flex col-12 col-sm-4">
                <div class="position-relative w-100">
                    <img class="vcard__photo a-foto" src="./img/0697f2b1c4f431b548ae2d0c65dd1d0e.png" alt="Фото директора">
                </div>

            </div>
            <div class="col-12 col-sm-8">
                <div class="vcard__descr ">
                    <div class="vcard__inner d-flex flex-column">

                        <h4 class="mt-1 vcard__director">Лев Бровко</h4>
                        <p class="mt-2 pb-5 vcard__position">Коммерческий директор</p>
                        <a class="ml-6 pb-5 position-relative vcard__phone" href="tel: +74956629291">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-phone-call"></use>
                            </svg>
                            +7 (495) 662-9291 </a>
                        <a class="ml-6 position-relative vcard__email" href="mailto:info@unitechnologies.ru">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-email"></use>
                            </svg>
                            info@unitechnologies.ru </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vcard mb-5">

        <div class="row mb-6 pb-6 flex-row align-items-stretch justify-content-center">

            <div class="col-12 col-sm-8">
                <div class="vcard__descr ">
                    <div class="vcard__inner d-flex flex-column">

                        <h4 class="mt-1 vcard__director">Мария Бартенева</h4>
                        <p class="mt-2 pb-5 vcard__position">Управляющий директор</p>
                        <a class="ml-6 pb-5 position-relative vcard__phone" href="tel: +74956629291">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-phone-call"></use>
                            </svg>
                            +7 (495) 662-9291 </a>
                        <a class="ml-6 position-relative vcard__email" href="mailto:info@unitechnologies.ru">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-email"></use>
                            </svg>
                            info@unitechnologies.ru </a>
                    </div>
                </div>
            </div>
            <div class="justify-content-center align-items-stretch position-relative overflow-hidden d-flex col-12 col-sm-4">
                <div class="position-relative w-100">
                    <img class="vcard__photo a-foto" src="./img/2cc00d7105ddeff6b337f715ffc16e06.png" alt="Фото директора">
                </div>

            </div>
        </div>
    </div>
    <div class="vcard mb-5">

        <div class="row mb-6 pb-6 flex-row align-items-stretch justify-content-center">
            <div class="justify-content-center align-items-stretch position-relative overflow-hidden d-flex col-12 col-sm-4">
                <div class="position-relative w-100">
                    <img class="vcard__photo a-foto" src="./img/3f401320c1074aed22b3f67cabc3d475.png" alt="Фото директора">
                </div>

            </div>
            <div class="col-12 col-sm-8">
                <div class="vcard__descr ">
                    <div class="vcard__inner d-flex flex-column">

                        <h4 class="mt-1 vcard__director">Александр Храмцов</h4>
                        <p class="mt-2 pb-5 vcard__position">Технический директор</p>
                        <a class="ml-6 pb-5 position-relative vcard__phone" href="tel: +74956629291">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-phone-call"></use>
                            </svg>
                            +7 (495) 662-9291 </a>
                        <a class="ml-6 position-relative vcard__email" href="mailto:info@unitechnologies.ru">
                            <svg class="icon-svg d-block text-primary">
                                <use xlink:href="#svg-email"></use>
                            </svg>
                            info@unitechnologies.ru </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none">
        <? $APPLICATION->IncludeComponent(
            "redsign:hcard",
            "organization",
            array(
                "COMPONENT_TEMPLATE" => "organization",
                "DIRECTOR_FIRST_NAME" => "Александр Храмцов",
                "DIRECTOR_LAST_NAME" => "",
                "EMAIL" => "info@unitechnologies.ru",
                "ORGANIZATION" => "",
                "PHONE" => "+7 (495) 662-9291",
                "PHOTO" => "./team/img/3f401320c1074aed22b3f67cabc3d475.png",
                "POSITION" => "Технический директор",
                "ADR_COUNTRY_NAME" => "",
                "ADR_REGION" => "",
                "ADR_LOCALITY" => "",
                "ADR_STREET_ADDRESS" => "",
                "ADR_EXT_ADDRESS" => "",
                "ADR_POSTAL_CODE" => "",
                "WORKHOURS" => ""
            ),
            false
        ); ?>
    </div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>