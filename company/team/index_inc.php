<p>
	 Команда «Унитеха» имеет 20-летний опыт работы на российском рынке ИКТ, в ходе которой успешно реализован ряд сложных проектов в интересах крупных компаний-заказчиков. Специалисты «Унитеха», глубоко понимая специфику деятельности и порядок работы своих заказчиков, готовы применять свой опыт в реализации комплексных проектов.
</p>