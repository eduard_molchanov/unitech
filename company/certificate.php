<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Страница для экспериментов");


?>
<link rel="stylesheet" href="https://atuin.ru/demo/fancybox/jquery.fancybox.css">

<style>
    .shadow {
        box-shadow: 0 1px 4px rgba(0,0,0,0.3), -23px 0 20px -23px rgba(0,0,0,0.8), 23px 0 20px -23px rgba(0,0,0,0.8), 0 0 40px rgba(0,0,0,0.1) inset;
    }
</style>
<div class="row shadow">
    <a data-caption="Фото 1" data-fancybox="my-images-2" href="/company/img/certificate_1.jpg">
        <img src="/company/img/certificate_1.jpg" alt="" width="250"/>Лицензия ФСБ

    </a>

    <a data-caption="Фото 10" data-fancybox="my-images-2" href="/company/img/certificate_2.jpg">
        <img src="/company/img/certificate_2.jpg" alt="" width="250"/>Сертификат соответствия ГОСТ Р ИСО 9001-2015
    </a>

</div>


<script src="https://atuin.ru/demo/fancybox/jquery.fancybox.js"></script>
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");

//  http://212.193.51.52/company/certificate.php
?>
