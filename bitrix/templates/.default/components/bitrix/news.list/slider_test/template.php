<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//print_r($arResult)?>
<div class="carousel-container">
	<div id="carousel" class  = "carousel_main_service">
	<?foreach ($arResult["ITEMS"] as $arItem):?>	
			<div class="carousel-feature">
		 	<img class="carousel-image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="Image Caption">
		 	  <div class="carousel-caption"><div class = "title"><?=$arItem["NAME"]?></div>
			  <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<div class = "text"><?echo $arItem["PREVIEW_TEXT"];?></div>
		<?endif;?>
			  </div>
		 	</div>		  
		<?endforeach?>	
		
		</div>
		                   <div id="carousel-left"></div>
                          <div id="carousel-right"></div>
                          <div class = "slider_before">
                          </div>
	</div>
