$('document').ready(function(){

    $('.slick_portfolio').slick({
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 1
    });
    $('.search_header').click(function() {
        $(this).next(".seach").stop(true , true).slideToggle();    

    });
    $("#carousel").featureCarousel({

           autoPlay : 0,
           largeFeatureWidth : 475,
           largeFeatureHeight : 475,
           smallFeatureWidth : 250,
            smallFeatureHeight : 250,
            smallFeatureOffset: 120,
            carouselSpeed : 1000,
            autoPlay : 3000
               });
    $('.vacancies .description').readmore({
        speed: 200,
        moreLink: '<a class = "morelink link" href="#"><span>Узнать больше</span></a>',
        lessLink: '<a class = "lesslink link" href="#"><span>Свернуть</span></a>',
        collapsedHeight: 115
    });
    $(".phone_mask").mask("+7(000)000-00-00",  {placeholder: "+7 (ХХХ) ХХХ-ХХ-ХХ"});

    $(".fancybox_link").fancybox({
        helpers: {
            overlay: {
               locked: false
                }
            }
    });

    $(".certificates_link").fancybox({
        padding:[34 , 30, 30, 30],
        wrapCSS : "fancybox_list",
        helpers: {
            overlay: {
               locked: false
                }
            }
    });
    $('.form select').styler();
    $('.project_filter_select').styler();
    $(".fancybox_form").fancybox({
                padding:[12 , 40, 40, 40],
                helpers: {
       overlay: {
           locked: false
            }
        }
        });

});
