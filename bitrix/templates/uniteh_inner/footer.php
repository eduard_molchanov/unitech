<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>			
              
                      
                  
             
			   </div>
                  </div>
			  
			  </div>
			</div>
		</div>
		<!--<div id="space-for-footer"></div>!-->
	</div>
	
<footer>

      <div class = "container">
          <div class = "row">
                    <div class = "col-xs-12">
                        <div class = "footer_top">
                          <div class = "footer_info">
                            <img src="/i/logo.png" alt="">

                            <?$APPLICATION->IncludeFile("/include/bottom_info.php")?>
                        </div>
						
<ul class="footer_menu">						
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"bottom_uniteh", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "bottom_uniteh"
	),
	false
);?>
      </ul>   
                    </div>


              <div class = "footer_bottom">
                  <div class = "copyright">
                    © 2017  «Универсальные технологии»
                  </div>

                  <div class = "webrost">
                    Разработка сайта <a href = "http://www.webrost.ru/" target="_blank">Студия "Веброст"</a>
                  </div>
              </div>
              </div>
          </div>
      </div>
  </footer>


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.js"></script>
<script type="text/javascript" src="/js/readmore.min.js"></script>
<script type="text/javascript" src="/js/jquery.featureCarousel.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/js/slick.js"></script>
<script type="text/javascript" src="/js/jquery.formstyler.js"></script>
<script type="text/javascript" src="/js/map.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<?$APPLICATION->ShowHeadScripts();?>
 <!-- <script type="text/javascript" src="js/pixel.perfect.js"></script>-->
</body>
</html>