(function( $ ) {    
	$('#'+JQForm.id).wjQWF({
		content_window_css : 'wjQWF-content popul popul_contact contact_select', //классы окна виджета через ' '
		header : '<div class = "h1">Написать нам</div><div class = "warning">Поля, отмеченные звёздочкой, обязательны для заполнения</div>', //любой блоковый элемент
		footer: '',
		okbox : {
			html: '<div class="wjQWF-ok-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщений
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide',
		},
		errbox : {
			html: '<div class="wjQWF-err-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщений
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		}, 
		form: {
			html: '<form class = "form"></div>', //любой блоковый элемен
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		send_btn : {
			html: '<button class="buttom buttom_form">Отправить</button>',
			on_class: 'wjQWF-show wjQWF-btn',
			off_class: 'wjQWF-hide wjQWF-btn'
		},
        default_inputs: {
            input: '<fieldset><label for="#DOM_ID#">#FIELD_CAPTION##FIELD_REQUIRED#</label><input class="form__write_input" type="text" name="#FIELD_NAME#" id="#DOM_ID#" value="#FIELD_VALUE#"></fieldset>',
            textarea: '<fieldset><label for="#DOM_ID#">#FIELD_CAPTION##FIELD_REQUIRED#</label><textarea name="#FIELD_NAME#">#FIELD_VALUE#</textarea></fieldset>', 
        },
       
		bx_data : JQForm.bx_data,
	});

	$('#'+JQForm.id+'-btn').fancybox({
					beforeShow: function () {
						if ($('#'+JQForm.id).wjQWF('toReset'))
						{
							$('#'+JQForm.id).wjQWF('resetForm');
						}
						wrapCSS: 'fancy-popup'
					}						
	});
}( jQuery ) );