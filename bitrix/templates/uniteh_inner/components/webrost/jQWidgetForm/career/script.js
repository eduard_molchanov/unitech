(function( $ ) {    
	$('#'+JQPopupForm.id).wjQWF({
		content_window_css : 'wjQWF-content', //классы окна виджета через ' '
		header : '<div class = "h1">Отправить резюме</div><div class = "warning">Поля, отмеченные звёздочкой, обязательны для заполнения</div>', //любой блоковый элемент
		footer: '',
		okbox : {
			html: '<div class="wjQWF-ok-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщений
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide',
		},
		errbox : {
			html: '<div class="wjQWF-err-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщений
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		form: {
			html: '<form class="form"><form>', //любой блоковый элемен
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		send_btn : {
			html: '<button class = "buttom buttom_form">Отправить<span></span></button>',
			on_class: 'wjQWF-show wjQWF-btn',
			off_class: 'wjQWF-hide wjQWF-btn'
		},
		bx_data : JQPopupForm.bx_data,
	});

	$('#'+JQPopupForm.id+'-btn').fancybox({
					beforeShow: function () {
						if ($('#'+JQPopupForm.id).wjQWF('toReset'))
							$('#'+JQPopupForm.id).wjQWF('resetForm');
					},
					wrapCSS: 'fancy-popup' 
	});
}( jQuery ) );