<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="service_catalog">
<?foreach ($arResult['SECTIONS_LIST'][0] as $arSection) {
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
	?>
	<div class="service_catalog_item" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
			<?if($arSection['PICTURE']){?>
		<div class="name"><img src="<?=$arSection['PICTURE']['SRC']?>" alt="">
			<?}?>		
	<?=$arSection['NAME']?></div>
		<ul>
		
			<?foreach ($arResult['SECTIONS_LIST'][$arSection['ID']] as $subSection) {?>
				<li><a href="<?=$subSection['SECTION_PAGE_URL']?>"><?=$subSection['NAME']?></a></li>
				
			<?}?>
		
		</ul>
	</div>
<?}?>
</div>