<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
 <p><?=$arResult["~PREVIEW_TEXT"]?></p>
 <?=$arResult["~DETAIL_TEXT"]?>
	<?if($arResult["PROJECTS"]):?>
<div class="detail_project">
    <div class="h2">Реализованные проекты</div>

<div class="service_project service_project--content">
<?foreach($arResult["PROJECTS"] as $arItem):?>
	<div class="item">
		<div class="item--img"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
				class="preview_picture"
				border="0"
				src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
				width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
				height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
				alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
				title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
				style="float:left"
				/></a></div>
		<div class="item--description">
		    <a class="name" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a><br />
		</div>
	</div>
<?endforeach;?>
</div>
</div>
<?endif?>
    <br/>
<a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>" class="back buttom">Вернуться к списку услуг</a>
