<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class = "service_catalog">
    <?foreach ($arResult["SECTIONS"] as $arSection) {?>
        <div class = "service_catalog_item">
               <div class = "name"><img src = "<?=$arSection["PICTURE"]["SRC"]?>" alt=""><span><?=$arSection["NAME"]?></span></div>                                           
            <?if ($arSection["ELEMENTS"]) {?>
                <ul>
                <?foreach ($arSection["ELEMENTS"] as $arSubSection) {?>
                    <li>
                        <a href="<?=$arSubSection["DETAIL_PAGE_URL"]?>"><?=$arSubSection["NAME"]?></a>
                    </li>
                <?}?>
                </ul>
            <?}?>
        </div>
    <?}?>
</div>