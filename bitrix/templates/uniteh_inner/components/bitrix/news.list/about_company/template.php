<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<img class="preview_picture" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"/>		
			<?else:?>
				<img class="preview_picture" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"/>					
			<?endif;?>
		<?endif?>
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br />
			<?else:?>
				<b><?echo $arItem["NAME"]?></b><br />
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<p><?echo $arItem["PREVIEW_TEXT"];?></p>
		<?endif;?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<div style="clear:both"></div>
		<?endif?>
		<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<small>
			<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
			</small><br />
		<?endforeach;?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
		<?if($pid =="LIST1"): ?> 
			<h2><?=$arProperty["NAME"]?>:</h2>
			<ul>
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<li><?=implode("<li>", $arProperty["DISPLAY_VALUE"]);?></li></ul>
			<?else:?>
				<li><?=$arProperty["DISPLAY_VALUE"];?></li>
			<?endif?>
			<?endif?>
			
		<?endforeach;?>	
		
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
		<?if($pid =="LIST2"): ?>
			<h2><?=$arProperty["NAME"]?>:</h2>
			<ol>
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<li><?=implode("<li>", $arProperty["DISPLAY_VALUE"]);?></ol>
			<?else:?>
				<li><?=$arProperty["DISPLAY_VALUE"];?></li>
			<?endif?>
			<?endif?>
		<?endforeach;?>	
		
		<?/*foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
		<?if($pid =="UPLOAD_FILE"): ?>
		<h2><?=$arProperty["NAME"]?>:</h2>
		<div class="download"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><a class="download__link pdf" href="<?=$arItem["DISPLAY_PROPERTIES"]["UPLOAD_FILE"]["FILE_VALUE"]["SRC"]?>">
		<?=$arItem["DISPLAY_PROPERTIES"]["UPLOAD_FILE"]["FILE_VALUE"]["ORIGINAL_NAME"]?></a>
		<span><?=round($arItem["DISPLAY_PROPERTIES"]["UPLOAD_FILE"]["FILE_VALUE"]["FILE_SIZE"]/1024,2)?>&nbsp;kb.</span>
		</div>
	    <?endif?>
		<?endforeach;*/?>
	</p>
<?endforeach;?>
</div>
