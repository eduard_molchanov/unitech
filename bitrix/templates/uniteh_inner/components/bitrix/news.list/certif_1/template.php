<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
<ul class="certificates">
<?foreach($arResult["ITEMS"] as $k=>$arItem):?>
	<li class="certificates_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="fancybox_link" rel="group">
			<span class="img_box"><img src="<?=$arItem['PREVIEW_PICTURE_MOD']['SRC']?>" ></span>
			<span class="title"><?echo $arItem["NAME"]?></span>
		</a>
	</li>
<?endforeach;?>
</ul>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>