<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['ITEMS'] as &$item)
{
    $picture = CFile::ResizeImageGet(
        $item['PREVIEW_PICTURE'],
        true
    );
    $item['PREVIEW_PICTURE_MOD'] = array(
        'SRC' => $picture['src'],
        'WIDTH' => $picture['width'],
        'HEIGHT' => $picture['height']
    );
}
?>