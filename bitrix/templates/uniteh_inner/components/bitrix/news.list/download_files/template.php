<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="download">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
		<?if($pid =="UPLOAD_FILES"): ?>
		<h2><?=$arProperty["NAME"]?>:</h2>
		<div class="download"><i class="fa fa-file-excel-o" aria-hidden="true"></i><a class="download__link xls" href="<?=$arItem["DISPLAY_PROPERTIES"]["UPLOAD_FILE"]["FILE_VALUE"]["SRC"]?>">
		<?=$arItem["DISPLAY_PROPERTIES"]["UPLOAD_FILES"]["FILE_VALUE"]["ORIGINAL_NAME"]?></a>
		<span><?=round($arItem["DISPLAY_PROPERTIES"]["UPLOAD_FILES"]["FILE_VALUE"]["FILE_SIZE"]/1024,2)?>&nbsp;kb.</span>
		</div>
	    <?endif?>
		<?endforeach;?>
<?endforeach;?>
</div>
