<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!--<pre><?var_dump($arResult["PARTNERS"])?></pre>!-->
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class = "img_logo"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>

	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;?>
	    <?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

		<h3><?=$arProperty["NAME"]?>:&nbsp;</h3>
		<ul><?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<li><?=implode("&nbsp;<br/><li>&nbsp;", $arProperty["DISPLAY_VALUE"]);?></li></ul>
		<?else:?>
			<li><?=$arProperty["DISPLAY_VALUE"];?></li>
		<?endif?>
	<?endforeach;?>
<div class="pagination_buttom pagination_buttom_margin">
	<?if($arResult['NEXT_POST']):?>
		<div class="link_prev"><a href="<?=$arResult['NEXT_POST']['DETAIL_PAGE_URL']?>" title="<?=$arResult['PREV_POST']['NAME']?>">ПРЕДЫДУЩИЙ ПРОЕКТ</a></div>
	<?endif?>
	<?if ($arResult['NEXT_POST'] && $arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/projects">ВЕРНУТЬСЯ К СПИСКУ ПРОЕКТОВ</a></div>
	<?endif?>
	<?if ($arResult['NEXT_POST'] && !$arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/projects">ВЕРНУТЬСЯ К СПИСКУ ПРОЕКТОВ</a></div>
	<?endif?>
	<?if (!$arResult['NEXT_POST'] && $arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/projects">ВЕРНУТЬСЯ К СПИСКУ ПРОЕКТОВ</a></div>
	<?endif?>
	<?if($arResult['PREV_POST']):?>
	<div class="link_next"><a href="<?=$arResult['PREV_POST']['DETAIL_PAGE_URL']?>" title="<?=$arResult['NEXT_POST']['NAME']?>">СЛЕДУЮЩИЙ ПРОЕКТ</a></div>
	<?endif?>
</div>
<!--<div class = "pagination_buttom pagination_buttom_margin">
	<?if($arResult['PREV_POST']):?>
	<div class="link_prev"><a href="<?=$arResult['PREV_POST']["DETAIL_PAGE_URL"]?>" title="<?=$arResult['PREV_POST']['NAME']?>">ПРЕДЫДУЩИЙ ПРОЕКТ</a></div>
	<?endif?>
	<?if ($arResult['NEXT_POST'] && $arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/projects">ВЕРНУТЬСЯ К СПИСКУ ПРОЕКТОВ</a></div>
	<?endif?>
	<?if ($arResult['NEXT_POST'] && !$arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/projects">ВЕРНУТЬСЯ К СПИСКУ ПРОЕКТОВ</a></div>
	<?endif?>
	<?if (!$arResult['NEXT_POST'] && $arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/projects">ВЕРНУТЬСЯ К СПИСКУ ПРОЕКТОВ</a></div>
	<?endif?>
		<?if($arResult['NEXT_POST']):?>
	<div class="link_next"><a href="<?=$arResult['NEXT_POST']["DETAIL_PAGE_URL"]?>" title="<?=$arResult['NEXT_POST']['NAME']?>">СЛЕДУЮЩИЙ ПРОЕКТ</a></div>
	<?endif?>!-->

</div>
<?if($arResult["PARTNERS"]):?>
<div class = "clients">
<h2>Партнеры</h2>
<ul class = "client_list">
<?foreach($arResult["PARTNERS"] as $arItem):?>

	<li>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span class = "img_box"><img class = "img_logo" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"/></span>
				</a>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span class = "name"><?echo $arItem["NAME"]?></span></a>
	</li>
<?endforeach?>
</ul>
</div>
<?endif?>