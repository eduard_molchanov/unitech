<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['SECTIONS_LIST'] = array();
foreach($arResult['SECTIONS'] as $section){
	$arResult['SECTIONS_LIST'][intval($section['IBLOCK_SECTION_ID'])][] = $section;
}
unset($arResult['SECTIONS']);