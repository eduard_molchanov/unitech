<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<div class = "publication_data"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
	<?endif;?>
		<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
	<div class = "publication_link"><?=$arProperty["NAME"]?>:&nbsp;
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?=implode($arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
	<?endforeach;?></div>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
<div class="pagination_buttom pagination_buttom_margin">
	<?if($arResult['NEXT_POST']):?>
		<div class="link_prev"><a href="<?=$arResult['NEXT_POST']['DETAIL_PAGE_URL']?>" title="<?=$arResult['PREV_POST']['NAME']?>">ПРЕДЫДУЩАЯ ПУБЛИКАЦИЯ</a></div>
	<?endif?>
	<?if ($arResult['NEXT_POST'] && $arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/presscenter/publication">ВЕРНУТЬСЯ К СПИСКУ ПУБЛИКАЦИЙ</a></div>
	<?endif?>
	<?if ($arResult['NEXT_POST'] && !$arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/presscenter/publication">ВЕРНУТЬСЯ К СПИСКУ ПУБЛИКАЦИЙ</a></div>
	<?endif?>
	<?if (!$arResult['NEXT_POST'] && $arResult['PREV_POST']):?>
	<div class="link_spisok"><a class="back buttom" href="/presscenter/publication">ВЕРНУТЬСЯ К СПИСКУ ПУБЛИКАЦИЙ</a></div>
	<?endif?>
	<?if($arResult['PREV_POST']):?>
	<div class="link_next"><a href="<?=$arResult['PREV_POST']['DETAIL_PAGE_URL']?>" title="<?=$arResult['NEXT_POST']['NAME']?>">СЛЕДУЮЩАЯ ПУБЛИКАЦИЯ</a></div>
	<?endif?>
</div>