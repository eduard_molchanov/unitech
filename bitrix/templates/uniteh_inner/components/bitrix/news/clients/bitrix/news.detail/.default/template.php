<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component detail_picture*/
$this->setFrameMode(true);
?>
<div class="news-detail client_detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="img_logo"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
<a class = "detail_link"><?if(is_array($arProperty["DISPLAY_VALUE"])):?></a>
			<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
		<br />
	<?endforeach;
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
	
	</div>	

	<!--<img class="img_logo" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>">
	<div class = "detail_status">
		Статус клиента может занимать несколько строк
	</div>
	<p>
	 ПАО «Ростелеком» – одна из крупнейших в России и Европе телекоммуникационных компаний национального масштаба, присутствующая во всех сегментах рынка услуг связи и охватывающая миллионы домохозяйств в России.
	</p>
	<p>
		 Компания занимает лидирующее положение на российском рынке услуг ШПД и платного телевидения: количество абонентов услуг ШПД превышает 12,2 млн, а платного ТВ «Ростелекома»&nbsp;–&nbsp;более 9,2 млн пользователей, из которых свыше 4,0 миллионов смотрит уникальный федеральный продукт «Интерактивное ТВ».
	</p>
	<a class = "detail_link" href="http://www.rostelecom.ru">http://www.rostelecom.ru</a>!-->
	<!--<div class="detail_project"> 
    <div class="h2">Реализованные проекты</div>

          <div class="item">
                <div class="item--img">
                          <img alt="" src="img/project_sert/img_1.png">
              </div>
				<div class="item--description">
					   <a href=" " class="name">Название реализованного проекта в сфере аудита</a>
						<div class="text">Краткое описание комплекса предоставленных услуг, и восторга заказчика,
						который он конечно же испытал и логотип которого размещён слева от этого текста, наличие которого желательно.</div>
					   <a class="link" href="">Узнать больше </a>
				</div>
			</div>
            <div class="item">
                <div class="item--img">
                          <img alt="" src="img/project_sert/img_1.png">
              </div>
				<div class="item--description">
					   <a href=" " class="name">Название реализованного проекта в сфере аудита</a>
						<div class="text">Краткое описание комплекса предоставленных услуг, и восторга заказчика,
						который он конечно же испытал и логотип которого размещён слева от этого текста, наличие которого желательно.</div>
					   <a class="link" href="">Узнать больше </a>
				</div>
			</div>

                                           <div class="item">
                <div class="item--img">
                          <img alt="" src="img/project_sert/img_1.png">
              </div>
				<div class="item--description">
					   <a href=" " class="name">Название реализованного проекта в сфере аудита</a>
						<div class="text">Краткое описание комплекса предоставленных услуг, и восторга заказчика,
						который он конечно же испытал и логотип которого размещён слева от этого текста, наличие которого желательно.</div>
					   <a class="link" href="">Узнать больше </a>
				</div>
			</div>
    </div>!-->
	<!--<a class="back buttom" href=" ">Все клиенты</a>!-->
	
	
	
