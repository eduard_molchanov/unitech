<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

// ищем id свойств

$prop = array();
$res = CIBlockProperty::GetList(array(), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y"));
while ($pf = $res->GetNext()) {
  $prop[$pf["CODE"]] = $pf;
}

?>
<div class = "vacancies">
<?foreach ($arResult['SECTIONS'] as $arSection) {
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
?>

<div class="vacancies_item">
<div class = "vacancies_item_title">
	<div class = "name"><?=$arSection['NAME']?></div>
			<?if (strlen($arSection['DESCRIPTION']) > 0) {?>
		<div class = "salary"><?=$arSection['DESCRIPTION']?></div>
		<?}?>
	<a href = "#form_summary"  class = "fancybox_form">Отправить резюме</a></div>
	<a href = "#form_summary"  class = "fancybox_form adaptiv_summary">Отправить резюме</a>
	<div class="description">
		<?if ($arSection['PICTURE']) {
		$img = CFile::GetFileArray($arSection['PICTURE']);
		?>
		<img src="<?=$img['SRC']?>" alt="<?=$img['ALT']?>" />
		<?}?>
		<div class = "description_name">Требования к кандидату:</div>
		<ul>
			<?for ($i=0; $i<count($arSection['UF_VAC_REQS']); $i++) {?>
				<?$sign = ($i == count($arSection['UF_VAC_REQS']) - 1  ? "." : ",")?>
				<li><?=$arSection['UF_VAC_REQS'][$i].$sign?></li>
			<?}?>
		</ul>
		<div class = "description_name">Основные обязанности:</div>
		<ul>
			<?for ($i=0; $i<count($arSection['UF_VAC_DUTIES']); $i++) {?>
				<?$sign = ($i == count($arSection['UF_VAC_DUTIES']) - 1  ? "." : ",")?>
				<li><?=$arSection['UF_VAC_DUTIES'][$i].$sign?></li>
			<?}?>
		</ul>
		<div class = "description_name">Условия работы:</div>
		<ul>
			<?for ($i=0; $i<count($arSection['UF_VAC_CONS']); $i++) {?>
				<?$sign = ($i == count($arSection['UF_VAC_CONS']) - 1  ? "." : ",")?>
				<li><?=$arSection['UF_VAC_CONS'][$i].$sign?></li>
			<?}?>
		</ul>
</div>
	</div>
<?}?>
</div>