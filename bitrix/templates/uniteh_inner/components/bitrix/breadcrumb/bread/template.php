<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();

$strReturn .= '<div class = "breadcrumbs"><div class = "container"><div class = "row"><div class = "col-lg-12"><ul class = "breadcrumbs_list">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	;

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
			
				<li>
				<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">
					<span>'.$title.'</span>
				</a>
				</li>
			';
	}
	else
	{
		$strReturn .= '
			<li>
				'.$arrow.'
				<span>'.$title.'</span>
			</li>';
	}
}

$strReturn .= '<div style="clear:both"></div></ul></div></div></div></div>';

return $strReturn;
?>

        
            
                
                 <!--   <ul class = "breadcrumbs_list">
                        <li><a href = " "><span>Главная</span></a></li>
                        <li><a href = " "><span>Услуги и решения</span></a></li>
                        <li><a href = " "><span>ИТ-колсантинг</span></a></li>
                        <li><span>Аудит ИТ-инфраструктуры и ИТ-процессов компании</span></li>
                    </ul>-->
              
       
