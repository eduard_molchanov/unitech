<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?> 
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?//$APPLICATION->ShowHead();?>
    <?$APPLICATION->ShowHeadStrings()?>
    <?$APPLICATION->ShowCSS()?>
<link href="/css/common.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/slick.css">
    <link rel="stylesheet" href="/css/feature-carousel.css">
    <link rel="stylesheet" href="/css/jquery.formstyler.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/slick-theme.css">
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<?$APPLICATION->ShowMeta("robots")?>
    <?$APPLICATION->ShowMeta("keywords")?>
    <?$APPLICATION->ShowMeta("description")?>

	<title><?$APPLICATION->ShowTitle()?></title>
	

</head>
	<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="adaptiv_menu">
		<div class = "adaptiv_menu_top">
			<div class="search_adaptiv_box">
				<form action ="/search/">
					<input type="text" name="q" >
					<input name="s" type="submit" value="">
				</form>		
			</div>	
			<div class="close_adaptiv"></div>
		</div>	
		<ul class = "adaptiv_list">
			<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"uniteh_top", 
						array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "2",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "top",
							"USE_EXT" => "N",
							"COMPONENT_TEMPLATE" => "uniteh_top"
						),
						false
					);?>
		</ul>
		<div class = "adaptiv_bottom">
			<div class="info_header">
				<span class="info_header_phone">+7 (495) 662-92-91</span>
				<span class="info_header_mail"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>info@unitechnologies.ru</a></span>  
				<?$APPLICATION->IncludeFile("/include/presentation.php")?>
			</div>
		</div>
	</div>	
    <div class = "wrap">
    <header>
                <div class = "header">
                    <div class = "container">
						<a class = "logo" href="http://www.unitechnologies.ru"><img src="/i/logo_top.png" alt=""></a> 
					  <ul class = "main_menu hidden_block">
						<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"uniteh_top", 
						array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "2",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "top",
							"USE_EXT" => "N",
							"COMPONENT_TEMPLATE" => "uniteh_top"
						),
						false
					);?>
				</ul>	
					<form class = "search_header_box" method = "GET" action ="/search/">
						<div class = "search_header"></div>
							<div class = "seach">
								<input class="search-query" type="text" name = "q" placeholder="Поиск">
							</div>
					</form>
					<div class = "hamburger"></div>		
					<div class="sm_overlay"></div>	
					
					<div class = "info_header">
						<?$APPLICATION->IncludeFile("/include/phone.php")?>
					</div>
					 <div class = "header_english_box"><a href = "/aboutcompany/" class = "header_english"><span>About Company</span></a> </div>
                </div>
</div>
</header>
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "bread", Array(
	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
	),
	false
);?>
      <div class = "wrap_content">
          <div class = "container">
              <div class = "row">
                  <div class = "col-ld-3 col-md-3 hidden_block">
                      <div class = "wrap_left">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"left_inner_menu",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "left",
		"USE_EXT" => "N"
	)
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "include_areas_template.php",
		"PATH" => SITE_DIR."include/news_left.php"
	),
false,
Array(
	'HIDE_ICONS' => 'Y'
)
);?>
<div class = "banner">
<?$APPLICATION->IncludeFile("/include/bunner.php")?>
</div>

	 </div>
	</div>
<div class = "content-index col-ld-9 col-md-9">
    <div class = "wrap_center">
        <h1><?$APPLICATION->ShowTitle(false);?></h1>

          