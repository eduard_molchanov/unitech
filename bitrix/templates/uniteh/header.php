<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?> 
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?/*$APPLICATION->ShowHead();*/?>
    <?$APPLICATION->ShowHeadStrings()?>
    <?$APPLICATION->ShowCSS()?>
<!--<link href="/css/common.css" type="text/css" rel="stylesheet" />-->
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/slick.css">
    <link rel="stylesheet" href="/css/feature-carousel.css">
    <link rel="stylesheet" href="/css/jquery.formstyler.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/slick-theme.css">
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
	<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" type="image/x-icon">
	<?$APPLICATION->ShowMeta("robots")?>
    <?$APPLICATION->ShowMeta("keywords")?>
    <?$APPLICATION->ShowMeta("description")?>

	<title><?$APPLICATION->ShowTitle()?></title>
	<!--[if lte IE 6]>
	<style type="text/css">
		
		#banner-overlay { 
			background-image: none;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>images/overlay.png', sizingMethod = 'crop'); 
		}
		
		div.product-overlay {
			background-image: none;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>images/product-overlay.png', sizingMethod = 'crop');
		}
		
	</style>
	<![endif]-->


		<div id="panel"><?$APPLICATION->ShowPanel();?></div>

</head>
	<body class = "main">
	<div class="adaptiv_menu">
		<div class = "adaptiv_menu_top">
			<div class="search_adaptiv_box">
				<form action ="/search/">
					<input class="adaptiv_search" type="text" name="q" >
					<input name="s" type="submit" value="" class="adaptiv_search__submit">
				</form>		
			</div>	
			<div class="close_adaptiv"></div>
		</div>	
		<ul class = "adaptiv_list">
			<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"uniteh_top", 
						array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "2",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "top",
							"USE_EXT" => "N",
							"COMPONENT_TEMPLATE" => "uniteh_top"
						),
						false
					);?>
		</ul>
		<div class = "adaptiv_bottom">
			<div class="info_header">
				<?$APPLICATION->IncludeFile("/include/phone.php")?>
				<?$APPLICATION->IncludeFile("/include/presentation.php")?>
			</div>
		</div>
	</div>	
	<div id="page-wrapper">
		<div class  = "wrap">
			  <header>
						<div class = "header">
							<div class = "container">
							   <a class = "logo"><img src="/i/logo_top.png" alt=""></a>
						<ul class = "main_menu hidden_block">
						<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"uniteh_top", 
						array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"DELAY" => "N",
							"MAX_LEVEL" => "2",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "top",
							"USE_EXT" => "N",
							"COMPONENT_TEMPLATE" => "uniteh_top"
						),
						false
					);?>
				</ul>	
			<form class = "search_header_box" method = "GET" action ="/search/">
                            <div class = "search_header"></div>
                            <div class = "seach">
                                <input class="search-query" type="text" name = "q" placeholder="Поиск">
                            </div>
			</form>	
					<div class = "hamburger"></div>		
					<div class="sm_overlay"></div>	
					<div class = "info_header">
                           <?$APPLICATION->IncludeFile("/include/phone.php")?>  </div>
							<div class = "header_english_box"><a href = "/aboutcompany/" class = "header_english"><span>About Company</span></a> </div>
                            
                </div>
        </div>
      </header>



				