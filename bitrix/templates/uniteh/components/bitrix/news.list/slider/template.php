<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//print_r($arResult)?>

	<div class="carousel-container">
		<div id="carousel" class  = "carousel_main_service">
	<?foreach ($arResult["ITEMS"] as $arItem):?>	
			<div class="carousel-feature">
		 	<img class="carousel-image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="Image Caption">
				  <div class="carousel-caption">
					  <div class = "carousel_table">
						   <div class = "carousel_cell">
							<div class = "title"><?=$arItem["NAME"]?></div>
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<div class = "text"><?echo $arItem["PREVIEW_TEXT"];?></div>
									<div class="carousel-feature_link">
                           <?if ($arItem["PROPERTIES"]["LINK_DETAIL"]["VALUE"]):?>
		 		           <a href="<?=$arItem["PROPERTIES"]["LINK_DETAIL"]["VALUE"]?>" class="about">Узнать больше</a>
						   <?endif;?>
						   <?if ($arItem["PROPERTIES"]["LINK_SECTION"]["VALUE"]):?>
						   <a href="<?=$arItem["PROPERTIES"]["LINK_SECTION"]["VALUE"]?>" class="section">В раздел</a>
						   <?endif;?>
				           
									</div>
								<?endif;?>
						  </div>
					  </div>
				  </div>
		 	</div>		  
		<?endforeach?>	
		</div>
		<div id="carousel_2" class  = "carousel_main_service">
	<?foreach ($arResult["ITEMS"] as $arItem):?>	
			<div class="carousel-feature">
		 	<img class="carousel-image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="Image Caption">
				  <div class="carousel-caption">
					  <div class = "carousel_table">
						   <div class = "carousel_cell">
							<div class = "title"><?=$arItem["NAME"]?></div>
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<div class = "text"><?echo $arItem["PREVIEW_TEXT"];?></div>
									<div class="carousel-feature_link">
										<a href=" " class="about">Узнать больше</a> <a href=" " class="section">В раздел</a>
									</div>
								<?endif;?>
						  </div>
					  </div>
				  </div>
		 	</div>		  
		<?endforeach?>	
		</div>
		                   <div id="carousel-left"></div>
                          <div id="carousel-right"></div>
                          <div class = "slider_before">
                        
	</div></div>
