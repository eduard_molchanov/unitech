<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if($arItem["DEPTH_LEVEL"] == 1):?>
<ul class = "footer_menu_box">
		<li<?if ($arItem["SELECTED"]):?> class="active"<?endif?>>
			<a href="<?=$arItem["LINK"]?>" class="footer_menu_title">
				<?=$arItem["TEXT"]?>
			</a>
			<?if($arItem["IS_PARENT"]):?>
				<ul>
			<?else:?>
				</li>
			<?endif?>
</ul>		
	<?else:?>
		<li<?if ($arItem["SELECTED"]):?> class="active"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></div></li>", ($previousLevel-1) );?>
<?endif?>


