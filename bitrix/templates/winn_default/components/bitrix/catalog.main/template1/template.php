<?// if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="catalog-main">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <p>
            <? if (is_array($arItem["PICTURE"])): ?>
                <img style="vertical-align:middle;" border="0" src="<?= $arItem["PICTURE"]["SRC"] ?>"
                     width="<?= $arItem["PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PICTURE"]["HEIGHT"] ?>"
                     alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>"/>
            <? endif ?>
            <? if ($arItem["LIST_PAGE_URL"]): ?>
                <a style="vertical-align:middle;" href="<?= $arItem["LIST_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
            <? else: ?>
                <span style="vertical-align:middle;"><?= $arItem["NAME"] ?></span>
            <? endif ?>
        </p>
    <? endforeach ?>
</div>
<!--    **************************************************************************************      -->
<div class="row col-12">

    <div class="col-6 col-md-6 my-3 my-md-4">

        <article class="product product--card" id="bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b" data-entity="item">


            <div class="product-image-wrapper mb-3">
                <a class="product-image-canvas" href="/site_nj/departments/motoproizvodstvo-tyuning/" title="Мотопроизводство: разработка, тюнинг, детейлинг" data-entity="image-wrapper">
                    <img class="product-image aos-init aos-animate" id="bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_pict" src="/upload/iblock/b07/voi2mqyo3j6tz7supedyef1lhj7e595d.jpg" alt="Мотопроизводство: разработка, тюнинг, детейлинг" title="Мотопроизводство: разработка, тюнинг, детейлинг" data-aos="zoom-in" data-aos-duration="500">
                </a>
            </div>

            <h4 class="product-title my-0 mb-md-2">
                <a class="text-body" href="/site_nj/departments/motoproizvodstvo-tyuning/" title="Мотопроизводство: разработка, тюнинг, детейлинг">

                    Мотопроизводство: разработка, тюнинг, детейлинг
                </a>
            </h4>

            <script>
                var obbx_3218110189_546_1676533a4586dcea66c3b61d68a9762b = new JCCatalogItem({'PRODUCT_TYPE':'0','SHOW_QUANTITY':'','SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':false,'PRODUCT_PREVIEW':false,'DISPLAY_COMPARE':'','BIG_DATA':'','TEMPLATE_THEME':'','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'546','NAME':'Мотопроизводство: разработка, тюнинг, детейлинг','DETAIL_PAGE_URL':'/site_nj/departments/motoproizvodstvo-tyuning/','PICT':{'ID':'2601','TIMESTAMP_X':'19.05.2021 15:29:56','MODULE_ID':'iblock','HEIGHT':'1600','WIDTH':'1600','FILE_SIZE':'275419','CONTENT_TYPE':'image/jpeg','SUBDIR':'iblock/b07','FILE_NAME':'voi2mqyo3j6tz7supedyef1lhj7e595d.jpg','ORIGINAL_NAME':'motoproizvodstvo-razrabotka-tyuning-deteyling.jpg','DESCRIPTION':'','HANDLER_ID':'','EXTERNAL_ID':'a39bca3e0c21a74485d8fb57b6a01d5a','SRC':'/upload/iblock/b07/voi2mqyo3j6tz7supedyef1lhj7e595d.jpg','UNSAFE_SRC':'/upload/iblock/b07/voi2mqyo3j6tz7supedyef1lhj7e595d.jpg','SAFE_SRC':'/upload/iblock/b07/voi2mqyo3j6tz7supedyef1lhj7e595d.jpg','ALT':'Мотопроизводство: разработка, тюнинг, детейлинг','TITLE':'Мотопроизводство: разработка, тюнинг, детейлинг'},'CAN_BUY':false,'CHECK_QUANTITY':'','MAX_QUANTITY':'','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'','ITEM_PRICES':[''],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':'','ITEM_QUANTITY_RANGE_SELECTED':'','ITEM_MEASURE_RATIOS':[{'RATIO':'1'}],'ITEM_MEASURE_RATIO_SELECTED':'0','MORE_PHOTO':'','MORE_PHOTO_COUNT':''},'BASKET':{'ADD_PROPS':false,'QUANTITY':'','PROPS':'','EMPTY_PROPS':true,'BASKET_URL':'','ADD_URL_TEMPLATE':'','BUY_URL_TEMPLATE':''},'VISUAL':{'ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b','PICT_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_pict','PICT_SLIDER_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_pict_slider','QUANTITY_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_quantity','QUANTITY_UP_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_quant_up','QUANTITY_DOWN_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_quant_down','PRICE_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_price','PRICE_OLD_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_price_old','PRICE_TOTAL_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_price_total','PRICE_ECONOMY_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_price_economy','BUY_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_buy_link','BASKET_PROP_DIV':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_basket_prop','BASKET_ACTIONS_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_basket_actions','NOT_AVAILABLE_MESS':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_not_avail','COMPARE_LINK_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_compare_link','SUBSCRIBE_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_subscribe','FAVORITE_ID':'bx_3218110189_546_1676533a4586dcea66c3b61d68a9762b_favorite'},'MESS':{'MESS_BTN_BUY':'<svg class=\"icon-cart icon-svg\"><use xlink:href=\"#svg-cart\"><\/use><\/svg>','MESS_BTN_INCART':'<svg class=\"icon-cart icon-svg\"><use xlink:href=\"#svg-check\"><\/use><\/svg>'},'PRODUCT_DISPLAY_MODE':'','USE_ENHANCED_ECOMMERCE':'','DATA_LAYER_NAME':'','BRAND_PROPERTY':'','SLIDER_SLIDE_COUNT':'4'});
            </script>
        </article>
    </div>
    <div class="col-6 col-md-6 my-3 my-md-4">

        <article class="product product--card" id="bx_3218110189_545_f0c09c5eb494b9eca93aea22ad9dcba4" data-entity="item">


            <div class="product-image-wrapper mb-3">
                <a class="product-image-canvas" href="/site_nj/departments/sudostroenie-yakhty-lodki-katera/" title="Судостроение: яхты, лодки, катера, вейвранеры" data-entity="image-wrapper">
                    <img class="product-image aos-init aos-animate" id="bx_3218110189_545_f0c09c5eb494b9eca93aea22ad9dcba4_pict" src="/upload/iblock/a78/jo9ph5f60nk98z390aa1at87yspmhng4.jpg" alt="Судостроение: яхты, лодки, катера, вейвранеры" title="Судостроение: яхты, лодки, катера, вейвранеры" data-aos="zoom-in" data-aos-duration="500">
                </a>
            </div>

            <h4 class="product-title my-0 mb-md-2">
                <a class="text-body" href="/site_nj/departments/sudostroenie-yakhty-lodki-katera/" title="Судостроение: яхты, лодки, катера, вейвранеры">

                    Судостроение: яхты, лодки, катера, вейвранеры
                </a>
            </h4>


        </article>
    </div>
</div>