<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

/** @global CDatabase $DB */

use Bitrix\Main\Localization\Loc;

?>
<style>
    #sel-year12 {
        text-align: right;
        /*display: none;*/
    }

    #sel-year12 select {
        border: 2px solid #49a58c !important;
        color: #317560;
    }

    #sel-year12 select:hover {
        background-color: #317560 !important;
        color: #fff !important;
    }
</style>
<!--    шблон фильтра года  -->

<!--<h1>42</h1>-->

<?php
//var_dump($arResult['YEARS']);
//var_dump($arParams['SHOW_TITLE']);
if (!strpos($_SERVER[REQUEST_URI], "?ELEMENT_ID=")) {

    if (is_array($arResult['YEARS']) && count($arResult['YEARS']) > 0): ?>

        <section id="sel-year12">
            <span>смотреть проекты за </span>
            <?php if ($arParams['SHOW_TITLE']): ?>
                <div class=""><?= Loc::getMessage('RS_FLYAWAY.RNA_FLYAWAY.ARCHIVE') ?></div>
            <?php endif; ?>
            <select onchange="window.location.href=this.options[this.selectedIndex].value">
                <option value="/projects/">Все</option>
                <?php foreach ($arResult['YEARS'] as $iYear => $arYear): ?>
                    <option value="<?= $arYear['ARCHIVE_URL'] ?>"
                        <?php
                        if ($_GET["YEAR"] == $arYear['NAME']) {
                            echo "selected";
                        }
                        ?>

                        <?php if ($arParams['SHOW_YEARS']): ?>
                            <a href="<?= $arYear['ARCHIVE_URL'] ?>">
                                <?= $arYear['NAME'] ?> (<?= $arYear['COUNT'] ?>)
                            </a>
                        <?php endif; ?>

                    </option>

                <?php endforeach; ?>

            </select>
            <span> год </span>
        </section>
    <?php endif;
}
?>

<!--    шблон фильтра года  -->