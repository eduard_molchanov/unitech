<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;

$request = Application::getInstance()->getContext()->getRequest();
$sCurPage = $APPLICATION->GetCurPage(true);


if (Loader::includeModule('redsign.winn')) {
//    $APPLICATION->AddBufferContent(function () use ($APPLICATION) {
    echo \Redsign\Winn\MyTemplate::getSiteFooter();
//    });
}
?>


<div id="footer-main" class="l-page__footer l-footer l-footer--dark py-4" data-section="interstitial">
    <div class="container">
        <div class="d-flex flex align-items-start align-items-md-center justify-content-between text-secondary mb-4">
            <div id="footer-menu" class="">
                <ul class="menu-line list-unstyled">

                    <li><a href="/company/"
                           class="<?php if ($_SERVER[REQUEST_URI] == "/company/") echo "font-weight-bolder" ?>">Компания</a>
                    </li>

                    <li><a href="/catalog/"
                           class="<?php if ($_SERVER[REQUEST_URI] == "/catalog/") echo "font-weight-bolder" ?>">Решения</a>
                    </li>

                    <li><a href="/services/"
                           class="<?php if ($_SERVER[REQUEST_URI] == "/services/") echo "font-weight-bolder" ?>">Направления</a>
                    </li>
                    <li><a href="/projects/"
                           class="<?php if ($_SERVER[REQUEST_URI] == "/projects/") echo "font-weight-bolder" ?>">Проекты</a>
                    </li>

                    <li><a href="/partnery/"
                           class="<?php if ($_SERVER[REQUEST_URI] == "/partnery/") echo "font-weight-bolder" ?>">Партнеры</a>
                    </li>

                    <li><a href="/presscenter/"
                           class="<?php if ($_SERVER[REQUEST_URI] == "/presscenter/") echo "font-weight-bolder" ?>">Новости</a>
                    </li>

                    <li><a href="/contacts/"
                           class="<?php if ($_SERVER[REQUEST_URI] == "/contacts/") echo "font-weight-bolder" ?>">Контакты</a>
                    </li>

                </ul>
            </div>
            <div class="up-float-button js-link-up btn btn-sm btn-link">
                <svg class="icon-svg">
                    <use xlink:href="#svg-chevron-up"></use>
                </svg>
            </div>
        </div>


        <hr class="mt-4 mb-6">
        <div class="mt-6 mb-4 pb-3">
            <div class="d-block my-4 py-2 py-md-0">
                <?php
                //                                echo SITE_DIR;
                $APPLICATION->IncludeFile(
                    SITE_DIR . '/include/footer/address.php',
                    array(),
                    array(
                        'SHOW_BORDER' => false
                    )
                );
                ?>
            </div>
            <div class="d-block my-4 py-2 py-md-0">
                <?php
                $APPLICATION->IncludeFile(
                    SITE_DIR . '/include/footer/phones.php',
                    array(),
                    array(
                        'SHOW_BORDER' => false
                    )
                );
                ?>
            </div>
            <div class="d-block my-4 py-2 py-md-0">
                <?php
                $APPLICATION->IncludeFile(
                    SITE_DIR . '/include/footer/emails.php',
                    array(),
                    array(
                        'SHOW_BORDER' => false
                    )
                );
                ?>
            </div>
        </div>
        <div class="d-flex flex justify-content-between text-secondary flex-column flex-md-row mt-6 mt-md-0">
            <div class="d-flex justify-content-between mb-5 pb-4 mb-md-0 pb-md-0">
                <div class="">
                    <?php
                    $APPLICATION->IncludeFile(
                        SITE_DIR . '/include/footer/socnet_links.php',
                        array(),
                        array(
                            'SHOW_BORDER' => false
                        )
                    );
                    ?>
                </div>
                <div class="d-md-none">
                    <?php
                    $APPLICATION->IncludeFile(
                        SITE_DIR . '/include/footer/site_selector.php',
                        array(),
                        array(
                            'SHOW_BORDER' => false
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="font-size-sm">
                <?php #REDSIGN_COPYRIGHT# ?>
                <?= Loc::getMessage('RS_FOOTER_COPYRIGHT', array('#CURRENT_YEAR#' => date('Y'))) ?>
            </div>
        </div>
    </div>
</div>

</div><?php // l-page ?>

<div class="scroll-notify js-scroll-hidden-compensate" data-scroll-notify="" data-aos="zoom-in" data-aos-duration="500"
     data-aos-delay="1000">
    <div class="scroll-notify__toddler"></div>
</div>
<?php $APPLICATION->IncludeFile(SITE_DIR . "include/template/body_end.php", array(), array("MODE" => "html")); ?>

</div>

</div>
</div>
<?php
$APPLICATION->IncludeFile(
    "include/panels/side-panel.php",
    array(),
    array('SHOW_BORDER' => false)
);
?>
<script>
        $(document).ready(function() {
            $(".l-header__hamburger").on("click", function () {
                $("body, html").addClass("overs");
            });
            $(".fancybox-close-small").on("click", function () {
                $("body, html").removeClass("overs");
            });
        });
</script>
</body>
</html>
