
<?if (!empty($arResult)):?>
    <ul class="menu-line list-unstyled">

        <?
        foreach($arResult as $arItem):
            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <?if($arItem["SELECTED"]):?>
            <li><a href="<?=$arItem["LINK"]?>" class="font-weight-bolder"><?=$arItem["TEXT"]?></a></li>
        <?else:?>
            <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
        <?endif?>

        <?endforeach?>

    </ul>
<?endif?>




<div class="l-page__footer l-footer l-footer--dark py-4" data-section="interstitial">
    <div class="container">
        <div class="d-flex flex align-items-start align-items-md-center justify-content-between text-secondary mb-4">
            <div class="">

                <ul class="menu-line list-unstyled">

                    <li><a href="/company/" class="font-weight-bolder">Компания</a></li>

                    <li><a href="/catalog/">Решения</a></li>

                    <li><a href="/services/">Направления</a></li>

                    <li><a href="/partnery/">Партнеры</a></li>

                    <li><a href="/news/">Новости</a></li>

                    <li><a href="/contacts/">Контакты</a></li>


                </ul>
            </div>
            <div class="up-float-button js-link-up btn btn-sm btn-link">
                <svg class="icon-svg">
                    <use xlink:href="#svg-chevron-up"></use>
                </svg>
            </div>
        </div>


        <hr class="mt-4 mb-6">
        <div class="mt-6 mb-4 pb-3">
            <div class="d-block my-4 py-2 py-md-0">
                <div class="d-flex align-items-center">
                    <div class="d-block mr-3">
                        <svg class="icon-svg text-primary d-block">
                            <use xlink:href="#svg-pin"></use>
                        </svg>
                    </div>
                    <div class="d-block">
                        г. Химки, ул. Рабочая, д. 2а, корп. 22а, офис 213а
                    </div>
                </div>
            </div>
            <div class="d-block my-4 py-2 py-md-0">
                <div class="d-flex align-items-center">
                    <div class="d-block mr-3">
                        <svg class="icon-svg text-primary">
                            <use xlink:href="#svg-phone-call"></use>
                        </svg>
                    </div>
                    <div class="d-block">
                        <a href="tel:+74956629291" class="d-block">+7 (495) 662-9291 </a>
                    </div>
                </div>
            </div>
            <div class="d-block my-4 py-2 py-md-0">
                <div class="d-flex align-items-center">
                    <div class="d-block mr-3">
                        <svg class="icon-svg text-primary">
                            <use xlink:href="#svg-email"></use>
                        </svg>
                    </div>
                    <div class="d-block">
                        <a href="mailto:info@unitechnologies.ru" class="d-block">info@unitechnologies.ru</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex justify-content-between text-secondary flex-column flex-md-row mt-6 mt-md-0">
            <div class="d-flex justify-content-between mb-5 pb-4 mb-md-0 pb-md-0">
                <div class="">
                    <ul class="list-inline list-unstyled">
                        <li class="list-inline-item">
                            <a class="text-secondary" target="_blank" href="https://www.facebook.com/1CBitrix"
                               rel="nofollow">
                                <svg class="icon-svg icon-color h5">
                                    <use xlink:href="#svg-facebook"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="text-secondary" target="_blank" href="https://twitter.com/1c_bitrix"
                               rel="nofollow">
                                <svg class="icon-svg icon-color h5">
                                    <use xlink:href="#svg-twitter"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="text-secondary" target="_blank" href="https://vk.com/bitrix_1c" rel="nofollow">
                                <svg class="icon-svg icon-color h5">
                                    <use xlink:href="#svg-vk"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="text-secondary" target="_blank"
                               href="https://www.youtube.com/user/1CBitrixChannel" rel="nofollow">
                                <svg class="icon-svg icon-color h5">
                                    <use xlink:href="#svg-youtube"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="text-secondary" target="_blank"
                               href="https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=41&amp;LESSON_ID=7285"
                               rel="nofollow">
                                <svg class="icon-svg icon-color h5">
                                    <use xlink:href="#svg-ok"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="d-md-none">
                </div>
            </div>
            <div class="font-size-sm">
                Разработано в <a href="https://www.redsign.ru/" target="_blank" style="color:currentColor">«АЛЬФА
                    Системс»</a></div>
        </div>
    </div>
</div>

<div class="container">
    <div class="d-flex flex align-items-start align-items-md-center justify-content-between text-secondary mb-4">
        <div class="">
            <ul class="menu-line list-unstyled">

                <li><a href="/company/" class="font-weight-bolder">Компания</a></li>

                <li><a href="/catalog/">Решения</a></li>

                <li><a href="/services/">Направления</a></li>

                <li><a href="/partnery/">Партнеры</a></li>

                <li><a href="/news/">Новости</a></li>

                <li><a href="/contacts/">Контакты</a></li>


            </ul>
        </div>
        <div class="up-float-button js-link-up btn btn-sm btn-link">
            <svg class="icon-svg">
                <use xlink:href="#svg-chevron-up"></use>
            </svg>
        </div>
    </div>


    <hr class="mt-4 mb-6">
    <div class="mt-6 mb-4 pb-3">
        <div class="d-block my-4 py-2 py-md-0">
            <div class="d-flex align-items-center">
                <div class="d-block mr-3">
                    <svg class="icon-svg text-primary d-block"><use xlink:href="#svg-pin"></use></svg>
                </div>
                <div class="d-block">
                    г. Химки, ул. Рабочая, д. 2а, корп. 22а,  офис 213а    </div>
            </div>            </div>
        <div class="d-block my-4 py-2 py-md-0">
            <div class="d-flex align-items-center">
                <div class="d-block mr-3">
                    <svg class="icon-svg text-primary"><use xlink:href="#svg-phone-call"></use></svg>
                </div>
                <div class="d-block">
                    <a href="tel:+74956629291" class="d-block">+7 (495) 662-9291 </a>
                </div>
            </div>            </div>
        <div class="d-block my-4 py-2 py-md-0">
            <div class="d-flex align-items-center">
                <div class="d-block mr-3">
                    <svg class="icon-svg text-primary"><use xlink:href="#svg-email"></use></svg>
                </div>
                <div class="d-block">
                    <a href="mailto:info@unitechnologies.ru" class="d-block">info@unitechnologies.ru</a>
                </div>
            </div>            </div>
    </div>
    <div class="d-flex flex justify-content-between text-secondary flex-column flex-md-row mt-6 mt-md-0">
        <div class="d-flex justify-content-between mb-5 pb-4 mb-md-0 pb-md-0">
            <div class="">
                <ul class="list-inline list-unstyled">
                    <li class="list-inline-item">
                        <a class="text-secondary" target="_blank" href="https://www.facebook.com/1CBitrix" rel="nofollow">
                            <svg class="icon-svg icon-color h5"><use xlink:href="#svg-facebook"></use></svg>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-secondary" target="_blank" href="https://twitter.com/1c_bitrix" rel="nofollow">
                            <svg class="icon-svg icon-color h5"><use xlink:href="#svg-twitter"></use></svg>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-secondary" target="_blank" href="https://vk.com/bitrix_1c" rel="nofollow">
                            <svg class="icon-svg icon-color h5"><use xlink:href="#svg-vk"></use></svg>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-secondary" target="_blank" href="https://www.youtube.com/user/1CBitrixChannel" rel="nofollow">
                            <svg class="icon-svg icon-color h5"><use xlink:href="#svg-youtube"></use></svg>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-secondary" target="_blank" href="https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=41&amp;LESSON_ID=7285" rel="nofollow">
                            <svg class="icon-svg icon-color h5"><use xlink:href="#svg-ok"></use></svg>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="d-md-none">
            </div>
        </div>
        <div class="font-size-sm">
            Разработано в <a href="https://www.redsign.ru/" target="_blank" style="color:currentColor">«АЛЬФА Системс»</a>            </div>
    </div>
</div>