<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
if (!function_exists(deleteDirectory)) {
	function deleteDirectory($dirPath) {
		if ($dirPath == '/')
			return;
		if (is_dir($dirPath)) {
			$objects = scandir($dirPath);
			foreach ($objects as $object) {
				if ($object != "." && $object !="..") {
					if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
						deleteDirectory($dirPath . DIRECTORY_SEPARATOR . $object);
					} else {
						unlink($dirPath . DIRECTORY_SEPARATOR . $object);
					}
				}
			}
		reset($objects);
		rmdir($dirPath);
		}
	}
}
if ($arParams['INCLUDE_JQUERY'] == 'Y') 
	$APPLICATION->AddHeadScript($componentPath.'/js/jquery-1.11.3.min.js');

if ($arParams['INCLUDE_JQUERY_UI'] == 'Y') 
	$APPLICATION->AddHeadScript($componentPath.'/js/jquery-ui.min.js');

if ($arParams['INCLUDE_FANCYBOX'] == 'Y') 
	$APPLICATION->AddHeadScript($componentPath.'/js/jquery.fancybox.pack.js');

$APPLICATION->AddHeadScript($componentPath.'/js/wjQWF.js');
$APPLICATION->AddHeadScript($componentPath.'/js/fileuploader.js');

$arResult['EVAL_URL'] = $this->GetPath().'/eval.php';
$arResult['PARAMS_HASH'] = md5(serialize($arParams).$this->GetTemplateName());

$params2session = array();
$params2session['EMAIL_TO'] = ($arParams["EMAIL_TO"] == '') ? COption::GetOptionString("main", "email_from") : $arParams['EMAIL_TO'];
$params2session['EVENT_MESSAGE_ID'] = $arParams["EVENT_MESSAGE_ID"];
$params2session['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
$params2session['PUT_ACTIVE'] = $arParams['PUT_ACTIVE'];
$params2session['OK_TEXT'] = $arParams['OK_TEXT'];
$params2session['FORM_THEME'] = $arParams['FORM_THEME'];

$fields = array();

foreach ($arParams['FIELDNAMES'] as $fieldname) {

	if (strlen($fieldname) > 0) {
		$fields[$fieldname]['REQUIRED'] = $arParams[$fieldname.'_REQUIRED'];
		$fields[$fieldname]['CHECK_FUNCTION'] = $arParams[$fieldname.'_CHECK_FUNCTION'];
		$fields[$fieldname]['IBLOCK_FIELD'] = $arParams[$fieldname.'_IBLOCK_FIELD'];
		$fields[$fieldname]['CAPTION'] = $arParams[$fieldname.'_CAPTION'];
		$fields[$fieldname]['INPUT_TYPE'] = $arParams[$fieldname.'_INPUT_TYPE'];
		$fields[$fieldname]['DEFAULT_VALUE'] = $arParams[$fieldname.'_DEFAULT_VALUE'];
		$fields[$fieldname]['VALUES'] = $arParams[$fieldname.'_VALUES'];
		$fields[$fieldname]['ERROR_MESSAGE_NOTFILLED'] = strlen($arParams[$fieldname.'_ERROR_MESSAGE_NOTFILLED']) > 0 ? $arParams[$fieldname.'_ERROR_MESSAGE_NOTFILLED'] : 'FIELD NOT FILLED';
		//echo "</br>ERROR_MESSAGE_NOTFILLED".$fields[$fieldname]['ERROR_MESSAGE_NOTFILLED'];
		$fields[$fieldname]['ERROR_MESSAGE_INCORRECT'] = strlen($arParams[$fieldname.'_ERROR_MESSAGE_INCORRECT']) > 0 ? $arParams[$fieldname.'_ERROR_MESSAGE_INCORRECT'] : 'FIELD INCORRECT';
		//echo "</br>ERROR_MESSAGE_INCORRECT".$fields[$fieldname]['ERROR_MESSAGE_INCORRECT'];
	}
	
}


$params2session['FIELDS'] = $fields;


if (isset($_SESSION['wjQWF'][$arResult['PARAMS_HASH']])) {
	deleteDirectory($_SERVER['DOCUMENT_ROOT'].'/upload/webrost/'.$arResult['PARAMS_HASH']);
}

$_SESSION['wjQWF'][$arResult['PARAMS_HASH']] = $params2session;


$this->IncludeComponentTemplate();
?>