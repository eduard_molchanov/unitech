<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($_POST['FIELDNAMES'])) $_POST['FIELDNAMES'] = $arCurrentValues['FIELDNAMES'];

$site = ($_REQUEST["site"] <> ''? $_REQUEST["site"] : ($_REQUEST["src_site"] <> ''? $_REQUEST["src_site"] : false));
$arFilter = Array("TYPE_ID" => "FEEDBACK_FORM", "ACTIVE" => "Y");
if($site !== false)
	$arFilter["LID"] = $site;

$arEvent = Array();
$dbType = CEventMessage::GetList($by="ID", $order="DESC", $arFilter);
while($arType = $dbType->GetNext())
	$arEvent[$arType["ID"]] = "[".$arType["ID"]."] ".$arType["SUBJECT"];

if(!CModule::IncludeModule("iblock"))
	return;
	
$arIBlocks=Array(0 => GetMessage('CWFAP_DONT_ADD_IBLOCK'));
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arPropList = array();
$arFieldList = array(	
						'NAME' =>  GetMessage('CWFAP_FIELD_LIST_NAME'), 'PREVIEW_TEXT' => GetMessage('CWFAP_FIELD_LIST_PREVIEW_TEXT'), 'DETAIL_TEXT' => GetMessage('CWFAP_FIELD_LIST_DETAIL_TEXT'), 
						'ACTIVE_FROM' => GetMessage('CWFAP_FIELD_LIST_ACTIVE_FROM'), 'ACTIVE_TO' => GetMessage('CWFAP_FIELD_LIST_ACTIVE_TO'), 
						'PREVIEW_PICTURE' => GetMessage('CWFAP_FIELD_LIST_PREVIEW_PICTURE'), 'DETAIL_PICTURE' => GetMessage('CWFAP_FIELD_LIST_DETAIL_PICTURE'),
					);
if (empty($_POST['IBLOCK_ID'])) $_POST['IBLOCK_ID'] = $arCurrentValues['IBLOCK_ID'];					
if (isset($_POST['IBLOCK_ID'])) {
	$rsPropsList = CIBlock::GetProperties($_POST['IBLOCK_ID']);
	while ($arProp = $rsPropsList->Fetch()) {
		$arPropList['PROPERTY_'.$arProp['CODE']] = $arProp['NAME'].' ('.$arProp['CODE'].')';
	}
	$arFieldList = $arFieldList + $arPropList; 
} 

$arGroups = array();
$arFields = array();
foreach ($_POST['FIELDNAMES'] as $fieldname) {
	if (strlen($fieldname) > 0) {
		$arGroups['FIELD_'.$fieldname] = array('NAME' => GetMessage('CWFAP_FIELD').' '.$fieldname);
		$arFields[$fieldname.'_REQUIRED'] = array(
				'NAME' => GetMessage('CWFAP_REQUIRED'),
				'TYPE' => 'CHECKBOX',
				'PARENT' => 'FIELD_'.$fieldname,
			);
		$arFields[$fieldname.'_CAPTION'] = array(
				'NAME' => GetMessage('CWFAP_CAPTION'),
				'TYPE' => 'STRING',
				'PARENT' => 'FIELD_'.$fieldname,
			);
		$arFields[$fieldname.'_IBLOCK_FIELD'] = array(
				'NAME' => GetMessage('CWFAP_IBLOCK_FIELD'),
				'TYPE' => 'LIST',
				'PARENT' => 'FIELD_'.$fieldname,
				'VALUES' => $arFieldList,
			);
		$arFields[$fieldname.'_INPUT_TYPE'] = array(
				'NAME' => GetMessage('CWFAP_INPUT_TYPE'),
				'TYPE' => 'LIST',
				'VALUES' => array('input'=>GetMessage('CWFAP_INPUT_TYPES_INPUT'), 'textarea'=>GetMessage('CWFAP_INPUT_TYPES_TEXTAREA'), 'select'=>GetMessage('CWFAP_INPUT_TYPES_SELECT'), 'hidden'=>GetMessage('CWFAP_INPUT_TYPES_HIDDEN'), 'file' => GetMessage('CWFAP_INPUT_TYPES_FILE')),
				'PARENT' => 'FIELD_'.$fieldname,
			);					
		$arFields[$fieldname.'_DEFAULT_VALUE'] = array(
				'NAME' => GetMessage('CWFAP_DEFAULT_VALUE'),
				'TYPE' => 'STRING',
				'PARENT' => 'FIELD_'.$fieldname,
			);				
		$arFields[$fieldname.'_ERROR_MESSAGE_NOTFILLED'] = array(
				'NAME' => GetMessage('CWFAP_ERROR_MESSAGE_NOTFILLED'),
				'TYPE' => 'STRING',
				'PARENT' => 'FIELD_'.$fieldname,
			);
		$arFields[$fieldname.'_ERROR_MESSAGE_INCORRECT'] = array(
				'NAME' => GetMessage('CWFAP_ERROR_MESSAGE_INCORRECT'),
				'TYPE' => 'STRING',
				'PARENT' => 'FIELD_'.$fieldname,
			);			
		$arFields[$fieldname.'_CHECK_FUNCTION'] = array(
				'NAME' => GetMessage('CWFAP_CHECK_FUNCTION'),
				'TYPE' => 'STRING',
				'PARENT' => 'FIELD_'.$fieldname,
			);
	}
}

$arComponentParameters = array(
	"GROUPS" => array_merge(
		array(
			"JQUERY" => array("NAME" => GetMessage('CWFAP_JQUERY')),
			"PARAMS" => array("NAME" => GetMessage('CWFAP_PARAMS')),
			"FIELDS" => array("NAME" => GetMessage('CWFAP_FIELDS')),
		),
		$arGroups
	),
	"PARAMETERS" => array_merge(
		array(
/* 			"USE_CAPTCHA" => Array(
				"PARENT" => "PARAMS",
				"NAME" => GetMessage("CWFAP_CAPTCHA"), 
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "Y", 
			), */
			"INCLUDE_JQUERY" => array (
				"PARENT" => "JQUERY",
				"NAME" => GetMessage("CWFAP_INCLUDE_JQUERY"),
				"TYPE" => "CHECKBOX",
			),
			"INCLUDE_JQUERY_UI" => array (
				"PARENT" => "JQUERY",
				"NAME" => GetMessage("CWFAP_INCLUDE_JQUERY_UI"),
				"TYPE" => "CHECKBOX",
			),		
			"INCLUDE_FANCYBOX" => array (
				"PARENT" => "JQUERY",
				"NAME" => GetMessage("CWFAP_INCLUDE_FANCYBOX"),
				"TYPE" => "CHECKBOX",
			),				
			"OK_TEXT" => Array(
				"PARENT" => "PARAMS",
				"NAME" => GetMessage("CWFAP_OK_MESSAGE"), 
				"TYPE" => "STRING",
				"DEFAULT" => GetMessage("CWFAP_OK_TEXT"), 
			),
			"EMAIL_TO" => Array(
				"PARENT" => "PARAMS",
				"NAME" => GetMessage("CWFAP_EMAIL_TO"), 
				"TYPE" => "STRING",
				"DEFAULT" => htmlspecialcharsbx(COption::GetOptionString("main", "email_from")), 
			),
			"EVENT_MESSAGE_ID" => Array(
				"PARENT" => "PARAMS",
				"NAME" => GetMessage("CWFAP_EMAIL_TEMPLATES"), 
				"TYPE"=>"LIST", 
				"VALUES" => $arEvent,
				"DEFAULT"=>"", 
				"MULTIPLE"=>"Y", 
				"COLS"=>25, 
			),
			"IBLOCK_ID" => Array(
				"PARENT" => "PARAMS",
				"NAME" => GetMessage("CWFAP_IBLOCK_ID"), 
				"TYPE" => "LIST",
				"DEFAULT" => '',
				"REFRESH" => "Y",
				"VALUES" => $arIBlocks
			),
			"PUT_ACTIVE" => Array(
				"PARENT" => "PARAMS",
				"NAME" => GetMessage('CWFAP_ADD_ACTIVE'), 
				"TYPE" => "CHECKBOX",
			),
			"FIELDNAMES" => array (
				"NAME" => GetMessage('CWFAP_FIELD_NAME'),
				"TYPE" => "STRING",
				"MULTIPLE" => "Y",
				"REFRESH" => "Y",
				"PARENT" => "FIELDS"
			),	
		),
		$arFields
	)
);

?>