(function( $ ) {
	$.widget( "custom.wjQWF", {
	_input_css_class: 'wjQWF-input',
	_reset: false,
	_jQ_content_window: {},
	_jQ_header: {},
	_jQ_okbox: {},
	_jQ_errbox: {},
	_jQ_form: {},
	_jQ_send_btn: {},
	_result: false,
	
	// Опции по умолчанию
	options: {
		content_window_css : 'wjQWF-content', //классы окна виджета через ' '
		header : '<div class="wjQWF-title">Форма обратной связи</div>', //любой блоковый элемент
		footer : '',
		okbox : {
			html: '<div class="wjQWF-ok-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщения
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide',
		},
		errbox : {
			html: '<div class="wjQWF-err-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщений
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		form: {
			html: '<div class="wjQWF-form"></div>', //любой блоковый элемен
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		default_star: '<span>*</span>',
		default_inputs:{
					/*#DOM_ID# - id элемента, #FIELD_CAPTION# - текст метки, #FIELD_REQUIRED# (=<span>*</span>), #FIELD_NAME# имя инпута*/
					//полный пример
					/*input: '<fieldset><label for="#DOM_ID#">#FIELD_CAPTION##FIELD_REQUIRED#</label><span class="wjQWF-err-field wjQWF-#FIELD_NAME#-err"></span><input type="text" name="#FIELD_NAME#" id="#DOM_ID#" value="#FIELD_VALUE#"></fieldset>',*/
					hidden: '<input type="hidden" id=#DOM_ID# name="#FIELD_NAME#" value="#FIELD_VALUE#">',
					input: '<fieldset><label for="#DOM_ID#">#FIELD_CAPTION##FIELD_REQUIRED#</label><input type="text" name="#FIELD_NAME#" id="#DOM_ID#" value="#FIELD_VALUE#"></fieldset>',
					textarea: '<fieldset><label for="#DOM_ID#">#FIELD_CAPTION##FIELD_REQUIRED#</label><textarea name="#FIELD_NAME#" id="#DOM_ID#">#FIELD_VALUE#</textarea></fieldset>', 
					select: '<fieldset><label for="#DOM_ID#">#FIELD_CAPTION##FIELD_REQUIRED#</label><select name="#FIELD_NAME#" id="#DOM_ID#"></select></fieldset>', 
					file: '<fieldset><label for="#DOM_ID#">#FIELD_CAPTION##FIELD_REQUIRED#</label><span class="wjQWF-err-field wjQWF-#FIELD_NAME#-err"><div id="#DOM_ID#"></div></fieldset>',
		},
		groups: {}, //[{inputs: ['NAME', 'EMAIL'], html: '<div class="g1"></div>'},	{inputs: ['REVIEW'], html: '<div class="g2"></div>'}]
		inputs: { //переопределение конкретных input'ов, названия - имена полей в компноненте (массив FIELDNAMES)
		


		},
		send_btn : {
			html: '<button>Отправить</button>',
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		bx_data : {
			sessid: '', //<?=bitrix_sessid_post()?>
			params: '', //<?=json_encode($arParams)?>
			templatename: '', //<?=$templateName?>
			eval_url: '' //<?=$arResult['EVAL_URL']?>
		},
		callbacks: {
			beforeSend: function () {
				return true;
			},
			beforeLoad: function () {},
			beforeDraw: function () {}, 
			afterLoad : function () {}, 
			afterDraw : function () {}, 
            applyResult: false,
		}
	},
 
    _isFunction: function isFunction(functionToCheck) {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    },
	// the constructor
	_create: function() {
		//Создаем объекты jQuery и добавляем их к окну
		this._jQ_content_window = $('#'+this.element.attr('id'));
		this._jQ_content_window.addClass(this.options.content_window_css);
		this._jQ_header = $(this.options.header);
		this._jQ_content_window.append(this._jQ_header);
		this._jQ_okbox = $(this.options.okbox.html).addClass(this.options.okbox.off_class);
		this._jQ_content_window.append(this._jQ_okbox);
		this._jQ_errbox = $(this.options.errbox.html).addClass(this.options.errbox.off_class);
		this._jQ_content_window.append(this._jQ_errbox);
		this._jQ_form = $(this.options.form.html).addClass(this.options.form.on_class);
		this._jQ_content_window.append(this._jQ_form);
		this._jQ_send_btn = $(this.options.send_btn.html);
		this._jQ_content_window.append(this._jQ_send_btn);
		this._jQ_send_btn.click(function () {
			this.sendForm();
		}.bind(this));
		//this.loadForm();
		this.initForm();
	},

	initForm: function () {
		$.ajax({
			url: this.options.bx_data.eval_url,
			type: 'post',
			data: {
				sessid: this.options.bx_data.sessid, 
				params_hash: this.options.bx_data.params_hash, 
				request_type: 'init'
			},
			context: this,
			success: function (answer) {
				var arResult = JSON.parse(answer);
				this.drawForm(arResult);
			}
		});
	},
	
	drawForm: function (arResult) {
		//Вывод групп
		for (var j=0; j < this.options.groups.length; j++) {
			this.options.groups[j].jQ_obj = $(this.options.groups[j].html);
			this._jQ_form.append(this.options.groups[j].jQ_obj);	
		}
		var uploader_inputs = [];
		//Вывод полей
		if (!('FIELDS' in arResult))
			return;
		for (var i=0; i<arResult.FIELDS.length; i++) {
			var input;
			if (arResult.FIELDS[i].NAME in this.options.inputs) {
				if (arResult.FIELDS[i].REQUIRED === 'Y')
					input = this.options.inputs[arResult.FIELDS[i].NAME].req;
				else
					input = this.options.inputs[arResult.FIELDS[i].NAME].dom;				
			}
			else {
				input = this.options.default_inputs[arResult.FIELDS[i].INPUT_TYPE];
				var id =  this.element.attr('id') + '-' + arResult.FIELDS[i].NAME;
				input = input.replace(/#DOM_ID#/g, id); //#DOM_ID# - id элемента, #FIELD_CAPTION# - текст метки, #FIELD_REQUIRED# (=<span>*</span>), #FIELD_NAME# имя инпута
				input = input.replace(/#FIELD_NAME#/g, arResult.FIELDS[i].NAME); 
				input = input.replace(/#FIELD_CAPTION#/g, arResult.FIELDS[i].CAPTION);
				input = ('VALUE' in arResult.FIELDS[i]) ? input.replace(/#FIELD_VALUE#/g, arResult.FIELDS[i].VALUE) : input.replace(/#FIELD_VALUE#/g, '');
				input = (arResult.FIELDS[i].REQUIRED === 'Y') ? input.replace(/#FIELD_REQUIRED#/g, this.options.default_star) : input.replace(/#FIELD_REQUIRED#/g, '');	
			}

			//присваивание значений
			jQ_input = $(input);
			//Если это select заполняем возможные значения
			if ((arResult.FIELDS[i].INPUT_TYPE == 'select') && (arResult.FIELDS[i].VALUES != null))
				for (var si = 0; si < arResult.FIELDS[i].VALUES.length; si++) {
					jQ_input.find('select').append('<option value="'+arResult.FIELDS[i].VALUES[si].VALUE+'">'+arResult.FIELDS[i].VALUES[si].DESCRIPTION+'</option>');
				}
			var appended = false;
			jQ_input.find('input, textarea, select').val(arResult.FIELDS[i].VALUE);
			
			//Размещение по группам
			for (var j=0; j < this.options.groups.length; j++) {
				for (var k=0; k < this.options.groups[j].inputs.length; k++) {
					if (arResult.FIELDS[i].NAME == this.options.groups[j].inputs[k]) {
						this.options.groups[j].jQ_obj.append(jQ_input);
						appended = true;
						break;
					}
				}
				if (appended)
					break;
			}
			if (!appended)
				this._jQ_form.append(jQ_input);
			if (arResult.FIELDS[i].INPUT_TYPE == 'file')
				uploader_inputs.push(arResult.FIELDS[i].NAME);
		}
		this._jQ_form.find('input, textarea, select').addClass(this._input_css_class);
		this._jQ_form.append(this.options.footer);
		this._attachFileUploader(uploader_inputs);
		this._afterDraw();
	},
	
 	_showError: function (field, message) {
		if (this._jQ_form.find('.wjQWF-'+field+'-err').length > 0)
			this._jQ_form.find('.wjQWF-'+field+'-err').text(message);
		else
			this._jQ_errbox.append($(this.options.errbox.message).append(message));
	},
	
	applyResult: function (arResult) {
		//Показываем форму и кнопку, если они были скрыты
		this._jQ_form.removeClass(this.options.form.off_class).addClass(this.options.form.on_class);
		this._jQ_send_btn.removeClass(this.options.send_btn.off_class).addClass(this.options.send_btn.on_class);
		//Очистка сообщений
		this._jQ_okbox.empty();
		//Вывод сообщения от сервера, в зависимости от результата
		if (arResult.STATUS == 'OK') {
			//$('.wjQWF-ok-box').closest(".fancy-popup").addClass("success");  //class верхнему div для заказать звонок
			//$('.wjQWF-ok-box').closest(".form-review").addClass("success");  //class верхнему div для отзывы
			//$('.wjQWF-ok-box').closest(".form-feedback").addClass("success");  //class верхнему div для контакты
			this._jQ_okbox.removeClass(this.options.okbox.off_class).addClass(this.options.okbox.on_class);
			this._jQ_okbox.append($(this.options.okbox.message).append(arResult.OK_MESSAGE));
			this._jQ_errbox.removeClass(this.options.errbox.on_class).addClass(this.options.errbox.off_class);
			this._jQ_send_btn.removeClass(this.options.send_btn.on_class).addClass(this.options.send_btn.off_class);
			this._jQ_form.removeClass(this.options.form.on_class).addClass(this.options.form.off_class);
		}
		else {
			if (arResult.STATUS == 'ERR') {
				this._jQ_errbox.removeClass(this.options.errbox.off_class).addClass(this.options.errbox.on_class);
				this._jQ_errbox.empty();
				this._jQ_form.find('.wjQWF-err-field').text();
				//Добавить соообщения не связанные с полями
				if ('OTHERS' in arResult.ERRORS)
					for (msg in arResult.ERRORS.OTHERS) {
						this._showError('other', msg);
					}
				//Сообщения связанные с полями
				for (field in arResult.ERRORS.FIELDS) {
					this._showError(field, arResult.ERRORS.FIELDS[field]);
				}
			}
		}
	},
	
	resetForm: function () {
		this._jQ_content_window.empty();
		this._create();
		this._reset = false;
	},
	
	//Метод загрузки (связь с битрикс)
	sendForm: function () {
		var send_data = {sessid: this.options.bx_data.sessid, params_hash: this.options.bx_data.params_hash};
		if (!this._reset) {
			this._jQ_form.find(' .'+this._input_css_class).each(function () {
				send_data[this.name] = $(this).val();
			});

			send_data.confirm_submit = "confirmed";
			if (!this._beforeSend(send_data)) return;
			
			
		}
		else {
			this._reset = false;
			this._result = false;
		}
		
		$.ajax({
			url: this.options.bx_data.eval_url,
			data: send_data,
			type: 'POST',
			context: this,
			success: function (data) {
				var arResult = JSON.parse(data);
				if (arResult['STATUS'] == 'OK') {
					this._result = 'OK';
					this._reset = true;
				} else {
					if (arResult['STATUS'] == 'ERR') {
						this._result = 'ERROR';
					}
				}
				this._afterLoad();
                //если есть такая функиця
                if (this._isFunction(this.options.callbacks.applyResult))
                    this.options.callbacks.applyResult.apply(this, [arResult]);
                else
                    this.applyResult(arResult);
			},
		});
	},
	
	_attachFileUploader: function (input_names) {
		for (var i=0; i<input_names.length; i++) {
			var id = this.element.attr('id') + '-' + input_names[i];
			var uploader = new qq.FileUploader({
				element: $('#' + id)[0],
				action: this.options.bx_data.eval_url,
				params: {sessid: this.options.bx_data.sessid, params_hash: this.options.bx_data.params_hash, field: input_names[i], upload: 'confirmed'},
				multiple: false,
				uploadButtonText: 'Обзор ...',
				allowedExtensions: ['jpg', 'jpeg', 'png'], //перенести в опции
				acceptFiles: 'image/jpeg, image/png', //перенести в опции
				template:	'<div class="qq-uploader">' +
								'<ul class="qq-upload-list"></ul>' +
								'<div class="qq-upload-drop-area"><span>{dragText}</span></div>' +
								'<div class="qq-upload-button">{uploadButtonText}</div>' +
							'</div>',		
				fileTemplate: '<li>' +
						'<span class="qq-progress-bar"></span>' +
						'<span class="qq-upload-file"></span>' +
						'<span class="qq-upload-spinner"></span>' +
						'<span class="qq-upload-size"></span>' +
						'<a class="qq-upload-cancel" href="#">{cancelButtonText}</a>' +
						'<span class="qq-upload-failed-text">{failUploadtext}</span>' +
					'</li>',	
				messages: {
					typeError: "Поддерживаются только файлы с расширением {extensions}.",
					sizeError: "{file} слишком большой, максимальный размер файла {sizeLimit}.",
					minSizeError: "{file} слишком маленький, минимальный размер файла {minSizeLimit}.",
					emptyError: "{file} - пустой, выберите другой файл.",
					onLeave: "Файл все еще загружается, если покинете страницу сейчас, загрузка будет отменена."
				},					
			}); 
		}
	},
	
	_beforeSend: function (data) {
		return this.options.callbacks.beforeSend.apply(this, [data]);
	},
	
	_beforeLoad: function (arParams) {
		this.options.callbacks.beforeLoad.apply(this, [arParams]);
	},
	
	_beforeDraw: function () {
		this.options.callbacks.beforeDraw.apply(this);
	},
	
	_afterLoad: function () {
		this.options.callbacks.afterLoad.apply(this);
	},
	
	_afterDraw: function () {
		this.options.callbacks.afterDraw.apply(this);
	},
	
	toReset: function () {
		return this._reset;
	},
	getResult: function () {
		return this._result;
	}
  })
}( jQuery ) );