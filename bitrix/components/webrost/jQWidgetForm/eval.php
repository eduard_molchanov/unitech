<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//include($_SERVER["DOCUMENT_ROOT"]."/bitrix/components/webrost/poly.ajax.form/lang/ru/component.php");
function deleteDirectory($dirPath) {
	if ($dirPath == '/')
		return;
    if (is_dir($dirPath)) {
        $objects = scandir($dirPath);
        foreach ($objects as $object) {
            if ($object != "." && $object !="..") {
                if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                    deleteDirectory($dirPath . DIRECTORY_SEPARATOR . $object);
                } else {
                    unlink($dirPath . DIRECTORY_SEPARATOR . $object);
                }
            }
        }
    reset($objects);
    rmdir($dirPath);
    }
}
function CheckEmail($var){
	if (preg_match('/^[а-яА-Яa-zA-Z0-9_\.\-]+@[а-яА-Яa-zA-Z0-9\-]+\.[а-яА-Яa-zA-Z\-\.]+$/Du', $var))
	{		
		return true;
	}
	else{
		return false;
	}

}

function CheckPhone($var){
	if (preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $var))
	{		
		return true;
	}
	else{
		return false;
	}

}

// function CheckPhone($var)
// {
	// if (preg_match('/^[а-яА-Яa-zA-Z0-9_\.\-]+@[а-яА-Яa-zA-Z0-9\-]+\.[а-яА-Яa-zA-Z\-\.]+$/Du', $var)){		
		// return true;
	// }
	// else{
		// return false;
	// }

// }
 
/**
 * Bitrix vars
 *
 * @var array $arParams 
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
/* 	if (!defined('BX_UTF'))
		$mf_sess_exp = iconv('utf-8', 'cp1251', "Ваша сессия истекла. Обновите страницу.");
	else  */
		$mf_sess_exp = "Ваша сессия истекла. Обновите страницу.";
//Загрузка файлов		
if($_SERVER["REQUEST_METHOD"] == "POST" && $_GET["upload"] == 'confirmed') {
	$_REQUEST['sessid'] = $_GET['sessid'];
	if ((!isset($_SESSION['wjQWF'][$_GET["params_hash"]])) && (!check_bitrix_sessid())) {
		$arResult["ERRORS"]["OTHER"] = $mf_sess_exp;
		echo Bitrix\Main\Web\Json::encode($arResult);
		return;
	}
	$dir = $_SERVER['DOCUMENT_ROOT'].'/upload/webrost/'.$_GET["params_hash"];
	
	if (file_exists($dir))
		deleteDirectory($dir);
	
	if (mkdir ($dir, 0777, true)) {
		require('uploader.php');
		$allowedExtensions = array('jpeg', 'jpg', 'png');
		$sizeLimit = 1 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$arResult = $uploader->handleUpload($dir.'/');
		
		$ext = pathinfo($uploader->getName(), PATHINFO_EXTENSION);
		$new_name = md5($uploader->getName()).'.'.$ext;
		rename($dir.'/'.$uploader->getName(), $dir.'/'.$new_name);
		$_SESSION['wjQWF'][$_GET["params_hash"]]['FILES'] = array();
		$_SESSION['wjQWF'][$_GET["params_hash"]]['FILES'][$_GET['field']] = $dir.'/'.$new_name;
	} else {
		$arResult['ERRORS']['OTHER'] = 'FOLDER ERROR';
	}
	echo Bitrix\Main\Web\Json::encode($arResult);
	return;
}		
			
if (!isset($_SESSION['wjQWF'][$_POST["params_hash"]])) {
	$arResult["ERRORS"]["OTHER"] = 'Ошибка параметров';
	echo Bitrix\Main\Web\Json::encode($arResult);
	return;
}

$arResult["ERRORS"] = array();	

$_REQUEST['sessid'] = $_POST['sessid'];
if(!check_bitrix_sessid()) {
	$arResult["ERRORS"]["OTHER"] = $mf_sess_exp;
	echo Bitrix\Main\Web\Json::encode($arResult);	
	return;
}
	
$arParams = $_SESSION['wjQWF'][$_POST["params_hash"]]; 

$arParams["EVENT_NAME"] = "FEEDBACK_FORM";
$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);

if($arParams["EMAIL_TO"] == '')
	$arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
    
//Форма запросила инициализацию
if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["request_type"] == 'init') {
	$answer = array();
	foreach ($arParams['FIELDS'] as $name => $field) {
		$answer[] = array (
			'NAME' => $name,
			'REQUIRED' => $field['REQUIRED'],
			'INPUT_TYPE' => $field['INPUT_TYPE'],
			'CAPTION' => $field['CAPTION'],
			'VALUE' => $field['DEFAULT_VALUE'],
            'VALUES' => $field["VALUES"],
			'ERROR_MESSAGE' => $field['ERROR_MESSAGE']
		);
	}
	echo Bitrix\Main\Web\Json::encode(array('FIELDS' => $answer));
}

//Валидация
if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["confirm_submit"] == 'confirmed')
{
		function check_length($value) {
			if (strlen($value) > 0)
				return true;
			else
				return false;
		}

		foreach ($arParams["FIELDS"] as $fieldname => $field) {				
			if ($field['REQUIRED'] == 'Y') {// по возможности свернуть условия
				if ($field['INPUT_TYPE'] == 'file') {
					if (!isset($arParams['FILES'][$fieldname])) {
						$arResult['ERRORS']['FIELDS'][$fieldname] = $arParams['FIELDS'][$fieldname]['ERROR_MESSAGE_NOTFILLED'];	
					}	
					continue;
				}			
				if (strlen(trim($_POST[$fieldname])) > 0) {
					if (function_exists($field['CHECK_FUNCTION']))
						if (!call_user_func($field['CHECK_FUNCTION'], $_POST[$fieldname]))
							$arResult['ERRORS']['FIELDS'][$fieldname] = $arParams['FIELDS'][$fieldname]['ERROR_MESSAGE_INCORRECT'];
				}
				else	
					$arResult['ERRORS']['FIELDS'][$fieldname] = $arParams['FIELDS'][$fieldname]['ERROR_MESSAGE_NOTFILLED'];
			}
			else
				if (strlen(trim($_POST[$fieldname])) > 0)
					if (function_exists($field['CHECK_FUNCTION']))
						if (!call_user_func($field['CHECK_FUNCTION'], $_POST[$fieldname]))
							$arResult['ERRORS']['FIELDS'][$fieldname] = $arParams['FIELDS'][$fieldname]['ERROR_MESSAGE_INCORRECT'];
		}

		if(empty($arResult["ERRORS"]))
		{
			foreach ($arParams['FIELDS'] as $fieldname => $field) {
				if (!defined('BX_UTF'))
					$arFields[$fieldname] = iconv('utf-8', 'cp1251', $_POST[$fieldname]);
				else 
					$arFields[$fieldname] = $_POST[$fieldname];

			}
			$arFields["EMAIL_TO"] = $arParams["EMAIL_TO"];
			if(!empty($arParams["EVENT_MESSAGE_ID"]))
			{
				foreach($arParams["EVENT_MESSAGE_ID"] as $v)
					if(IntVal($v) > 0){
						CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
						//CEvent::SendImmediate($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));

                        /*swift mail*/
                        // подключаем swift_required.php который вы скачала и п.1
/*                             require_once $_SERVER['DOCUMENT_ROOT'].'bitrix/php_interface/include/swiftmailer/lib/swift_required.php';

                            // создаем письмо
                            $message = Swift_Message::newInstance()
                              ->setSubject($arParams["FORM_THEME"])
                              ->setFrom(array('gworlds@gmail.com'))
                              ->setTo(array($to))
                              ->setBody($message);

                            // настраиваем подключение к gmail
                            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                              ->setUsername('gworlds@gmail.com')
                              ->setPassword('crusader');

                            // отправляем
                            $mailer = Swift_Mailer::newInstance($transport);
                            $mailer->send($message); */
                        /**/
					}
			}
				
			if (is_numeric($arParams['IBLOCK_ID'])) {
			
			
			

				CModule::IncludeModule('iblock');
				
				$IBlockFields['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
				$IBlockFields['ACTIVE'] = ($arParams['PUT_ACTIVE'] == 'Y') ? 'Y' : 'N';
/* 				if (strlen($arParams['SECTION_ID']) > 0) {
					$IBlockFields['IBLOCK_SECTION_ID'] = $arParams['SECTION_ID'];
				} */
				$IBlockFields["DATE_ACTIVE_FROM"] = ConvertTimeStamp(false, "FULL");
				$properties = array ();
				foreach ($arParams['FIELDS'] as $fieldname => $field) {
					if ($field['INPUT_TYPE'] == 'file') {
						$file_id = CFile::MakeFileArray($arParams['FILES'][$fieldname]);
						$arFields[$fieldname] = $file_id;
					}
					if (strpos($field['IBLOCK_FIELD'], 'PROPERTY_') !== false) {
						$prop_name = substr($field['IBLOCK_FIELD'], 9, strlen($field['IBLOCK_FIELD'])-1);
						$properties[$prop_name] = $arFields[$fieldname];
					}
					else
						$IBlockFields[$field['IBLOCK_FIELD']] = $arFields[$fieldname];
				}
				$IBlockFields['PROPERTY_VALUES'] = $properties;
				$el = new CIBlockElement;
				$el->Add($IBlockFields);
			}
			$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
			$arResult["STATUS"] = "OK";
		} else {
			$arResult["STATUS"] = "ERR";
		}
	echo Bitrix\Main\Web\Json::encode($arResult);
}
?>