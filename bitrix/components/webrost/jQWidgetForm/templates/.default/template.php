<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

?>

<?
	$form_id = 'wjQWF-'.$arResult['PARAMS_HASH']; //Можно поменять на свой, но только при условии, что таких же не используется на странице
?>

<div class="form" id="<?=$form_id?>">
</div>

<script>
    <?// Это объект нужно использовать в script.js шаблона для инициализации виджета. Там же настраивается внешний вид формы.
      // Если форм на странице несколько, то для каждой из них нужен свой объект
      // Эти меры нужны для того, чтобы формы инициализировались в конце страницы
    ?>
    JQForm = {
        id: "<?=$form_id?>",
        bx_data: {
			sessid: '<?=bitrix_sessid()?>', /*<?=bitrix_sessid_post()?>*/
			params_hash: '<?=$arResult['PARAMS_HASH']?>',
			eval_url: '<?=$arResult['EVAL_URL']?>', /*<?=$arResult['EVAL_URL']?>*/
		},
    }
</script>