(function( $ ) { 
	$('#'+JQForm.id).wjQWF({
		content_window_css : 'wjQWF-content', //классы окна виджета через ' '
		header : '<div class="wjQWF-title">Написать письмо</div>', //любой блоковый элемент
		footer: '',
		okbox : {
			html: '<div class="wjQWF-ok-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщений
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide',
		},
		errbox : {
			html: '<div class="wjQWF-err-box"></div>', //любой блоковый элемент
			message: '<p></p>', //обертка сообщений
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		form: {
			html: '<div class="wjQWF-form"></div>', //любой блоковый элемен
			on_class: 'wjQWF-show',
			off_class: 'wjQWF-hide'
		},
		send_btn : {
			html: '<button>Отправить<span></span></button>',
			on_class: 'wjQWF-show wjQWF-btn',
			off_class: 'wjQWF-hide wjQWF-btn'
		},
		bx_data : JQForm.bx_data,
	});
}( jQuery ) );

