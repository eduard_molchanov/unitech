<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$this->setFrameMode(true);

if (is_array($arResult["SOCSERV"]) && !empty($arResult["SOCSERV"]))
{
?>
<ul class="list-inline list-unstyled">
	<?foreach($arResult["SOCSERV"] as $socserv):?>
	<li class="list-inline-item">
		<a class="text-secondary" target="_blank" href="<?=htmlspecialcharsbx($socserv["LINK"])?>" rel="nofollow">
			<svg class="icon-svg icon-color h5"><use xlink:href="#svg-<?=htmlspecialcharsbx($socserv["CLASS"])?>"></use></svg>
		</a>
	</li>
	<?endforeach?>
</ul>
<?
}
?>