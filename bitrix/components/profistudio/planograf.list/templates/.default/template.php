<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
global $DB;

foreach ($arResult["ITEMS"] as $arItem)
{
	foreach ($arItem['DISPLAY_PROPERTIES'] as $property)
	{
		if ($property['USER_TYPE'] == 'Planograf' and $property['VALUE'] != '')
		{
			$val = explode("|", $property["~VALUE"]);

			if ($val[1] == 'undefined')
			{
				$mapType = 'yandex';
			}
			else
			{
				$mapType = $val[1];
			}

			if (!empty($arParams["MAP_CHECK"]) and $arParams["MAP_CHECK"] == "Y" and !empty($arParams["MAP_DATA_COORDS"]))
			{
				$map[$mapType]["center"] = "[" . $arParams["MAP_DATA_COORDS"] . "]";
				$map[$mapType]["zoom"] = $arParams["MAP_DATA_ZOOM"];
			}
			else
			{
				$Objects["coords-" . $mapType] = !empty($val[2]) ? $val[2] : "null";
				$Objects["coords-array-" . $mapType] = json_decode($Objects["coords-" . $mapType], true);
				$map[$mapType]["center"] = "[" . implode(",", $Objects["coords-array-" . $mapType]["center"]) . "]";
				$map[$mapType]["zoom"] = $Objects["coords-array-" . $mapType]["zoom"];
			}

			$type = !empty($val[3]) ? $val[3] : "yandex#map";

			$val[4] = substr($val[4], 1, strlen($val[4]) - 2);
			if ($val[4] !== '')
			{
				$Objects["placemarks-" . $mapType][] = $val[4];
			}

			$val[5] = substr($val[5], 1, strlen($val[5]) - 2);
			if ($val[5] !== '')
			{
				$Objects["lines-" . $mapType][] = $val[5];
			}

			$val[6] = substr($val[6], 1, strlen($val[6]) - 2);
			if ($val[6] !== '')
			{
				$Objects["polygons-" . $mapType][] = $val[6];
			}
		}
	}
}

$placemarks[$arParams["MAP"]] = !empty($Objects["placemarks-" . $arParams["MAP"]]) ? implode(',', array_filter($Objects["placemarks-" . $arParams["MAP"]])) : false;
$placemarks[$arParams["MAP"]] = $placemarks[$arParams["MAP"]] ? '['.$placemarks[$arParams["MAP"]].']' : 'null';

$lines[$arParams["MAP"]] = !empty($Objects["lines-" . $arParams["MAP"]]) ? implode(',', array_filter($Objects["lines-" . $arParams["MAP"]])) : false;
$lines[$arParams["MAP"]] = $lines[$arParams["MAP"]] ? '['.$lines[$arParams["MAP"]].']' : 'null';

$polygons[$arParams["MAP"]] = !empty($Objects["polygons-" . $arParams["MAP"]]) ? implode(',', array_filter($Objects["polygons-" . $arParams["MAP"]])) : false;
$polygons[$arParams["MAP"]] = $polygons[$arParams["MAP"]] ? '['.$polygons[$arParams["MAP"]].']' : 'null';

$routes[$arParams["MAP"]] = 'null';
?>

<script type="text/javascript">
	if (myMap == undefined)
	{
		var myMap = {};
	}
</script>

<? if ($arParams['MAP'] == 'yandex') { ?>

	<?
	if ($arParams["ITEMS_ZOOMMAX"])
	{
		$maxZoom = $arParams["ITEMS_ZOOMMAX"];
	}
	else
	{
		$maxZoom = 17;
	}
	$minZoom = $arParams["ITEMS_ZOOMMIN"];
	?>
	<script type="text/javascript">
		myMap["<?=$arParams["MAP_ID"]?>"]={"init":{
              "center": <?= $map[$arParams["MAP"]]["center"] ? $map[$arParams["MAP"]]["center"] : "[]" ?>,
              "zoom": <?= $map[$arParams["MAP"]]["zoom"] ? $map[$arParams["MAP"]]["zoom"] : "0" ?>,
              "type": "<?=$type?>",
              "behaviors":["<?=$arParams['OPTIONS'][0];?>","<?=$arParams['OPTIONS'][1];?>","<?=$arParams['OPTIONS'][2];?>","<?=$arParams['OPTIONS'][3];?>","<?=$arParams['OPTIONS'][4];?>"],
              "options":{
				<?=(isset($maxZoom))?'"maxZoom":'.$maxZoom.',':''?><?=(isset($minZoom))?'"minZoom":'.$minZoom.',':''?>
			  }
            },
              "controls":<?=($arParams["MAP_TOOLS"]=="Y")?1:0?>,
              "traffic":0,
              "placemarks":<?=$placemarks[$arParams["MAP"]]?>,
              "lines":<?=$lines[$arParams["MAP"]]?>,
              "polygons":<?=$polygons[$arParams["MAP"]]?>,
              "routes":<?=$routes[$arParams["MAP"]]?>,
              "edit":false,
			  <?=((!empty($arParams['RESTRICT_MAP_AREA']) and $arParams['RESTRICT_MAP_AREA'] === 'Y') ? '"restrictMapArea": true,' : '')?> 
              "lang": <?=GetMessage("IBLOCK_PROP_planograf_js")?>
            };
	</script>

<? } else { ?>

	<?
	$strSql = "SELECT * FROM b_profi_planograf WHERE FOLDERS_NAME=\"" . $arParams["MAP"] . "\"";
	$res = $DB->Query($strSql, false, $err_mess . __LINE__);
	$rowz = $res->Fetch();
	if ($arParams["ITEMS_ZOOMMAX"])
	{
		$maxZoom = $arParams["ITEMS_ZOOMMAX"];
	}
	else
	{
		$maxZoom = $rowz["ZOOM"] - 1;
	}
	$minZoom = $arParams["ITEMS_ZOOMMIN"];
	?>
	<script type="text/javascript">
		myMap["<?=$arParams["MAP_ID"]?>"]={"init":{
            "center": <?= $map[$arParams["MAP"]]["center"] ? $map[$arParams["MAP"]]["center"] : "[]" ?>,
            "zoom": "<?= $map[$arParams["MAP"]]["zoom"] ? $map[$arParams["MAP"]]["zoom"] : 0 ?>",
            "type": "my#map",
            "behaviors":["<?=$arParams['OPTIONS'][0];?>","<?=$arParams['OPTIONS'][1];?>","<?=$arParams['OPTIONS'][2];?>","<?=$arParams['OPTIONS'][3];?>","<?=$arParams['OPTIONS'][4];?>"],
            "options":{
				<?=(isset($maxZoom))?'"maxZoom":'.$maxZoom.',':''?><?=(isset($minZoom))?'"minZoom":'.$minZoom.',':''?>
			}
          },
            "controls":<?=($arParams["MAP_TOOLS"]=="Y")?1:0?>,
            "traffic":0,
            "placemarks":<?=$placemarks[$arParams["MAP"]]?>,
            "lines":<?=$lines[$arParams["MAP"]]?>,
            "polygons":<?=$polygons[$arParams["MAP"]]?>,
            "routes":<?=$routes[$arParams["MAP"]]?>,
            "edit":false,
            "folder":"<?=$arParams["MAP"]?>",
			<?=((!empty($arParams['RESTRICT_MAP_AREA']) and $arParams['RESTRICT_MAP_AREA'] === 'Y') ? '"restrictMapArea": true,' : '')?>	
            "logo":"<?=$rowz["COVER"]?>",
            "lang": <?=GetMessage("IBLOCK_PROP_planograf_js")?>
          };
	</script>

<? } ?>

<div id="<?=$arParams["MAP_ID"]?>" style="width:<?=$arParams['ITEMS_WIDTH']?>; height:<?=$arParams['ITEMS_HEIGHT']?>; margin-bottom:20px;"></div>
