<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arCurrentValues["MAP_ID"]))
{
	$map_id = $arCurrentValues["MAP_ID"];
}
else
{
	$map_id = randString(10);
}

$arTemplateParameters["MAP_DATA"] = Array(
	'NAME' => GetMessage("PROFI_PLANOGRAF_COORDS"),
	'TYPE' => 'CUSTOM',
	'JS_FILE' => '/bitrix/js/profistudio.planograf/settings.js?v2.2.3.1',
	'JS_EVENT' => 'getCenter',
	'JS_DATA' => $map_id,
	'PARENT' => 'BASE',	
);
?>