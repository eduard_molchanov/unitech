<?
if ( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true )die();

if ( !CModule::IncludeModule( "iblock" ) )
	return;

$arTypes = CIBlockParameters::GetIBlockTypes();

$arIBlocks = Array();
$db_iblock = CIBlock::GetList( Array( "SORT" => "ASC" ), Array( "SITE_ID" => $_REQUEST[ "site" ], "TYPE" => ( $arCurrentValues[ "IBLOCK_TYPE" ] != "-" ? $arCurrentValues[ "IBLOCK_TYPE" ] : "" ) ) );
while ( $arRes = $db_iblock->Fetch() )
	$arIBlocks[ $arRes[ "ID" ] ] = $arRes[ "NAME" ];

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList( Array( "sort" => "asc", "name" => "asc" ), Array( "ACTIVE" => "Y", "IBLOCK_ID" => ( !empty( $arCurrentValues[ "IBLOCK_ID" ] ) ? $arCurrentValues[ "IBLOCK_ID" ] : $arCurrentValues[ "ID" ] ) ) );
while ( $arr = $rsProp->Fetch() ) {
	$arProperty[ $arr[ "CODE" ] ] = "[" . $arr[ "CODE" ] . "] " . $arr[ "NAME" ];
	if ( in_array( $arr[ "PROPERTY_TYPE" ], array( "L", "N", "S" ) ) ) {
		$arProperty_LNS[ $arr[ "CODE" ] ] = "[" . $arr[ "CODE" ] . "] " . $arr[ "NAME" ];
	}
}

$arUGroupsEx = Array();
$dbUGroups = CGroup::GetList( $by = "c_sort", $order = "asc" );
while ( $arUGroups = $dbUGroups->Fetch() ) {
	$arUGroupsEx[ $arUGroups[ "ID" ] ] = $arUGroups[ "NAME" ];
}
if ( !empty( $arCurrentValues[ "ELEMENT_CODE" ] ) ) {
	$rsEl = CIBlockElement::GetList( Array(), Array( "IBLOCK_ID" => ( !empty( $arCurrentValues[ "IBLOCK_ID" ] ) ? $arCurrentValues[ "IBLOCK_ID" ] : $arCurrentValues[ "ID" ] ), "CODE" => $arCurrentValues[ "ELEMENT_CODE" ] ) );
	$arEl = $rsEl->GetNext();
	$id = $arEl[ "ID" ];
}
if ( !empty( $arCurrentValues[ "ELEMENT_ID" ] ) ) {
	$id = $arCurrentValues[ "ELEMENT_ID" ];
}

if ( !empty( $arCurrentValues[ "MAP_ID" ] ) ) {
	$map_id = $arCurrentValues[ "MAP_ID" ];
} else {
	$map_id = randString( 10 );
}

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		/*"AJAX_MODE" => array(),*/
		"IBLOCK_TYPE" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "T_IBLOCK_DESC_LIST_TYPE" ),
			"TYPE" => "LIST",
			"VALUES" => $arTypes,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "T_IBLOCK_DESC_LIST_ID" ),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"ELEMENT_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "CP_BND_ELEMENT_ID" ),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"ELEMENT_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "CP_BND_ELEMENT_CODE" ),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),

		"PROPERTY_CODE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "PROFI_PLANOGRAF_IBLOCK_PROPERTY" ),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $arProperty_LNS,
			"DEFAULT" => "map",
			"ADDITIONAL_VALUES" => "Y",
		),
		'MAP_DATA' => array(
			'NAME' => GetMessage( "PROFI_PLANOGRAF_COORDS" ),
			'TYPE' => 'CUSTOM',
			'JS_FILE' => '/bitrix/js/profistudio.planograf/settings.js?v2.2.3.1',
			'JS_EVENT' => 'getCenter',
			'JS_DATA' => $id,
			'PARENT' => 'BASE',
		),
		'MAP_CHECK' => array(
			'NAME' => GetMessage( "PROFI_PLANOGRAF_CHECK" ),
			'TYPE' => 'CHECKBOX',
			'PARENT' => 'BASE',
			'DEFAULT' => 'N'
		),
		'RESTRICT_MAP_AREA' => array(
			'NAME' => GetMessage( "PROFI_PLANOGRAF_RESTRICT_MAP_AREA" ),
			'TYPE' => 'CHECKBOX',
			'PARENT' => 'BASE',
			'DEFAULT' => 'N'
		),
		"MAP_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "PROFI_PLANOGRAF_MAP_ID" ),
			"TYPE" => "STRING",
			"DEFAULT" => $map_id,
		),
		"ITEMS_WIDTH" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "PROFI_PLANOGRAF_SIRINA_KARTY" ),
			"TYPE" => "STRING",
			"DEFAULT" => "500px",
		),
		"ITEMS_HEIGHT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "PROFI_PLANOGRAF_VYSOTA_KARTY" ),
			"TYPE" => "STRING",
			"DEFAULT" => "300px",
		),
		"ITEMS_ZOOMMAX" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "PROFI_PLANOGRAF_ZUM_NA_KARTE_PO_UMOL" ),
			"TYPE" => "STRING",
			"DEFAULT" => "3",
		),
		"ITEMS_ZOOMMIN" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage( "PROFI_PLANOGRAF_MINIMALQNYY_ZUM" ),
			"TYPE" => "STRING",
			"DEFAULT" => "0",
		),

		'OPTIONS' => array(
			'NAME' => GetMessage( "PROFI_PLANOGRAF_NASTROYKI" ),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'VALUES' => array(
				'scrollZoom' => GetMessage( "PROFI_PLANOGRAF_IZMENENIE_MASSTABA_K" ),
				'dblClickZoom' => GetMessage( "PROFI_PLANOGRAF_IZMENENIE_MASSTABA_D" ),
				'rightMouseButtonMagnifier' => GetMessage( "PROFI_PLANOGRAF_IZMENENIE_MASSTABA_P" ),
				'leftMouseButtonMagnifier' => GetMessage( "PROFI_PLANOGRAF_IZMENENIE_MASSTABA_L" ),
				'drag' => GetMessage( "PROFI_PLANOGRAF_PERETASKIVANIE" ),
				/*'ENABLE_HOTKEYS' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_HOTKEYS'),*/
				/*'ENABLE_RULER' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_RULER'),*/
			),


			'DEFAULT' => array( 'scrollZoom', 'dblClickZoom', 'drag' ),
			'PARENT' => 'ADDITIONAL_SETTINGS',
		),
		'MAP_TOOLS' => array(
			'NAME' => GetMessage( "PROFI_PLANOGRAF_TOOLS" ),
			'TYPE' => 'CHECKBOX',
			'PARENT' => 'ADDITIONAL_SETTINGS',
			'DEFAULT' => 'Y'
		),

	),
);
?>