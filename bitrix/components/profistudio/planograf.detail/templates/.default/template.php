<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	global $DB;
  $_REQUEST["id"] = $arResult["ID"];
?>

            <script type="text/javascript">
            if(myMap==undefined){
              var myMap={};
            }
            </script>

<?if($arParams["NAME"]=="Y"):?>
  <h3><?=$arResult["NAME"]?></h3>
<?endif;?>
  <? foreach ( $arResult['DISPLAY_PROPERTIES'] as $property ) { ?>
		<? if (  $property ['USER_TYPE'] == 'Planograf' and  $property['VALUE'] != '' ){
			$val = explode("|", $property["~VALUE"]);

			if (!empty($arParams["MAP_CHECK"]) and $arParams["MAP_CHECK"] == "Y" and !empty($arParams["MAP_DATA_COORDS"]))
			{
				$coords_array["center"] = "[".$arParams["MAP_DATA_COORDS"]."]";
				$coords_array["zoom"] = $arParams["MAP_DATA_ZOOM"];
			}
			else
			{
				$coords = !empty($val[2]) ? $val[2] : "null";
				$coords_array = json_decode($coords,true);
				$coords_array["center"] = "[".implode(",",$coords_array["center"])."]";
			}

			$type = !empty($val[3]) ? $val[3] : "yandex#map";
			$placemarks = !empty($val[4]) ? $val[4] : "null";
			$lines = !empty($val[5]) ? $val[5] : "null";
			$polygons = !empty($val[6]) ? $val[6] : "null";
			$routes = !empty($val[7]) ? $val[7] : "null";
		?>
<?
  if($val[0]==2){
      if($arParams["ITEMS_ZOOMMAX"]){
        $maxZoom=$arParams["ITEMS_ZOOMMAX"];
      } else {
        $maxZoom=17;
      }
      $minZoom = $arParams["ITEMS_ZOOMMIN"];
?>
			<script type="text/javascript">
			myMap["<?=$arParams["MAP_ID"]?>"]={"init":{
              "center": <?= $coords_array["center"] ? $coords_array["center"] : "[]" ?>,
              "zoom": <?= $coords_array["zoom"] ? $coords_array["zoom"] : "0" ?>,
  						"type": "<?=$type?>",
  						"behaviors":["<?=$arParams['OPTIONS'][0];?>","<?=$arParams['OPTIONS'][1];?>","<?=$arParams['OPTIONS'][2];?>","<?=$arParams['OPTIONS'][3];?>","<?=$arParams['OPTIONS'][4];?>"],
  						"options":{
							<?=(isset($maxZoom))?'"maxZoom":'.$maxZoom.',':''?><?=(isset($minZoom))?'"minZoom":'.$minZoom.',':''?>
						}
  					},
  						"controls":<?=($arParams["MAP_TOOLS"]=="Y")?1:0?>,
  						"traffic":0,
  						"placemarks":<?=$placemarks?>,
  						"lines":<?=$lines?>,
  						"polygons":<?=$polygons?>,
  						"routes":<?=$routes?>,
  						"edit":false,
						<?=((!empty($arParams['RESTRICT_MAP_AREA']) and $arParams['RESTRICT_MAP_AREA'] === 'Y') ? '"restrictMapArea": true,' : '')?>
              			"lang": <?=GetMessage("IBLOCK_PROP_planograf_js")?>
  					};

            </script>

<?} else {?>

		<?
			$strSql = "SELECT *	FROM b_profi_planograf WHERE FOLDERS_NAME=\"".$val[1]."\"";
			$res = $DB->Query($strSql, false, $err_mess.__LINE__);
			$rowz = $res->Fetch();
      if($arParams["ITEMS_ZOOMMAX"]){
        $maxZoom=$arParams["ITEMS_ZOOMMAX"];
      } else {
        $maxZoom=$rowz["ZOOM"]-1;
      }
      $minZoom = $arParams["ITEMS_ZOOMMIN"];
		?>
		<script type="text/javascript">
		myMap["<?=$arParams["MAP_ID"]?>"]={"init":{
            "center": <?= $coords_array["center"] ? $coords_array["center"] : "[]" ?>,
            "zoom": <?= $coords_array["zoom"] ? $coords_array["zoom"] : "0" ?>,
            "type": "my#map",
            "behaviors":["<?=$arParams['OPTIONS'][0];?>","<?=$arParams['OPTIONS'][1];?>","<?=$arParams['OPTIONS'][2];?>","<?=$arParams['OPTIONS'][3];?>","<?=$arParams['OPTIONS'][4];?>"],
            "options":{
				<?=(isset($maxZoom))?'"maxZoom":'.$maxZoom.',':''?><?=(isset($minZoom))?'"minZoom":'.$minZoom.',':''?>
			}
          },
            "controls":<?=($arParams["MAP_TOOLS"]=="Y")?1:0?>,
            "traffic":0,
            "placemarks":<?=$placemarks?>,
            "lines":<?=$lines?>,
            "polygons":<?=$polygons?>,
            "routes":<?=$routes?>,
            "edit":false,
            "folder":"<?=$val[1]?>",
			<?=((!empty($arParams['RESTRICT_MAP_AREA']) and $arParams['RESTRICT_MAP_AREA'] === 'Y') ? '"restrictMapArea": true,' : '')?>
            "logo":"<?=$rowz["COVER"]?>",
            "lang": <?=GetMessage("IBLOCK_PROP_planograf_js")?>
          };
          </script>
<?}?>
    <? } ?>
  <? } ?>
      <div id="<?=$arParams["MAP_ID"]?>" style="width:<?=$arParams['ITEMS_WIDTH']?>; height:<?=$arParams['ITEMS_HEIGHT']?>; margin-bottom:20px;"></div>
