<?
$MESS["PROFI_PLANOGRAF_NAME"] = "Выводить заголовок";
$MESS["PROFI_PLANOGRAF_TIP_INFOBLOKA"] = "Тип инфоблока";
$MESS["PROFI_PLANOGRAF_INFOBLOKA"] = "инфоблока";
$MESS["PROFI_PLANOGRAF_KOLICESTVO_ELEMENTOV"] = "Количество элементов";
$MESS["PROFI_PLANOGRAF_SIRINA_KARTY"] = "Ширина карты";
$MESS["PROFI_PLANOGRAF_VYSOTA_KARTY"] = "Высота карты";
$MESS["PROFI_PLANOGRAF_ZUM_NA_KARTE_PO_UMOL"] = "Максимальный зум";
$MESS["PROFI_PLANOGRAF_MINIMALQNYY_ZUM"] = "Минимальный зум";
$MESS["PROFI_PLANOGRAF_MINI_KARTA"] = "Мини карта";
$MESS["PROFI_PLANOGRAF_IZMERITQ_RASTOANIE"] = "Измерить растояние";
$MESS["PROFI_PLANOGRAF_NASTROYKI"] = "Настройки";
$MESS["PROFI_PLANOGRAF_IZMENENIE_MASSTABA_K"] = "Изменение масштаба колесом мыши";
$MESS["PROFI_PLANOGRAF_IZMENENIE_MASSTABA_D"] = "Изменение масштаба двойным щелчком мыши";
$MESS["PROFI_PLANOGRAF_IZMENENIE_MASSTABA_P"] = "Изменение масштаба правой кнопкой мыши";
$MESS["PROFI_PLANOGRAF_IZMENENIE_MASSTABA_L"] = "Изменение масштаба левой кнопкой мыши";
$MESS["PROFI_PLANOGRAF_PERETASKIVANIE"] = "Перетаскивание";
$MESS["PROFI_PLANOGRAF_TIP_KARTY"] = "Тип карты";
$MESS["PROFI_PLANOGRAF_POISK"] = "Поиск";
$MESS["PROFI_PLANOGRAF_CHECK"] = "Зафиксировать карту";
$MESS["PROFI_PLANOGRAF_COORDS"] = "Координаты центра и зум";
$MESS["PROFI_PLANOGRAF_ELEMENT_ID"] = "ID элемента";
$MESS["PROFI_PLANOGRAF_ELEMENT_CODE"] = "Символьный код элемента";
$MESS["PROFI_PLANOGRAF_IBLOCK_PROPERTY"] = "Свойства инфоблока";
$MESS["PROFI_PLANOGRAF_TOOLS"] = "Выводить инструменты";


$MESS["T_IBLOCK_DESC_LIST_ID"] = "Код информационного блока";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Тип информационного блока (используется только для проверки)";
$MESS["CP_BND_ELEMENT_CODE"] = "Код элемента";
$MESS["CP_BND_ELEMENT_ID"] = "ID элемента";
$MESS["PROFI_PLANOGRAF_MAP_ID"] = "Идентификатор блока с картой";

$MESS["PROFI_PLANOGRAF_RESTRICT_MAP_AREA"] = "Ограничить область смещения карты";
?>