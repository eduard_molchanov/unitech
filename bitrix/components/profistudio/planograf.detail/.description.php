<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("PROFI_PLANOGRAF_PLANOGRAF"),
	"DESCRIPTION" => GetMessage("PROFI_PLANOGRAF_PLANOGRAF"),
	"ICON" => "/images/icon.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "Profi studio", // for example "my_project"
	),
	"COMPLEX" => "N",
);

?>