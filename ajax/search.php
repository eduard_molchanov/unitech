<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по сайту");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"popup",
	array(
		"CHECK_DATES" => "N",
		"CONTAINER_ID" => "popup-title-search",
		"CONVERT_CURRENCY" => "N",
		"INPUT_ID" => "popup-title-search-input",
		"NUM_CATEGORIES" => "2",
		"ORDER" => "date",
		"PAGE" => "/search/",
		"PREVIEW_HEIGHT" => "75",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PREVIEW_WIDTH" => "75",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"SHOW_PREVIEW" => "Y",
		"TOP_COUNT" => "5",
		"USE_LANGUAGE_GUESS" => "Y",
		"COMPONENT_TEMPLATE" => "popup",
		"CATEGORY_0_TITLE" => "Товары",
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_iblock_catalog" => array(
			0 => "44",
		),
		"CATEGORY_0_main" => "",
		"CATEGORY_1_TITLE" => "Статьи",
		"CATEGORY_1" => array(
			0 => "main",
			1 => "iblock_articles",
			2 => "iblock_content",
		),
		"CATEGORY_1_main" => array(
		),
		"CATEGORY_1_iblock_articles" => array(
			0 => "42",
		),
		"CATEGORY_1_iblock_content" => array(
			0 => "45",
			1 => "47",
		),
		"SHOW_INPUT_LABEL" => "Y",
		"INPUT_LABEL_TEXT" => ""
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>