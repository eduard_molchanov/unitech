<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<div class="site-menu d-flex flex-column">
	<div class="site-menu__head">
		<div class="d-none d-lg-block">
			<a class="h1" href="<?=SITE_DIR?>"><?php
				?><?$APPLICATION->IncludeFile(
					SITE_DIR."include/company_logo.php",
					Array(),
					Array("MODE"=>"html")
				);?><?php
			?></a>
		</div>
		<div class="d-block d-lg-none text-right container">
			<div class="d-inline-block ml-5">
				<?php
				$APPLICATION->IncludeFile(
					'include/globals/recall-icon.php',
					array()
				);
				?>
			</div>
			<div class="d-inline-block ml-5">
				<?php
				$APPLICATION->IncludeFile(
					'include/globals/cart-icon.php',
					array(
						'AJAX_LOAD' => 'Y',
					),
					array(
						'SHOW_BORDER' => false
					)
				);
				?>
			</div>
		</div>
	</div>
	<div class="site-menu__search d-block d-lg-none">
		<div class="container">
			<?php
			$APPLICATION->IncludeFile(
				SITE_DIR.'include/templates/search.php',
				array(),
				array(
					'SHOW_BORDER' => false
				)
			);
			?>
		</div>
	</div>
	<div class="flex-grow-1 flex-shrink-0 d-flex align-items-center mb-4">
		<div class="container">

			<?php

			$APPLICATION->IncludeFile(
				SITE_DIR.'include/templates/main-menu.php',
				array(),
				array(
					'SHOW_BORDER' => false
				)
			);
			?>
		</div>
	</div>

	<div class="container">

		<div class="d-flex flex justify-content-between text-secondary flex-column flex-md-row">
			<div>
				<div class="d-block my-5 my-md-4">
					<?php
					$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footer/address.php',
						array(),
						array(
							'SHOW_BORDER' => false
						)
					);
					?>
				</div>
				<div class="d-block my-5 my-md-4">
					<?php
					$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footer/phones.php',
						array(),
						array(
							'SHOW_BORDER' => false
						)
					);
					?>
				</div>
				<div class="d-block my-5 my-md-4">
					<?php
					$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footer/emails.php',
						array(),
						array(
							'SHOW_BORDER' => false
						)
					);
					?>
				</div>
			</div>
			<div class="my-5 my-md-4">
				<?php
				$APPLICATION->IncludeFile(
					SITE_DIR.'/include/footer/socnet_links.php',
					array(),
					array(
						'SHOW_BORDER' => false
					)
				);
				?>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>