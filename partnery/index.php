<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
//$APPLICATION->SetTitle(" Партнеры");
$APPLICATION->SetTitle(" Партнеры");
//?><!----><?// $APPLICATION->AddChainItem("Партнеры");?>
<div class="row">
	<div class="col-lg-12">
		<p class="accent">
			 Команда компании «Универсальные технологии» («Унитех») имеет более чем 15-летний опыт работы на российском рынке ИКТ, в ходе которой успешно реализован ряд сложных проектов в интересах крупных компаний-заказчиков. Специалисты «Унитех», глубоко понимая специфику деятельности и порядок работы своих заказчиков, готовы применять свой опыт в реализации комплексных проектов. Компания «Унитех» обладает значительным опытом и партнерскими статусами ведущих производителей в области инфо-коммуникационных технологий.
		</p>
		 <? /*
            $APPLICATION->IncludeComponent(
                "bitrix:news",
                "flat",
                array(
                    "ADD_ELEMENT_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "N",
                    "CATALOG_VIEW_MODE" => "VIEW_ELEMENTS",
                    "CHECK_DATES" => "Y",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "DETAIL_BACKGROUND_COLOR" => "BANNER_BACKGROUND_COLOR",
                    "DETAIL_BACKGROUND_IMAGE" => "",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_FIELD_CODE" => array("", ""),
                    "DETAIL_PAGER_SHOW_ALL" => "Y",
                    "DETAIL_PAGER_TEMPLATE" => "",
                    "DETAIL_PAGER_TITLE" => "",
                    "DETAIL_PROPERTY_CODE" => array("URL_SITE", "BANNER_BACKGROUND_COLOR", ""),
                    "DETAIL_SET_CANONICAL_URL" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "N",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "IBLOCK_ID" => "53",
                    "IBLOCK_TYPE" => "partnery",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "LIST_BACKGROUND_COLOR" => "-",
                    "LIST_FIELD_CODE" => array("", ""),
                    "LIST_PROPERTY_CODE" => array("URL_SITE", ""),
                    "MEDIA_PROPERTY" => "",
                    "MESSAGE_404" => "",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "bootstrap",
                    "PAGER_TITLE" => "Направления",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "SECTIONS_GRID_RESPONSIVE_SETTINGS" => "{\"xxs\":{\"items\":\"1\"},\"md\":{\"items\":\"2\"}}",
                    "SECTION_BACKGROUND_IMAGE" => "-",
                    "SEF_MODE" => "N",
                    "SET_LAST_MODIFIED" => "Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SLIDER_PROPERTY" => "",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "Y",
                    "TEMPLATE_THEME" => "blue",
                    "USE_CATEGORIES" => "N",
                    "USE_FILTER" => "N",
                    "USE_PERMISSIONS" => "N",
                    "USE_RATING" => "N",
                    "USE_REVIEW" => "N",
                    "USE_RSS" => "N",
                    "USE_SEARCH" => "N",
                    "USE_SHARE" => "N",
                    "VARIABLE_ALIASES" => array("ELEMENT_ID" => "ELEMENT_ID", "SECTION_ID" => "SECTION_ID")
                )
            );
 */
            ?>
	</div>
</div>


<div id="well-partnery-vendors-orders" class="l-main__container well  ">
    <ul class="row list-unstyled mb-0">

        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_58">
            <div class="product product--card">
                <div class="product-image-wrapper mb-3">
                    <a class="product-image-canvas" href="/partnery/vendors/" >
                        <img class="product-image aos-init aos-animate" data-aos="zoom-in" data-aos-duration="500"
                             src="./img/vendor.jpg" alt="Вендоры"
                             title="Вендоры" width="500">
                    </a>
                </div>
                <h4 class="product-title my-0 mb-md-2">
                    <a class="text-body" href="/partnery/vendors/" >
<!--                        Вендоры <span>(12)</span> -->
                        Вендоры
                    </a>
                </h4>


            </div>
        </li>
        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_59">
            <div class="product product--card">
                <div class="product-image-wrapper mb-3">
                    <a class="product-image-canvas" href="/partnery/orders/" >
                        <img class="product-image aos-init aos-animate" data-aos="zoom-in" data-aos-duration="500"
                             src="./img/vendor.jpg" alt="Заказчики"
                             title="Заказчики" width="500">
                    </a>
                </div>
                <h4 class="product-title my-0 mb-md-2">
                    <a class="text-body" href="/partnery/orders/" >
<!--                        Бизнес-приложения <span>(12)</span> -->
                        Заказчики
                    </a>
                </h4>


            </div>
        </li>

    </ul>

</div>


<h2>Вендоры в цифрах (для редактирования заказчиком)</h2>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"achievements",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPANY_ACHIVEMENTS" => "[[\"600000\",\"Посетителей сайта<br>\\nежемесячно\"],[\"12\",\"Регионов<br>\\nприсутствия\"],[\"48000\",\"Покупок каждый<br>\\nмесяц\"],[\"15000\",\"Товаров<br>\\nна складах\"]]",
		"COMPONENT_TEMPLATE" => "achievements",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/empty.php"
	)
);?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>