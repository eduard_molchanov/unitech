<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Проекты");

require($_SERVER["DOCUMENT_ROOT"] . "/local/inc.filter.year.projects.php");

?><?$APPLICATION->IncludeComponent(
    "redsign:news.archive",
    "template42",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "N",
        "FILTER_NAME" => "dateFIlter",
        "IBLOCK_ID" => "21",
        "IBLOCK_TYPE" => "projects",
        "INCLUDE_SUBSECTIONS" => "N",
        "PARENT_SECTION" => "21",
        "PARENT_SECTION_CODE" => "projects",
        "SHOW_MONTHS" => "N",
        "SHOW_TITLE" => "N",
        "SHOW_YEARS" => "Y"
    )
);?><br>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "flat",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "/projects/proekt-detalno.php?ELEMENT_ID=#ELEMENT_ID#",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("",""),
        "FILTER_NAME" => "dateFIlter",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "IBLOCK_ID" => "21",
        "IBLOCK_TYPE" => "presscenter",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MEDIA_PROPERTY" => "",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "20",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "modern",
        "PAGER_TITLE" => "Проекты",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "150",
        "PROPERTY_CODE" => array("",""),
        "SEARCH_PAGE" => "/search/",
        "SET_BROWSER_TITLE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SLIDER_PROPERTY" => "",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_RATING" => "N",
        "USE_SHARE" => "N"
    )
);?>

    <br>
    <br>
    <h2>Проекты в цифрах</h2>
    <br>
    <br>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "achievements",
    Array(
        "AREA_FILE_SHOW" => "file",
        "COMPANY_ACHIVEMENTS" => "[[\"600000\",\"Посетителей сайта<br>\\nежемесячно\"],[\"12\",\"Регионов<br>\\nприсутствия\"],[\"48000\",\"Покупок каждый<br>\\nмесяц\"],[\"15000\",\"Товаров<br>\\nна складах\"]]",
        "COMPONENT_TEMPLATE" => "achievements",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/empty.php"
    )
);
//"DETAIL_URL" => "/projects/proekt-detalno.php?ELEMENT_ID=#ELEMENT_ID#",
?> <br>
    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>