<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Информационная безопасность");
?>
<h1>Тут про безопасность</h1>
<p>
	 В современных условиях ИТ-инфраструктура крупного предприятия представляет собой сложный и разветвленный организм, механизмы работы которого сложно формализовать. Затраты на поддержание работоспособности и развитие ИТ-инфраструктуры составляют существенную часть бюджета компании, при этом далеко не всегда понятно, насколько оправданы эти затраты. Ответом на данный вопрос являются результаты аудита ИТ-инфраструктуры. В рамках аудита наши специалисты проводят независимую оценку существующей ИТ-инфраструктуры и ИТ-процессов компании в соответствии с принятыми международными стандартами (COBIT, COSO\SOX, ISO 20000, eTOM и др.)
</p>
<h3>Аудит может проводиться по нескольким направлениям:</h3>
<ul>
	<li>Обследование оборудования – описание имеющегося серверного и телекоммуникационного оборудования</li>
	<li>Обследование программного обеспечения – изучение состава используемого ПО и целей его использования</li>
	<li>Анализ бизнес-приложений – анализ информационных систем по критериям:
	<ul>
		<li>дизайнер умывает руки</li>
		<li>красный квадратик - божественно, настоящий креатив</li>
		<li>адаптируемость</li>
		<li>масштабируемость</li>
		<li>отказоустойчивость</li>
		<li>стоимость поддержки и развития</li>
	</ul>
 </li>
	<li>Обследование ИТ-процессов и организационной структуры – изучение ИТ-процессов Компании и соответствие их бизнес-требованиям. Для каждого ИТ-процесса определяются:
	<ul>
		<li>роли участников процесса</li>
		<li>метрики процесса
		<ul>
			<li>удобство использования</li>
			<li>производительность</li>
			<li>адаптируемость</li>
			<li>масштабируемость</li>
		</ul>
 </li>
		<li>состав процедур</li>
		<li>ответственные за реализацию процесса</li>
	</ul>
 </li>
</ul>
<div class="service_project">
	<div class="h2">
		 Реализованные проекты
	</div>

 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"projects2", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "21",
		"IBLOCK_TYPE" => "projects",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "projects2"
	),
	false
);?> <a class="back buttom" href="/services/">Вернуться к списку услуг</a>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>