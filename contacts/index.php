<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");

?>

<? $APPLICATION->IncludeComponent(
    "redsign:hcard",
    "contacts",
    array(
        "COMPONENT_TEMPLATE" => "contacts",
        "ORG" => "ООО «Унитех»",
        "INN_KPP" => "7736245838 / 504701001",
        "OGRN" => "1157746455943",
        "ORGANIZATION" => "Общество с ограниченной ответственностью «Универсальные технологии»",
        "ADR_COUNTRY_NAME" => "",
        "ADR_REGION" => "",
        "ADR_LOCALITY" => "",
        "ADR_STREET_ADDRESS" => "Московская область, город Химки, улица Рабочая, дом 2а, корпус 22а, офис 213а",
        "ADR_EXT_ADDRESS" => "",
        "ADR_POSTAL_CODE" => "141401",
        "PHONE" => "+7 (495) 662 9291",
        "EMAIL" => "info@unitechnologies.ru",
        "ADR_COUNTRY_NAME_LEGAL" => "",
        "ADR_REGION_LEGAL" => "",
        "ADR_LOCALITY_LEGAL" => "",
        "ADR_STREET_ADDRESS_LEGAL" => "",
        "ADR_EXT_ADDRESS_LEGAL" => "",
        "ADR_POSTAL_CODE_LEGAL" => ""
    ),
    false
); ?>


    <h2>Схема проезда</h2>

    <script type="text/javascript"
            src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=60445215-6d3a-4f88-87fe-8d52b72e5bc9"></script>
    <script>

        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [55.906148, 37.438754],
                    zoom: 17
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                ),

                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: 'ООО «Универсальные технологии»',
                    balloonContent: 'Россия, Московская область, Химки, Рабочая улица, 2Ак22'
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    // iconImageHref: './myIcon.gif',
                    // iconImageHref: './logo.jpg',
                    iconImageHref: './logo.png',
                    // Размеры метки.
                    // iconImageSize: [30, 30],
                    // iconImageSize: [110, 25],
                    iconImageSize: [111, 43],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-10, -35]
                    // iconImageOffset: [35, 18]
                    // iconImageOffset: [0,0]
                });

            myMap.geoObjects
                .add(myPlacemark);
        });

    </script>


    <div id="map" style="height:400px; width:100%">
    </div>


<h2>Документы для скачивания</h2>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"files", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "46",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "FILE",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "files"
	),
	false
);?>
 
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>