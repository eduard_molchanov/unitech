<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Решения");
?>
    <div id="well" class="l-main__container well container">
        <div id="bx_incl_area_10" title="Двойной щелчок - Редактировать страницу в визуальном редакторе">
            <div id="bx_incl_area_10_1" title="Двойной щелчок - Добавить элемент">

                <div id="bx_incl_area_10_1_1">
                    <ul class="row list-unstyled mb-0">

                        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_8"
                            title="Двойной щелчок - Изменить раздел">
                            <div class="product product--card">
                                <div class="product-image-wrapper mb-3">
                                    <a class="product-image-canvas" href="/catalog/infrastruktura/">
                                        <img class="product-image aos-init aos-animate" data-aos="zoom-in"
                                             data-aos-duration="500"
                                             src="/assets/catalog/infrastruktura.jpeg"
                                             alt="Инфраструктура"
                                             title="Инфраструктура"

                                             height="180"
                                        >
                                    </a>
                                </div>
                                <h4 class="product-title my-0 mb-md-2">
                                    <a class="text-body" href="/catalog/infrastruktura/">
                                        <!--                            Инфраструктура <span>(5)</span> </a>-->
                                        Инфраструктура </a>
                                </h4>
                            </div>
                        </li>
                        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_8"
                            title="Двойной щелчок - Изменить раздел">
                            <div class="product product--card">
                                <div class="product-image-wrapper mb-3">
                                    <a class="product-image-canvas" href="/catalog/biznes-prilozheniya/"
                                       target="_blank">
                                        <img class="product-image aos-init aos-animate" data-aos="zoom-in"
                                             data-aos-duration="500"
                                             src="/assets/catalog/biznes-prilozheniya.jpg"
                                             alt="Бизнес-приложения"
                                             title="Бизнес-приложения"

                                             height="180">
                                    </a>
                                </div>
                                <h4 class="product-title my-0 mb-md-2">
                                    <a class="text-body" href="/catalog/biznes-prilozheniya/">
                                        <!--                            Инфраструктура <span>(5)</span> </a>-->
                                        Бизнес-приложения </a>
                                </h4>
                            </div>
                        </li>
                        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_8"
                            title="Двойной щелчок - Изменить раздел">
                            <div class="product product--card">
                                <div class="product-image-wrapper mb-3">
                                    <a class="product-image-canvas" href="/catalog/mobilnye-resheniya/">
                                        <img class="product-image aos-init aos-animate" data-aos="zoom-in"
                                             data-aos-duration="500"
                                             src="/assets/catalog/mobilnye-resheniya.jpg"
                                             alt="Мобильные решения"
                                             title="Мобильные решения"

                                             height="180">
                                    </a>
                                </div>
                                <h4 class="product-title my-0 mb-md-2">
                                    <a class="text-body" href="/catalog/mobilnye-resheniya/" target="_blank">
                                        <!--                            Инфраструктура <span>(5)</span> </a>-->
                                        Мобильные решения </a>
                                </h4>
                            </div>
                        </li>
                        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_8"
                            title="Двойной щелчок - Изменить раздел">
                            <div class="product product--card">
                                <div class="product-image-wrapper mb-3">
                                    <a class="product-image-canvas" href="/catalog/oblachnye-servisy/">
                                        <img class="product-image aos-init aos-animate" data-aos="zoom-in"
                                             data-aos-duration="500"

                                             src="/assets/catalog/oblachnye-servisy.jpeg"
                                             alt="Облачные сервисы"
                                             title="Облачные сервисы"
                                            
                                             height="180"
                                        >
                                    </a>
                                </div>
                                <h4 class="product-title my-0 mb-md-2">
                                    <a class="text-body" href="/catalog/oblachnye-servisy/">
                                        <!--                            Инфраструктура <span>(5)</span> </a>-->
                                        Облачные сервисы </a>
                                </h4>
                            </div>
                        </li>


                    </ul>
                </div>

            </div>
        </div>
    </div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>