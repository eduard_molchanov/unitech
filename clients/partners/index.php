<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Партнеры");
?>
<?$APPLICATION->AddChainItem("Партнеры");?>
<div class="row">
	<div class="col-lg-10">
		<p class="accent">
			 Компания «Унитех» имеет партнерские отношения с мировыми производителями оборудования и программного обеспечения, что позволяет создавать комплексные решения, сочетающие инфраструктурную часть с автоматизацией бизнес-процессов.
		</p>
	</div>
</div>

<h2>Партнеры в цифрах (для редактирования заказчиком)</h2>
 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"achievements",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPANY_ACHIVEMENTS" => "[[\"600000\",\"Посетителей сайта<br>\\nежемесячно\"],[\"12\",\"Регионов<br>\\nприсутствия\"],[\"48000\",\"Покупок каждый<br>\\nмесяц\"],[\"15000\",\"Товаров<br>\\nна складах\"]]",
		"COMPONENT_TEMPLATE" => "achievements",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/empty.php"
	)
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"catalog",
	array(
		"COMPONENT_TEMPLATE" => "catalog",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "7",
		"NEWS_COUNT" => "200",
		"USE_SEARCH" => "N",
		"USE_RSS" => "N",
		"USE_RATING" => "N",
		"USE_CATEGORIES" => "N",
		"USE_REVIEW" => "N",
		"USE_FILTER" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"CHECK_DATES" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/site_nj/departments/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"USE_PERMISSIONS" => "N",
		"STRICT_SECTION_CHECK" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"CATALOG_VIEW_MODE" => "VIEW_ELEMENTS",
		"PREVIEW_TRUNCATE_LEN" => "",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_NAME" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "BANNER_BACKGROUND_COLOR",
			2 => "",
		),
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_PAGER_TITLE" => "",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"PAGER_TEMPLATE" => "bootstrap",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Направления",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"SECTIONS_GRID_RESPONSIVE_SETTINGS" => "{\"xxs\":{\"items\":\"1\"},\"md\":{\"items\":\"2\"}}",
		"LIST_BACKGROUND_COLOR" => "-",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"DETAIL_BACKGROUND_COLOR" => "BANNER_BACKGROUND_COLOR",
		"DETAIL_BACKGROUND_IMAGE" => "BACKGROUND_IMAGE",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "#ELEMENT_CODE#/",
		)
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>