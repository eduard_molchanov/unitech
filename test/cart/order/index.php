<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
// $APPLICATION->SetPageProperty("wide_page", "Y");
// $APPLICATION->SetPageProperty("hide_page", "Y");
?><?$APPLICATION->IncludeComponent(
	"redsign:lightbasket.order", 
	"winn", 
	array(
		"COMPONENT_TEMPLATE" => "winn",
		"FIELDS_PROPS" => array(
			0 => "NAME",
			1 => "PHONE_NUMBER",
			2 => "EMAIL",
			3 => "COMPANY_NAME",
			4 => "ADRESS",
			5 => "COMMENT",
			6 => "DELIVERY_SERVICE",
			7 => "",
		),
		"IBLOCK_TYPE" => "lightbasket",
		"IBLOCK_ID" => "33",
		"ITEMS_PROP" => "ORDER_LIST",
		"BASKET_IBLOCK_TYPE" => "catalog",
		"BASKET_IBLOCK_ID" => "",
		"BASKET_PROPS" => array(
			0 => "CML2_ARTICLE",
			1 => "",
		),
		"CATALOG_IBLOCK_TYPE" => "catalog",
		"CATALOG_IBLOCK_ID" => "28",
		"CATALOG_PROPS" => array(
			0 => "",
			1 => "CML2_ARTICLE",
			2 => "",
		),
		"PATH_TO_CART" => "/testcart/",
		"SHOW_CONFIRM" => "Y",
		"CONFIRM_TEXT" => "Соглашаюсь на обработку <a href=\"/testabout/licence_work/\">персональных данных</a>",
		"FIELD_PARAMS" => "{\"48\":{\"validate\":\"\",\"validatePattern\":\"\",\"mask\":\"\"},\"49\":{\"validate\":\"\",\"validatePattern\":\"\",\"mask\":\"\"},\"50\":{\"validate\":\"\",\"validatePattern\":\"\",\"mask\":\"\"},\"51\":{\"validate\":\"\",\"validatePattern\":\"\",\"mask\":\"\"},\"52\":{\"validate\":\"\",\"validatePattern\":\"\",\"mask\":\"\"},\"54\":{\"validate\":\"\",\"validatePattern\":\"\",\"mask\":\"\"}}",
		"RS_VK_ID" => "20003922",
		"RS_FB_PAGE" => "https://www.facebook.com/redsignRU/",
		"USER_CONSENT_ID" => "1",
		"USER_CONSENT" => "Y",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N"
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
