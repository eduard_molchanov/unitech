<?php
$APPLICATION->IncludeComponent(
    "rswinn:socnet.links",
    "",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "FACEBOOK" => "https://www.facebook.com/1CBitrix",
        "VKONTAKTE" => "https://vk.com/bitrix_1c",
        "TWITTER" => "https://twitter.com/1c_bitrix",
        "OK" => "https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=41&LESSON_ID=7285",
        "YOUTUBE" => "https://www.youtube.com/user/1CBitrixChannel",
        // "GOOGLE" => "https://plus.google.com/111119180387208976312/",
        // "INSTAGRAM" => "https://instagram.com/1CBitrix/"
    ),
    false,
    array(
        "HIDE_ICONS" => "N"
    )
);
?>