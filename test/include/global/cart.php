<?php
$APPLICATION->IncludeComponent(
    "redsign:lightbasket.basket",
    "global",
    array(
        "COMPONENT_TEMPLATE" => "master",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "28",
        "PROPS" => array(
            0 => "CML2_ARTICLE",
            1 => "",
        ),
        "PATH_TO_ORDER" => "/testcart/order/",
        "AJAX_MODE" => "N",
        "PATH_TO_CART" => "/testcart/",
        "PATH_TO_CATALOG" => "/testcatalog/",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO"
    ),
    false
);