<div class="row">
	<div class="col-xs-10 mx-xs-auto col-xl-10 mx-xl-auto col-xxl-9 ml-xxl-0 mr-xxl-auto">
		<h2 class="mb-5 mt-0" data-aos="fade-up" data-aos-duration="500">Профессиональная команда конструкторов, инженеров и дизайнеров</h2>
		<p class="accent mb-5" data-aos="fade-up" data-aos-duration="500" data-aos-delay="150">
 <a href="mailto:info@utechs.ru">info@utechs.ru</a>
		</p>
		<p class="accent mb-5 pb-5" data-aos="fade-up" data-aos-duration="500" data-aos-delay="150">
 <a href="tel:74956629291">+7(495)662-9291</a>
		</p>
 <a class="btn btn-outline-primary btn-decolor--light btn-lg" href="/testcontacts/" data-aos="fade-up" data-aos-duration="500" data-aos-delay="300">Контакты</a>
	</div>
</div>