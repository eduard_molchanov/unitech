<h2 class="h1 mt-0" data-aos="fade-up" data-aos-duration="500">Пресс-центр</h2>
<div data-aos="fade-up" data-aos-duration="500" data-aos-delay="150">
<?$APPLICATION->IncludeComponent(
	"bitrix:news.line", 
	"newslist", 
	array(
		"COMPONENT_TEMPLATE" => "newslist",
		"IBLOCK_TYPE" => "articles",
		"IBLOCKS" => array(
			0 => "26",
		),
		"NEWS_COUNT" => "2",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "DATE_ACTIVE_FROM",
			2 => "",
		),
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"RS_USE_SUMMARY_PAGE" => "Y",
		"RS_SUMMARY_PAGE_TITLE" => "Подробнее",
		"RS_SUMMARY_PAGE_LINK" => "/testnews/",
		"RS_SHOW_IBLOCK" => "N",
		"GRID_RESPONSIVE_SETTINGS" => "{\"xxs\":{\"items\":\"1\"},\"sm\":{\"items\":\"2\"}}",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"CACHE_GROUPS" => "Y",
		"ACTIVE_DATE_FORMAT" => "d.m.Y"
	),
	false
);?>
</div>
<a class="btn btn-outline-primary btn-decolor--light btn-lg" href="/testnews/" data-aos="fade-up" data-aos-duration="500" data-aos-delay="300">Подробнее</a>