<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Winn: премиальный корпоративный сайт");
$APPLICATION->SetPageProperty("wide_page", "Y");
$APPLICATION->SetPageProperty("hide_title", "Y");
$APPLICATION->SetPageProperty("hide_section", "Y");
$APPLICATION->SetPageProperty("dark_page", "Y");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"section", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/testinclude/index/banners.php",
		"COMPONENT_TEMPLATE" => "section",
		"BACKGROUND_COLOR" => "#000000",
		"EDIT_TEMPLATE" => "",
		"AREA_ID" => "winn_home_banner",
		"USE_CONTAINER" => "N",
		"BACKGROUND_IMAGE" => "",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "N",
		"RS_LIST_CONTAINER_ALIGN" => "-",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "Y",
		"RS_LIST_SECTION_VH100" => "Y"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"section", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/testinclude/index/catalog.php",
		"COMPONENT_TEMPLATE" => "section",
		"BACKGROUND_COLOR" => "",
		"EDIT_TEMPLATE" => "winn_home_catalog",
		"USE_CONTAINER" => "Y",
		"BACKGROUND_IMAGE" => "",
		"AREA_ID" => "",
		"CONTAINER_ALIGN" => "right",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "Y",
		"RS_LIST_CONTAINER_ALIGN" => "right",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"section", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/testinclude/index/banners2.php",
		"COMPONENT_TEMPLATE" => "section",
		"BACKGROUND_COLOR" => "#000000",
		"EDIT_TEMPLATE" => "",
		"AREA_ID" => "winn_home_services",
		"USE_CONTAINER" => "N",
		"BACKGROUND_IMAGE" => "",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "N",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"section", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/testinclude/index/news.php",
		"COMPONENT_TEMPLATE" => "section",
		"BACKGROUND_COLOR" => "#0B1226",
		"EDIT_TEMPLATE" => "",
		"AREA_ID" => "winn_home_articles",
		"USE_CONTAINER" => "Y",
		"BACKGROUND_IMAGE" => "",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "Y",
		"RS_LIST_CONTAINER_ALIGN" => "-",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"section", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/testinclude/index/contacts.php",
		"COMPONENT_TEMPLATE" => "section",
		"BACKGROUND_COLOR" => "#000000",
		"EDIT_TEMPLATE" => "",
		"BACKGROUND_IMAGE" => "/testassets/images/about_us.jpg",
		"AREA_ID" => "",
		"USE_CONTAINER" => "Y",
		"CONTAINER_ALIGN" => "-",
		"RS_LIST_SECTION" => "l_section",
		"RS_LIST_SECTION_ADD_CONTAINER" => "Y",
		"RS_LIST_CONTAINER_ALIGN" => "-",
		"RS_LIST_SECTION_HEADER_COMPENSATE" => "N",
		"RS_LIST_SECTION_VH100" => "Y"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>