<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
//$APPLICATION->SetTitle("НАША КОМАНДА");
?>
    <link href="/css/common.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/slick.css">
    <link rel="stylesheet" href="/css/feature-carousel.css">
    <link rel="stylesheet" href="/css/jquery.formstyler.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/slick-theme.css">
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

    <div class="content-index col-ld-9 col-md-9">
        <div class="wrap_center">
            <h1>Наша команда</h1>

            <p>
                Команда «Унитеха» имеет 20-летний опыт работы на российском рынке ИКТ, в ходе которой успешно реализован
                ряд сложных проектов в интересах крупных компаний-заказчиков. Специалисты «Унитеха», глубоко понимая
                специфику деятельности и порядок работы своих заказчиков, готовы применять свой опыт в реализации
                комплексных проектов.
            </p><br>
            <div class="team">
                <div class="team_item" id="bx_651765591_54">
                    <div class="team_description">
                        <div class="name">Александр Витоженц</div>
                        <div class="office">
                            Генеральный директор
                        </div>
                        <div style="clear:both"></div>

                    </div> <!--description !-->
                    <div class="team_img">
                        <a><img class="preview_picture"
                                src="./img/fce14698e2c49bc75a107ebf8e6f46b1.png"></a>
                    </div>

                </div>
                <div class="team_item" id="bx_651765591_55">
                    <div class="team_description">
                        <div class="name">Лев Бровко</div>
                        <div class="office">
                            Коммерческий директор
                        </div>
                        <div style="clear:both"></div>

                    </div> <!--description !-->
                    <div class="team_img">
                        <a><img class="preview_picture"
                                src="./img/0697f2b1c4f431b548ae2d0c65dd1d0e.png"></a>
                    </div>

                </div>
                <div class="team_item" id="bx_651765591_57">
                    <div class="team_description">
                        <div class="name">Мария Бартенева</div>
                        <div class="office">
                            Управляющий директор
                        </div>
                        <div style="clear:both"></div>

                    </div> <!--description !-->
                    <div class="team_img">
                        <a><img class="preview_picture"
                                src="./img/2cc00d7105ddeff6b337f715ffc16e06.png"></a>
                    </div>

                </div>
                <div class="team_item" id="bx_651765591_56">
                    <div class="team_description">
                        <div class="name">Александр Храмцов</div>
                        <div class="office">
                            Технический директор
                        </div>
                        <div class="text">
                            <blockquote dir="ltr">
                                <p>
                                </p>
                                <p style="text-align: left;">
                                    <i><span style="font-size: 11pt;">"Разрабатывая и выполняя ИТ-проекты мы работаем с удовольствием. В основе нашего успеха лежат: мастерство, ответственность и взаимопонимание членов команды."</span></i><span
                                            style="font-size: 11pt;"> </span>
                                </p>
                                <p>
                                </p>
                                <p>
                                </p>
                            </blockquote>
                            <br></div>
                        <div style="clear:both"></div>

                    </div> <!--description !-->
                    <div class="team_img">
                        <a><img class="preview_picture"
                                src="./img/3f401320c1074aed22b3f67cabc3d475.png"></a>
                    </div>

                </div>
                <br></div>


        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>