<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

<div class="row">
    <div class="col-lg-8">
        <div class="b-error-page pt-1">
            <svg class="b-error-page__img mx-0 my-5 mb-1 icon-svg text-primary"><use xlink:href="#svg-404"></use></svg>
            <h3 class="mt-2 mb-2">Страница не найдена</h3>
            <div class="accent mt-2 mb-2 pb-1">Что-то пошло не так. Запрашиваемая Вами страница не существует.</div>
            <a class="accent text-link" href="/site_nj/">Вернуться на главную</a>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>