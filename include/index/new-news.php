<?php

CModule::IncludeModule("iblock");
//id нужного инфоблока
$iblock_id = 14;
$arFilter1 = array(
    "IBLOCK_ID" => $iblock_id,
    "ACTIVE" => "Y",

);
$data = CIBlockElement::GetList(array("ID" => "DESC",), $arFilter1, false, array("nPageSize" => 2));

?>

<style>
    h2.ch {
        color: white !important;
    }

    div.bx-newslist-content {
        color: white !important;
    }

    div.bx-newslist-date {
        color: white !important;
    }

    h3.bx-newslist-title {
        color: white !important;
    }

</style>

<h2 class="h1 mt-0 ch" data-aos="fade-up" data-aos-duration="500">Новости </h2>

<div data-aos="fade-up" data-aos-duration="500" data-aos-delay="150">

    <div class="l-section__container container col-12">

        <div data-aos="fade-up" data-aos-duration="500" data-aos-delay="150" class="aos-init aos-animate">


            <ul class="row list-unstyled mb-0">
                <?

                while ($i = $data->GetNext()): ?>

                    <li class="col-12 col-sm-6 mb-6" id="bx_4665576_482">
                        <div class="newslist-item md-6">

                            <div class="newslist-item__info-date small font-weight-bolder text-lowercase">
                                <div class="h4 m-0 lh-base">
                                    <?= FormatDate("d", MakeTimeStamp($i['TIMESTAMP_X'])) ?>
                                </div>
                                <?= FormatDate("F", MakeTimeStamp($i['TIMESTAMP_X'])) ?>
                            </div>
                            <a href="/presscenter/?ELEMENT_ID=<?=$i["ID"]?>" target="_blank" style="color:#F9F9F9!important;">
                            <h3 class="newslist-item__title mb-4 mt-5 pt-1">
                                <?= $i["NAME"] ?>
                            </h3>
                            </a>
                            <div class="newslist-item__preview-text lead">
                                <?= $i["PREVIEW_TEXT"] ?>
                            </div>
                        </div>

                    </li>

                <? endwhile; ?>

            </ul>

        </div>

    </div>

</div>

<a class="btn btn-outline-primary btn-decolor--light btn-lg" target="_blank"
   href="/presscenter/" data-aos="fade-up" data-aos-duration="500" data-aos-delay="300">Подробнее</a>