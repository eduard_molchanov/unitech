<!--<h2 class="h1 mt-0" data-aos="fade-up" data-aos-duration="500">Пресс-центр</h2>-->
<style>
 h2.ch{color:white !important;}
 div.bx-newslist-content{color:white !important;}
 div.bx-newslist-date{color:white !important;}
 h3.bx-newslist-title{color:white !important;}

</style>
<h2 class="h1 mt-0 ch" data-aos="fade-up" data-aos-duration="500" >Новости</h2>
<div data-aos="fade-up" data-aos-duration="500" data-aos-delay="150">

<?
//$APPLICATION->IncludeComponent(
//	"bitrix:news.line",
//	"newslist",
//	array(
//		"COMPONENT_TEMPLATE" => "newslist",
//		"IBLOCK_TYPE" => "articles",
//		"IBLOCKS" => array(
//			0 => "56",
//		),
//		"NEWS_COUNT" => "2",
//		"FIELD_CODE" => array(
//			0 => "PREVIEW_TEXT",
//			1 => "DATE_ACTIVE_FROM",
//			2 => "",
//		),
//		"SORT_BY1" => "ACTIVE_FROM",
//		"SORT_ORDER1" => "DESC",
//		"SORT_BY2" => "SORT",
//		"SORT_ORDER2" => "ASC",
//		"RS_USE_SUMMARY_PAGE" => "Y",
//		"RS_SUMMARY_PAGE_TITLE" => "Подробнее",
//		"RS_SUMMARY_PAGE_LINK" => "/news/",
//		"RS_SHOW_IBLOCK" => "N",
//		"GRID_RESPONSIVE_SETTINGS" => "{\"xxs\":{\"items\":\"1\"},\"sm\":{\"items\":\"2\"}}",
//		"DETAIL_URL" => "",
//		"CACHE_TYPE" => "A",
//		"CACHE_TIME" => "300",
//		"CACHE_GROUPS" => "Y",
//		"ACTIVE_DATE_FORMAT" => "d.m.Y"
//	),
//	false
//);
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news",
    "flat",
    Array(
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DETAIL_FIELD_CODE" => array("",""),
        "DETAIL_PAGER_SHOW_ALL" => "N",
        "DETAIL_PAGER_TEMPLATE" => "",
        "DETAIL_PAGER_TITLE" => "Страница",
        "DETAIL_PROPERTY_CODE" => array("",""),
        "DETAIL_SET_CANONICAL_URL" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "IBLOCK_ID" => "42",
        "IBLOCK_TYPE" => "articles",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "LIST_ACTIVE_DATE_FORMAT" => "j F",
        "LIST_FIELD_CODE" => array("",""),
        "LIST_PROPERTY_CODE" => array("",""),
        "MEDIA_PROPERTY" => "",
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "NEWS_COUNT" => "2",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PREVIEW_TRUNCATE_LEN" => "",
        "SEF_MODE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SLIDER_PROPERTY" => "",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_CATEGORIES" => "N",
        "USE_FILTER" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_RATING" => "N",
        "USE_RSS" => "N",
        "USE_SEARCH" => "N",
        "USE_SHARE" => "N",
        "VARIABLE_ALIASES" => Array("ELEMENT_ID"=>"ELEMENT_ID","SECTION_ID"=>"SECTION_ID")
    )
);?>
</div>
<!--<a class="btn btn-outline-primary btn-decolor--light btn-lg" href="/news/" data-aos="fade-up" data-aos-duration="500" data-aos-delay="300">Подробнее</a>-->
<a class="btn btn-outline-primary btn-decolor--light btn-lg" href="/presscenter/" data-aos="fade-up" data-aos-duration="500" data-aos-delay="300">Подробнее42</a>