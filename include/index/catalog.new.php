<h2 class="h1 mt-0" data-aos="fade-up" data-aos-duration="500">Решения</h2>

<div id="well" class="l-main__container well  ">
    <ul class="row list-unstyled mb-0">

        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_58">
            <div class="product product--card">
                <div class="product-image-wrapper mb-3">
                    <a class="product-image-canvas" href="/catalog/?SECTION_ID=58">
                        <img class="product-image aos-init aos-animate" data-aos="zoom-in" data-aos-duration="500"
                             src="/upload/iblock/c65/i8qj49okfz87b24nkqfklqmhhctgn2vg.png" alt="Инфраструктура"
                             title="Инфраструктура" width="300">
                    </a>
                </div>
                <h4 class="product-title my-0 mb-md-2">
                    <a class="text-body" href="/catalog/?SECTION_ID=58">
                        <!--                        Инфраструктура <span>(12)</span> </a>-->
                        Инфраструктура </a>
                </h4>


            </div>
        </li>
        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_59">
            <div class="product product--card">
                <div class="product-image-wrapper mb-3">
                    <a class="product-image-canvas" href="/catalog/?SECTION_ID=59">
                        <img class="product-image aos-init aos-animate" data-aos="zoom-in" data-aos-duration="500"
                             src="/upload/iblock/177/aln0mc8qz2233eg8au6e8dzwbcr9kk8f.png" alt="Бизнес-приложения"
                             title="Бизнес-приложения" width="300">
                    </a>
                </div>
                <h4 class="product-title my-0 mb-md-2">
                    <a class="text-body" href="/catalog/?SECTION_ID=59">
                        <!--                        Бизнес-приложения <span>(12)</span> </a>-->
                        Бизнес-приложения </a>
                </h4>


            </div>
        </li>
        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_60">
            <div class="product product--card">
                <div class="product-image-wrapper mb-3">
                    <a class="product-image-canvas" href="/catalog/?SECTION_ID=60">
                        <img class="product-image aos-init aos-animate" data-aos="zoom-in" data-aos-duration="500"
                             src="/upload/iblock/4b0/q07iazwpgwot9gmydnq5a9lkcsklljzj.png" alt="Мобильные решения"
                             title="Мобильные решения" width="300">
                    </a>
                </div>
                <h4 class="product-title my-0 mb-md-2">
                    <a class="text-body" href="/catalog/?SECTION_ID=60">
                        <!--                        Мобильные решения <span>(12)</span> </a>-->
                        Мобильные решения </a>
                </h4>


            </div>
        </li>
        <li class="col-xl-2.4 col-6 my-3 my-md-4" id="bx_1847241719_61">
            <div class="product product--card">
                <div class="product-image-wrapper mb-3">
                    <a class="product-image-canvas" href="/catalog/?SECTION_ID=61">
                        <img class="product-image aos-init aos-animate" data-aos="zoom-in" data-aos-duration="500"
                             src="/upload/iblock/7b0/2w32okpohp9x6y0mhx95owwannycn2ql.png" alt="Мобильные решения"
                             title="Мобильные решения" width="300">
                    </a>
                </div>
                <h4 class="product-title my-0 mb-md-2">
                    <a class="text-body" href="/catalog/?SECTION_ID=61">
                        <!--                        Облачные сервисы <span>(11)</span> </a>-->
                        Облачные сервисы </a>
                </h4>


            </div>
        </li>

    </ul>

</div>
<style>
    #a-btn-catalog {
        color: #48a586 !important;
        border-color: #48a586!important;
    }

    #a-btn-catalog:hover {
        color: white !important;
    }

</style>
<!--<a id="a-btn-contacts" class="btn btn-outline-primary btn-decolor--light btn-lg"-->
<!--<a id="a-btn-catalog" class="btn btn-outline-primary btn-decolor btn-lg mt-5" href="/catalog/" data-aos="fade-up"-->
<a id="a-btn-catalog" class="btn btn-outline-primary btn-decolor--light btn-lg" href="/catalog/" data-aos="fade-up"
   data-aos-duration="500" data-aos-delay="150">
    <!--<a class="btn btn-outline-primary btn-decolor--light btn-lg" href="/catalog/" data-aos="fade-up" data-aos-duration="500"  data-aos-delay="150">-->
    Все решения</a>