<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arTabs = array(
    'TAB_GENERAL' => array(
        'NAME' => Loc::getMessage('RS_WINN_TUNING_TAB_GENERAL'),
    ),
);

$arParams = array();
@include ('options.general.php');

$arParameters = array(
    'TABS' => $arTabs,
    'PARAMETERS' => $arParams,
);

return $arParameters;
