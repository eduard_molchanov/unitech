<?php
$APPLICATION->IncludeComponent(
    "redsign:lightbasket.basket",
    "global",
    array(
        "COMPONENT_TEMPLATE" => "master",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "44",
        "PROPS" => array(
            0 => "CML2_ARTICLE",
            1 => "",
        ),
        "PATH_TO_ORDER" => "/site_nj/cart/order/",
        "AJAX_MODE" => "N",
        "PATH_TO_CART" => "/site_nj/cart/",
        "PATH_TO_CATALOG" => "/site_nj/catalog/",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO"
    ),
    false
);