<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("About company");
?><!--      <img src = "../img/content.jpg" alt = "">
                          <p>The team of the Universal Technologies company (Unitech) has more than 15-year work experience in the Russian market of ICT during which a number of difficult projects for the benefit of the large companies customers is successfully realized. Specialists Unitekh, deeply understanding specifics of activities and an operating procedure of the customers, are ready to apply the experience in implementation of complex projects. The Unitekh company has considerable experience and the partner statuses of the leading producers in infocommunication technologies (IBM, Nexus, EMC, HDS, Cisco, Microsoft, Veritas, VMware, Veeam and many others). Competences and qualification of specialists of the Unitekh company provide accomplishment of the ICT projects with the high quality level.
                          </p>
                          <h2>The first list</h2>
                          <ul>
                            <li>IT-consulting</li>
                          </ul>
                          <h2>The second list</h2>
                          <ol>
                            <li>text</li>
                        </ol>
                        <h3>The table</h3>
                        <table>
                            <tr>
                                <th rowspan="2">N%</th>
                                <th rowspan="2">Name</th>
                                <th colspan="2">Name</th>
                            </tr>
                            <tr>
                            	<th class = "color_cell">Subsection</th>
                            	<th class = "color_cell">2 subsection</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>System</td>
                                <td>no</td>
                                <td>text</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>text</td>
                                <td>no</td>
                                <td>text</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>text</td>
                                <td>no</td>
                                <td>text</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>text</td>
                                <td>bno</td>
                                <td>text</td>
                            </tr>
                        </table>
                        <h3>Download files</h3>
                        <div class="download"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><a class="download__link pdf" href = " " >adobe .pdf</a><span>99kb</span></div>!-->
						<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"company", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "23",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "ENGLISH",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "company",
		"LANG" => "Y"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>