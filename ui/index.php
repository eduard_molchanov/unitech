<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Возможности решения");
?><br>
<h2 class="mt-3 mb-2">Типографика</h2>
<p class="lead mw-900">Главная цель большинства корпоративных сайтов, это донесение информации до посетителя. Рассказать о себе, естественно можно словом. Стиль заголовков, шрифт, размер, межстрочное расстояние, отступы — все эти характеристики воздействуют на лёгкость восприятия информации через текст. За все это и отвечает типографика.</p>
<h3 class="h1">Заголовок Первого Уровня</h3>
<div class="accent mw-900">Accent – Крупный текст. Он хорошо смотрится рядом с крупными заголовками</div>
<h3 class="h2">Заголовок Второго Уровня</h3>
<div class="lead mw-900">Lead – Укрупненный текст. Удобочитаемый размер шрифта способствует приятному, комфортному восприятию текстовой информации на сайте</div>
<h3>Заголовок Третьего Уровня</h3>
<p class="mw-900">Paragraph – Базовый текст. Используется редко, однако наш текст не терял надежды найти свою подругу и друзей, Lora Ipsum и Кнопки</p>
<h4>Заголовок Четвертого Уровня</h4>
<h4>Заголовок Пяторго Уровня</h4>
<h4>Заголовок Шестого Уровня</h4>
<div class="snippet-quote1">
	<div class="row">
		<div class="col-12 col-xl-9">
			<div class="snippet-quote__svg"></div>		
			<p class="snippet-quote__descr1">
				Цитата – это точная дословная выдержка из какого-либо текста или высказывания, от словосочетания или простого предложения до внушительного отрывка текста
			</p>
			<span class="snippet-quote__author">Неизвестный Автор</span>
		</div>
	</div>
</div>
<p>
	<a href="#">Ссылки в тексте</a>
</p>
<h2>Списки</h2>
<p class="accent mb-6">
	Отличный способ выделить ключевые позиции в тексте
</p>
<div class="row">
	<div class="col-12 col-sm-4">
		<div class="mb-6">
			<b>Нумерованный список</b>
			<ol>
				<li>Первым делом надо все обдумать</li>
				<li>Потом придумать как сделать</li>
				<li>И выполнить поставленную задачу</li>
			</ol>
		</div>
		<div class="mb-6">
			<b>Список — Тире</b>
			<ul class="list-dashed">
				<li>У нас есть быстрая доставка</li>
				<li>Мы гарантируем низкие цены</li>
				<li>Дарим бонусы и подарки</li>
			</ul>
		</div>
		<div class="mb-6">
			<b>Список характеристик</b>
			<dl>
				<dt>Артикул товара</dt>
				<dd>5842245</dd>
				<dt>Цвет</dt>
				<dd>Emotion RED 32</dd>
				<dt>Размер</dt>
				<dd>45</dd>
				<dt>Масса</dt>
				<dd>300 кг.</dd>
				<dt>Производитель</dt>
				<dd>Россия</dd>
			</dl>
		</div>
	</div>
	<div class="col-12 col-sm-4 offset-sm-2">
		<div class="mb-6">
			<b>Маркированный список # 1</b>
			<ul>
				<li>Сотрудничать с нами выгодно</li>
				<li>Доверять нам правильно</li>
			</ul>
		</div>
		<div class="mb-6">
		<b>Маркированный список # 2</b>
			<ul class="list-dotted">
				<li>Производим анализ ситуации</li>
				<li>Составляем план выполнения задачи</li>
				<li>Решаем задачу
				<ul>
					<li>Быстро</li>
					<li>Качественно</li>
					<li>Эффективно</li>
				</ul>
 </li>
				<li>Тестируем полученный результат</li>
				<li>Получаем качественный продукт</li>
			</ul>
		</div>
	</div>
</div>
<h2>Кнопки</h2>
<div class="mb-6">
	<h3 class="h4">Стандартные кнопки</h3>
	<div class="mt-2">
		<button type="button" class="btn btn-primary mb-1">Primary</button>
		<button type="button" class="btn btn-secondary mb-1">Secondary</button>
		<button type="button" class="btn btn-success mb-1">Success</button>
		<button type="button" class="btn btn-danger mb-1">Danger</button>
		<button type="button" class="btn btn-warning mb-1">Warning</button>
		<button type="button" class="btn btn-info mb-1">Info</button>
		<button type="button" class="btn btn-dark mb-1">Dark</button>
		<button type="button" class="btn btn-light mb-1">Light</button>
	</div>
</div>
<div class="mb-6">
	<h3 class="h4">Outlines buttons</h3>
	<div class="mt-2">
		<button type="button" class="btn btn-outline-primary mb-1">Primary</button>
		<button type="button" class="btn btn-outline-secondary mb-1">Secondary</button>
		<button type="button" class="btn btn-outline-success mb-1">Success</button>
		<button type="button" class="btn btn-outline-danger mb-1">Danger</button>
		<button type="button" class="btn btn-outline-warning mb-1">Warning</button>
		<button type="button" class="btn btn-outline-info mb-1">Info</button>
		<button type="button" class="btn btn-outline-dark mb-1">Dark</button>
		<button type="button" class="btn btn-outline-light mb-1">Light</button>
	</div>
</div>

<div class="mb-6 mb-1">
	<h3 class="h4">Decor Outlines buttons</h3>
	<div class="mt-2">
		<button type="button" class="btn btn-outline-primary btn-decolor mb-1">Primary</button>
		<button type="button" class="btn btn-outline-secondary btn-decolor mb-1">Secondary</button>
		<button type="button" class="btn btn-outline-success btn-decolor mb-1">Success</button>
		<button type="button" class="btn btn-outline-danger btn-decolor mb-1">Danger</button>
		<button type="button" class="btn btn-outline-warning btn-decolor mb-1">Warning</button>
		<button type="button" class="btn btn-outline-info btn-decolor mb-1">Info</button>
		<button type="button" class="btn btn-outline-dark btn-decolor mb-1">Dark</button>
		<button type="button" class="btn btn-outline-light btn-decolor mb-1">Light</button>
	</div>
</div>
<div class="mb-6">
	<h3 class="h4">Sizes</h3>
	<div class="mt-2 mb-4 d-flex align-items-end">
		<button type="button" class="btn btn-primary btn-lg mr-4 mb-1">Large button</button>
		<button type="button" class="btn btn-primary mr-4 mb-1">Default button</button>
		<button type="button" class="btn btn-primary btn-sm mb-1">Small button</button>
	</div>
	<div>
 <button type="button" class="btn btn-dark btn-lg btn-block">Block level button</button>
	</div>
</div>
<div class="mb-6">
	<h3 class="h4">Buttons group</h3>
	<div class="mt-2">
		<div class="btn-group btn-group-toggle" data-toggle="buttons">
 <label class="btn btn-secondary active"> <input type="radio" name="options" id="option1" autocomplete="off" checked=""> Active </label> <label class="btn btn-secondary"> <input type="radio" name="options" id="option2" autocomplete="off"> Radio </label> <label class="btn btn-secondary"> <input type="radio" name="options" id="option3" autocomplete="off"> Radio </label>
		</div>
	</div>
</div>
<div class="mb-6">
	<h3 class="h4">Dropdown</h3>
	<div class="dropdown">
 <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		Dropdown link </a>
		<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
 <a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another action</a> <a class="dropdown-item" href="#">Something else here</a>
		</div>
	</div>
</div>
 <br>
<div class="mb-5">
	<h2 class="mb-5">Элементы</h2>
	<div class="nav-wrap">
		<ul class="nav nav-slide" id="myTab" role="tablist">
			<li class="nav-item"> <a class="nav-link py-5 my-2 active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true"><b class="text-uppercase">Описание</b></a> </li>
			<li class="nav-item"> <a class="nav-link py-5 my-2" id="specification-tab" data-toggle="tab" href="#specification" role="tab" aria-controls="specification" aria-selected="false"><b class="text-uppercase">Характеристики</b></a> </li>
			<li class="nav-item"> <a class="nav-link py-5 my-2" id="files-tab" data-toggle="tab" href="#files" role="tab" aria-controls="files" aria-selected="false"><b class="text-uppercase">Документация</b></a> </li>
		</ul>
	</div>
	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade py-4 show active" id="description" role="tabpanel" aria-labelledby="description-tab">
			<p>
				Как-то текстовая копия рассказала маленькому тексту историю. Что когда она была оригинальной, её опубликовали на сайте, а пользователи читали и оставляли свои комментарии. Однако пришли горе-копирайтеры и тысячу раз её переписали. Поэтому маленькому тексту лучше оставаться дома, если он не хочет потерять себя.
			</p>
		</div>
		<div class="tab-pane fade py-4" id="specification" role="tabpanel" aria-labelledby="specification-tab">
			<dl style="max-width:730px;">
				<dt>Двигатель</dt>
				<dd>Milwaukee-Eight® 114</dd>
				<dt>Диаметр цилиндра</dt>
				<dd>102 мм</dd>
				<dt>Ход поршня</dt>
				<dd>114 мм</dd>
				<dt>Рабочий объем</dt>
				<dd>1,868 см3</dd>
				<dt>Степень сжатия</dt>
				<dd>5:1</dd>
			</dl>
		</div>
		<div class="tab-pane fade py-4" id="files" role="tabpanel" aria-labelledby="files-tab">
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list", 
				"files", 
				array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"FILTER_NAME" => "",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => "46",
					"IBLOCK_TYPE" => "content",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"INCLUDE_SUBSECTIONS" => "Y",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "20",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "bootstrap",
					"PAGER_TITLE" => "",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "FILE",
						2 => "",
					),
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_BY2" => "SORT",
					"SORT_ORDER1" => "DESC",
					"SORT_ORDER2" => "ASC",
					"STRICT_SECTION_CHECK" => "N",
					"COMPONENT_TEMPLATE" => "files"
				),
				false
			);?>
		</div>
	</div>
</div>
 <br>
<div class="mb-5">
	<h2 class="mb-5">Формы</h2>
	<div class="row">
		<div class="col-12 col-sm-6">
			<div class="mb-6">
				<h3 class="h4 mt-0">Bootstrap forms</h3>
				<div class="form-group">
 <label class="font-size-sm text-secondary d-block" for="exampleInputName">Ваше имя</label> <input type="text" name="name" class="form-control" id="exampleInputName" autocomplete="off">
				</div>
				<div class="form-group">
					<label class="font-size-sm text-secondary d-block" for="exampleInputPhone">Номер телефона</label>
					<input type="text" name="phone" class="form-control" id="exampleInputPhone" autocomplete="off" data-mask="+7 (000) 000-00-00">
				</div>
				<div class="form-group">
 <label class="font-size-sm text-secondary d-block" for="exampleInputPassword">Пароль</label> <input type="password" name="password" class="form-control" id="exampleInputPassword" autocomplete="off">
				</div>
				<div class="form-group">
 <label class="font-size-sm text-secondary d-block" for="exampleTextarea">Текст сообщения</label> <textarea class="form-control" id="exampleTextarea" rows="8"></textarea>
				</div>
			</div>
			<div class="mb-6">
				<h3 class="h4">Radio</h3>
				<div class="form-group form-check mb-3">
					<input class="form-check-input" id="optionsRadios1" type="radio" name="optionsRadios" value="option1" checked>
					<label class="form-check-label" for="optionsRadios1">Первый вариант состояния</label>
				</div>
				<div class="form-group form-check mb-3">
					<input class="form-check-input" id="optionsRadios2" type="radio" name="optionsRadios" value="option2">
					<label class="form-check-label" for="optionsRadios2">Второй вариант состояния</label>
				</div>
				<div class="form-group form-check mb-3">
					<input class="form-check-input" id="optionsRadios3" type="radio" name="optionsRadios" value="option3" disabled>
					<label class="form-check-label" for="optionsRadios3">Вариант третий отключен</label>
				</div>
			</div>
			<div class="mb-6">
				<h3 class="h4">Checkbox</h3>
				<div class="form-group form-check mb-3">
					<input class="form-check-input" id="optionsCheckbox1" type="checkbox" name="optionsCheckbox[]" value="option1" checked>
					<label class="form-check-label" for="optionsCheckbox1">Первый вариант состояния</label>
				</div>
				<div class="form-group form-check mb-3">
					<input class="form-check-input" id="optionsCheckbox2" type="checkbox" name="optionsCheckbox[]" value="option2">
					<label class="form-check-label" for="optionsCheckbox2">Второй вариант состояния</label>
				</div>
				<div class="form-group form-check mb-3">
					<input class="form-check-input" id="optionsCheckbox3" type="checkbox" name="optionsCheckbox[]" value="option3" disabled>
					<label class="form-check-label" for="optionsCheckbox3">Вариант третий отключен</label>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6">
			<div class="mb-6">
				<h3 class="h4 mt-0">Material forms</h3>
				<div class="form-group bmd-form-group">
 <label for="exampleInputName2" class="bmd-label-floating bmd-label-static">Ваше имя</label> <input type="text" class="form-control" name="name2" id="exampleInputName2">
				</div>
				<div class="form-group bmd-form-group">
					<label for="exampleInputPhone2" class="bmd-label-floating bmd-label-static">Номер телефона</label>
					<input type="text" class="form-control" name="phone2" id="exampleInputPhone2" data-mask="+7 (000) 000-00-00">
				</div>
				<div class="form-group bmd-form-group">
 <label for="exampleInputPassword2" class="bmd-label-floating bmd-label-static">Пароль</label> <input type="password" class="form-control" name="password2" id="exampleInputPassword2" autocomplete="off">
				</div>
				<div class="form-group bmd-form-group">
 <label for="example2Textarea" class="bmd-label-floating bmd-label-static">Текст сообщения</label> <textarea class="form-control" id="example2Textarea" rows="8"></textarea>
				</div>
			</div>
			<div class="mb-6">
				<h3 class="h4">Radio</h3>
				<div class="bmd-form-group pt-0 mb-2">
					<div class="bmd-custom-radio radio">
 <label> <input type="radio" name="optionMaterialRadio" value="option1" checked="">Первый вариант состояния </label>
					</div>
				</div>
				<div class="bmd-form-group pt-0 mb-2">
					<div class="bmd-custom-radio radio">
 <label> <input type="radio" name="optionMaterialRadio" value="option2">Второй вариант состояния </label>
					</div>
				</div>
				<div class="bmd-form-group pt-0 mb-2">
					<div class="bmd-custom-radio radio disabled">
 <label> <input type="radio" name="optionMaterialRadio" value="option3" disabled="">Вариант третий отключен </label>
					</div>
				</div>
			</div>
			<div class="mb-6">
				<h3 class="h4">Checkbox</h3>
				<div class="bmd-form-group pt-0 mb-2">
					<div class="checkbox bmd-custom-checkbox">
 <label> <input type="checkbox" name="optionMaterialCheckbox[]" value="option1" checked="">Первый вариант состояния </label>
					</div>
				</div>
				<div class="bmd-form-group pt-0 mb-2">
					<div class="checkbox bmd-custom-checkbox">
 <label> <input type="checkbox" name="optionMaterialCheckbox[]" value="option2">Второй вариант состояния </label>
					</div>
				</div>
				<div class="bmd-form-group pt-0 mb-2">
					<div class="checkbox bmd-custom-checkbox disabled">
 <label> <input type="checkbox" name="optionMaterialCheckbox[]" value="option3" disabled="">Вариант третий отключен </label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>